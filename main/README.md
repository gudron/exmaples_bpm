## Утилиты

```
go get -u github.com/mailru/easyjson/...
go get -u github.com/golang/mock/gomock
go get -u github.com/golang/mock/mockgen
```

## Обновление `golden`-файлов

```sh
$ go test -test.run BPTStore_GetBPTemplateByID/Existed ./src/bptemplate -golden
```

## Запуск с локальной машины на инфраструктуре стэнда

С пересборкой исполняемого файла

```sh
$ make HOST=dev-basov build run
```
