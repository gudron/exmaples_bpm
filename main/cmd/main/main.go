package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"git.elewise.com/elma365/main/src/auth"
	"git.elewise.com/elma365/main/src/common"
	"git.elewise.com/elma365/main/src/common/config"
	"git.elewise.com/elma365/main/src/common/errs"
	"git.elewise.com/elma365/main/src/common/i18n"
	"git.elewise.com/elma365/main/src/common/queries"

	"github.com/pkg/errors"
	"go.uber.org/zap"

	_ "git.elewise.com/elma365/types/validator" // code validator
	_ "github.com/lib/pq"
)

func main() {
	cfg := config.Default()

	if err := cfg.ReadEnv(); err != nil {
		fmt.Println(err.Error())

		os.Exit(1)
	}

	l := config.Logger(cfg)

	defer l.Sync()

	jCloser, err := config.Jaeger(cfg)
	if err != nil {
		l.Warn("jaeger connection error", zap.Error(errors.WithStack(err)))
	} else {
		defer jCloser.Close()
	}

	l.Info("connect to auth service")
	auth := auth.New(cfg)

	l.Info("connect to elastic")
	ec, err := queries.NewElastic(cfg)
	if err != nil {
		l.Fatal(err.Error(), errs.ZapStack(err))
	}
	qc := queries.NewClient(ec, auth)

	l.Info("connect to db")
	str, err := common.NewStore(context.Background(), cfg, qc)
	if err != nil {
		l.Fatal(err.Error(), errs.ZapStack(err))
	}

	l.Info("connect to redis")
	red, err := common.NewRedis(cfg)
	if err != nil {
		l.Fatal(err.Error(), errs.ZapStack(err))
	}

	l.Info("initializing new grpc connections")
	gClients, err := common.NewGRPCClients(cfg)
	if err != nil {
		l.Fatal(err.Error(), errs.ZapStack(err))
	}

	l.Info("load i18n")
	i18n, err := i18n.Load()
	if err != nil {
		l.Fatal(err.Error(), errs.ZapStack(err))
	}

	s := &http.Server{
		Addr:              cfg.Bind,
		Handler:           common.NewRouter(cfg, auth, str, gClients, red, i18n),
		ErrorLog:          zap.NewStdLog(zap.L()),
		ReadHeaderTimeout: time.Duration(cfg.ReadTimeout) * time.Second,
	}

	l.Info("starting http service")
	go func() {
		err := s.ListenAndServe()
		if err != nil {
			l.Fatal(err.Error())
		}
	}()

	c := make(chan os.Signal)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
	<-c
	l.Warn("stopping http service")
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		<-c
		cancel()
	}()
	if err = s.Shutdown(ctx); err != nil {
		l.Fatal(err.Error())
	}
	l.Warn("stopped")
}
