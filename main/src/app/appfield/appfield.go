package appfield

//go:generate easyjson appfield.go

import (
	"encoding/json"

	"git.elewise.com/elma365/types"
)

// AppField описывает поле приложения
//
//easyjson:json
type AppField struct {
	types.Field
	View FieldView `json:"view"`
}

// FieldView описывает данные отображения поля приложения
//
//easyjson:json
type FieldView struct {
	Name    string          `json:"name"`
	Sort    int             `json:"sort"`
	Tooltip string          `json:"tooltip"`
	Key     bool            `json:"key"`    // Основное значимое поле
	System  bool            `json:"system"` // Признак системного поля
	Hidden  bool            `json:"hidden"` // Видимость поля на клиенте (пример - обратные ссылки типа "Приложение")
	Data    json.RawMessage `json:"data"`   // Данные, специфичные для отображения
}

// FieldViewData описывает специализированные для каждого типа данные для отображения поля (базовая структура)
//
//easyjson:json
type FieldViewData struct {
	AdditionalType string `json:"additionalType"`
}
