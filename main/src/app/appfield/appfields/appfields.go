package appfields

//go:generate easyjson appfields.go

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"

	"git.elewise.com/elma365/main/src/app/appfield"
	"git.elewise.com/elma365/main/src/app/appfield/syscollection"
	"git.elewise.com/elma365/types"
)

// AppFields описывает массив полей приложения
//
//easyjson:json
type AppFields []appfield.AppField

// MakeCodeUniqueIfNeed проверяет отсутствие поля с заданным кодом в списке полей и добавляет к коду уникальный суффикс
// в случае его наличия
func (o AppFields) MakeCodeUniqueIfNeed(code string) string {
	if !o.IsCodeUsed(code) {
		return code
	}

	// Проверяем что такого кода еще нет
	isExists := false
	newCode := code
	suffixIndex := 0
	for !isExists {
		suffixIndex++
		newCode = fmt.Sprintf("%s_%d", code, suffixIndex)
		isExists = o.IsCodeUsed(code)
	}

	return newCode

}

// IsCodeUsed проверяет уникальность заданного кода
func (o *AppFields) IsCodeUsed(code string) bool {
	for _, field := range *o {
		if field.Code == code {
			return true
		}
	}

	return false
}

// Scan implements sql.Scanner interface
func (o *AppFields) Scan(pSrc interface{}) error {
	var afs []appfield.AppField

	switch src := pSrc.(type) {
	case nil:
		return nil

	case []byte:
		if err := json.Unmarshal(src, &afs); err != nil {
			return err
		}

	default:
		return fmt.Errorf("AppFields.Scan: cannot scan type %T into AppFields", pSrc)
	}

	*o = AppFields(afs)
	return nil
}

// Value implements sql.Valuer interface
func (o AppFields) Value() (value driver.Value, err error) {
	return o.MarshalJSON()
}

// Validate validates payload for current appfields
func (o *AppFields) Validate(payload map[string]json.RawMessage) error {
	fields := make([]types.Field, 0)
	for _, field := range *o {
		fields = append(fields, field.Field)
	}

	// Валидируем типы полей
	content := types.NewContent(fields)
	err := content.Validate(payload, true)
	if err != nil {
		return err
	}

	for _, field := range *o {
		if field.Type == types.SysCollection {
			if err := syscollection.ValidateItem(field, payload); err != nil {
				return err
			}
		}
	}

	return nil
}
