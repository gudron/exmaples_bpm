package syscollection

import (
	"encoding/json"
	"fmt"

	"git.elewise.com/elma365/main/src/app/appfield"
	"git.elewise.com/elma365/main/src/common/errs"
	"git.elewise.com/elma365/types/complextypes/syscollection"
	"github.com/pkg/errors"
)

// Data описывает объект Data поля типа SYS_COLLECTION
type Data syscollection.Data

// AppFields TODO
type AppFields interface {
	MakeCodeUniqueIfNeed(code string) string
}

// GetLinkedFieldCode формирует код связанного поля
func GetLinkedFieldCode(appCode string, fieldCode string, fields AppFields) string {
	code := fmt.Sprintf("%s_%s", appCode, fieldCode)
	return fields.MakeCodeUniqueIfNeed(code)
}

// ValidateItem валидирует данные для полей типа "Приложение"
func ValidateItem(field appfield.AppField, payload map[string]json.RawMessage) error {
	valuesJSON, ok := payload[field.Code]
	if !ok {
		return errors.Errorf("field not found")
	}

	if string(valuesJSON) == "null" {
		return nil
	}

	var values []string
	err := json.Unmarshal(valuesJSON, &values)
	if err != nil {
		return errors.Errorf("invalid value format for field <%s>", field.Code)
	}

	if field.Single && len(values) > 1 {
		return errors.Errorf("invalid value for field <%s>", field.Code)
	}

	return nil
}

// ValidateField валидирует поле типа "Приложение"
func ValidateField(newField appfield.AppField, originField *appfield.AppField, namespace, code string) error {
	if newField.Array == false {
		// Поле типа SYSTEM_COLLECTION может быть только массивом, во избежание проблем с переключением "один" - "много"
		return errs.ValidationError.WithData(
			fmt.Sprintf("field '%s' array value should be true", newField.Code))
	}

	var newData Data
	err := json.Unmarshal(newField.Data, &newData)
	if err != nil {
		return errors.WithStack(err)
	}

	if len(newData.Namespace) == 0 {
		return errs.ValidationError.WithData(
			fmt.Sprintf("field '%s' data 'namespace' property should be defined", newField.Code))
	}

	if len(newData.Code) == 0 {
		return errs.ValidationError.WithData(
			fmt.Sprintf("field '%s' data 'code' property should be defined", newField.Code))
	}

	if originField != nil {
		var originData Data
		err := json.Unmarshal(originField.Data, &originData)
		if err != nil {
			return errors.WithStack(err)
		}

		/*
		 * Запрет изменения настроек созданного поля
		 */

		if originData.Namespace != newData.Namespace {
			return errs.ValidationError.WithData(
				fmt.Sprintf("field '%s' data 'namespace' property can't be changed", newField.Code))
		}

		if originData.Code != newData.Code {
			return errs.ValidationError.WithData(
				fmt.Sprintf("field '%s' data 'code' property can't be changed", newField.Code))
		}

		if originData.LinkedFieldCode != newData.LinkedFieldCode {
			return errs.ValidationError.WithData(
				fmt.Sprintf("field '%s' data 'linkedFieldCode' property can't be changed", newField.Code))
		}

		if !originField.Single && newField.Single {
			return errs.ValidationError.WithData(
				fmt.Sprintf("field '%s' 'single' property can't be changed", newField.Code))
		}
	}

	return nil
}
