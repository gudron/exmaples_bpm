package syscollection

import (
	"encoding/json"
	"testing"

	"git.elewise.com/elma365/main/src/app/appfield"

	"github.com/stretchr/testify/assert"
)

func TestValidateItem(t *testing.T) {
	tests := []struct {
		name     string
		field    json.RawMessage
		payload  map[string]json.RawMessage
		expected string
	}{
		{
			name: "error: field not found",
			field: json.RawMessage(`{
				"code": "missing-field"
			}`),
			payload:  map[string]json.RawMessage{},
			expected: "field not found",
		},
		{
			name: "error: invalid multiple value when many FALSE",
			field: json.RawMessage(`{
				"code": "field1",
				"single": true
			}`),
			payload: map[string]json.RawMessage{
				"field1": json.RawMessage(`["1", "2"]`),
			},
			expected: "invalid value for field <field1>",
		},
		{
			name: "error: invalid single value when many TRUE",
			field: json.RawMessage(`{
				"code": "field1",
				"single": false
			}`),
			payload: map[string]json.RawMessage{
				"field1": json.RawMessage(`"1"`),
			},
			expected: "invalid value format for field <field1>",
		},

		{
			name: "null value",
			field: json.RawMessage(`{
				"code": "field1"
			}`),
			payload: map[string]json.RawMessage{
				"field1": json.RawMessage(`null`),
			},
			expected: "",
		},
		{
			name: "valid single value",
			field: json.RawMessage(`{
				"code": "field1",
				"single": true
			}`),
			payload: map[string]json.RawMessage{
				"field1": json.RawMessage(`["1"]`),
			},
			expected: "",
		},
		{
			name: "valid multiple values",
			field: json.RawMessage(`{
				"code": "field1",
				"single": false
			}`),
			payload: map[string]json.RawMessage{
				"field1": json.RawMessage(`["1", "2"]`),
			},
			expected: "",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			var field appfield.AppField
			err := json.Unmarshal(tt.field, &field)
			assert.NoError(t, err)

			err = ValidateItem(field, tt.payload)
			if len(tt.expected) == 0 {
				assert.NoError(t, err)
			} else {
				assert.Error(t, err)
				assert.Equal(t, tt.expected, err.Error())
			}
		})
	}
}
