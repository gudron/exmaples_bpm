package appview

//go:generate easyjson appview.go

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"git.elewise.com/elma365/main/src/app/appfield"
	"git.elewise.com/elma365/main/src/app/appfield/appfields"
	"git.elewise.com/elma365/main/src/app/appfield/syscollection"
	"git.elewise.com/elma365/main/src/app/appview/filter"
	"git.elewise.com/elma365/main/src/app/appview/settings"
	"git.elewise.com/elma365/main/src/auth/claims"
	"git.elewise.com/elma365/main/src/common/errs"
	"git.elewise.com/elma365/main/src/common/permissions"

	"git.elewise.com/elma365/types"

	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
)

const (
	Document              = "DOCUMENT"
	DocumentFileFieldCode = "__file"
	DocumentNameFieldCode = "__name"
)

// AppView описывает приложение
//
//easyjson:json
type AppView struct {
	ID          uuid.UUID           `db:"id" json:"__id"`
	Name        string              `db:"name" json:"name"`
	ElementName string              `db:"element_name" json:"elementName"`
	Namespace   string              `db:"namespace" json:"namespace"`
	Code        string              `db:"code" json:"code"`
	Type        string              `db:"type" json:"type" valid:"in(STANDART|DOCUMENT)"`
	Fields      appfields.AppFields `db:"fields" json:"fields"`
	Filters     filter.Filters      `db:"filters" json:"filters"`
	Settings    json.RawMessage     `db:"settings" json:"settings"`
	CreatedAt   time.Time           `db:"created_at" json:"__createdAt"`
	UpdatedAt   time.Time           `db:"updated_at" json:"__updatedAt"`
	DeletedAt   *time.Time          `db:"deleted_at" json:"__deletedAt"`
	CreatedBy   uuid.UUID           `db:"created_by" json:"__createdBy"`
	UpdatedBy   uuid.UUID           `db:"updated_by" json:"__updatedBy"`
}

// CopyWithUpdateMetadata implements common.entity
func (a *AppView) CopyWithUpdateMetadata(ctx context.Context) *AppView {
	newAppView := *a
	newAppView.ID = uuid.NewV4()
	newAppView.CreatedAt = time.Now().UTC()
	newAppView.CreatedBy = claims.UserIDFromContext(ctx)
	newAppView.UpdatedAt = time.Time{}
	newAppView.UpdatedBy = newAppView.CreatedBy
	return &newAppView
}

func (app *AppView) GetSettings() (*settings.ApplicationSettings, error) {
	settings := settings.ApplicationSettings{}
	err := json.Unmarshal(app.Settings, &settings)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	return &settings, nil

}

// GetKeyField получает ключевое поле приложения
func (app *AppView) GetKeyField() (*appfield.AppField, error) {
	for _, field := range app.Fields {
		if field.View.Key && !field.Deleted {
			return &field, nil
		}
	}

	return nil, errors.Errorf("Key fields not found for application <%s>", app.ID)
}

// UpdateFields обновляет данные полей
func (app *AppView) UpdateFields(ctx context.Context, str Store, fields appfields.AppFields) error {
	err := app.validateNewFields(fields)
	if err != nil {
		return err
	}

	appsForUpdate := make([]AppView, 0)
	newSysCollectionFields := make([]appfield.AppField, 0)

	// При добавлении поля типа SYSTEM_COLLECTION необходимо вычислить и прописать в его данные код связанного поля.
	// Поле попадает в readyToSaveFields только после этой операции и считается готовым к сохранению.
	// Кроме того, readyToSaveFields используется для получения уникального кода поля, которое ссылается на
	// собственное приложение. За одну операцию сохранения может добавиться несколько таких полей.
	readyToSaveFields := appfields.AppFields{}

	// Отделяем еще не сохраненные поля типа SYSTEM_COLLECTION от остальных полей
	for _, field := range fields {
		if field.Type == types.SysCollection {
			var fieldData syscollection.Data
			err := json.Unmarshal(field.Data, &fieldData)
			if err != nil {
				return errors.WithStack(err)
			}

			if len(fieldData.LinkedFieldCode) == 0 {
				newSysCollectionFields = append(newSysCollectionFields, field)
			} else {
				readyToSaveFields = append(readyToSaveFields, field)
			}
		} else {
			readyToSaveFields = append(readyToSaveFields, field)
		}
	}

	// Приложения текущего раздела
	var apps []AppView
	if len(newSysCollectionFields) > 0 {
		apps, err = str.GetAppsByNamespace(ctx, app.Namespace)
		if err != nil {
			return err
		}
	}

	// Заполняем код связанного поля и создаем обратное поле в связанном справочнике
	for _, field := range newSysCollectionFields {
		var err error
		readyToSaveFields, appsForUpdate, err = app.addSystemCollectionField(field, readyToSaveFields, appsForUpdate, apps)
		if err != nil {
			return err
		}
	}

	app.Fields = readyToSaveFields
	appsForUpdate = append(appsForUpdate, *app)

	return str.SaveFields(ctx, appsForUpdate)
}

func (app *AppView) addSystemCollectionField(field appfield.AppField, readyToSaveFields appfields.AppFields,
	appsForUpdate []AppView, appsCache []AppView) (appfields.AppFields, []AppView, error) {
	var fieldData syscollection.Data
	err := json.Unmarshal(field.Data, &fieldData)
	if err != nil {
		return readyToSaveFields, appsForUpdate, errors.WithStack(err)
	}

	var linkedField *appfield.AppField
	if fieldData.Code == app.Code {
		// Поле ссылается на собственное приложение
		linkedField, err = app.makeLinkedField(field.Code, fieldData, readyToSaveFields)
		if err != nil {
			return readyToSaveFields, appsForUpdate, errors.WithStack(err)
		}

		readyToSaveFields = append(readyToSaveFields, *linkedField)
	} else {
		// Ищем в обновленных приложениях
		isLinkedAppFromCache := true
		linkedApp := getAppByCode(fieldData.Code, appsForUpdate)
		if linkedApp == nil {
			// Или во всех приложениях раздела
			linkedApp = getAppByCode(fieldData.Code, appsCache)
			isLinkedAppFromCache = false
		}

		if linkedApp == nil {
			return readyToSaveFields, appsForUpdate, errs.ValidationError.WithData(
				fmt.Sprintf("linked application '%s' not found in namespace '%s'",
					fieldData.Code, app.Namespace))
		}

		linkedField, err = app.makeLinkedField(field.Code, fieldData, linkedApp.Fields)
		if err != nil {
			return readyToSaveFields, appsForUpdate, errors.WithStack(err)
		}
		linkedApp.Fields = append(linkedApp.Fields, *linkedField)

		if !isLinkedAppFromCache {
			appsForUpdate = append(appsForUpdate, *linkedApp)
		}
	}

	// Установим код связанного поля в текущее поле
	fieldData.LinkedFieldCode = linkedField.Code
	bytes, err := json.Marshal(fieldData)
	if err != nil {
		return readyToSaveFields, appsForUpdate, errors.WithStack(err)
	}
	field.Data = bytes

	// Добавим готовое поле к остальным полям
	readyToSaveFields = append(readyToSaveFields, field)

	return readyToSaveFields, appsForUpdate, nil
}

func (app *AppView) makeLinkedField(fieldCode string, fieldData syscollection.Data, fields appfields.AppFields) (
	*appfield.AppField, error) {

	linkedFieldCode := syscollection.GetLinkedFieldCode(app.Code, fieldCode, fields)
	data := syscollection.Data{
		Namespace:       app.Namespace,
		Code:            app.Code,
		LinkedFieldCode: fieldCode,
	}
	bytes, err := json.Marshal(data)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	linkedField := appfield.AppField{
		Field: types.Field{
			Code:   linkedFieldCode,
			Type:   types.SysCollection,
			Array:  true,
			Single: false, // Заранее неизвестно сколько будет связанных элементов
			Data:   bytes,
		},
		View: appfield.FieldView{
			Hidden: true, // Скрытое
		},
	}

	return &linkedField, nil
}

func (app *AppView) validateNewFields(fields []appfield.AppField) error {
	err := app.validateFieldsConsistency(fields)
	if err != nil {
		return err
	}

	for _, field := range fields {
		if field.Type == types.SysCollection {
			originField := getFieldByCode(field.Code, app.Fields)
			if err := syscollection.ValidateField(field, originField, app.Namespace, app.Code); err != nil {
				return err
			}
		}
	}

	return nil
}

func (app *AppView) validateFieldsConsistency(fields []appfield.AppField) error {
	for _, originField := range app.Fields {
		newField := getFieldByCode(originField.Code, fields)
		if newField == nil {
			return errs.ValidationError.WithData(
				fmt.Sprintf("missing already saved field '%s'", originField.Code))
		}

		if newField.Type != originField.Type {
			return errs.ValidationError.WithData(
				fmt.Sprintf("field '%s' already saved and should have type '%s'", originField.Code, originField.Type))
		}
	}

	return nil
}

// GetBPNamespace - возвращает подготовленный namespace для процессов этого приложения
// Чтобы из namespace можно было восстановить прилоежение, namespace процесса состоит из namespace и code приложения
// разделеные точкой (это запрещенный символ для простого namespace)
func (app *AppView) GetBPNamespace() string {
	return app.Namespace + "." + app.Code
}

func (s *AppStore) buildDocumentMandatoryFields() ([]appfield.AppField, error) {
	fileField := appfield.AppField{}
	fileField.Type = types.File
	fileField.Code = DocumentFileFieldCode
	fileField.Deleted = false
	fileField.Required = true
	fileField.Array = true
	fileField.View = appfield.FieldView{
		System: true,
	}

	stringFieldData, err := json.Marshal(map[string]string{
		"AdditionalType": "string",
	})
	if err != nil {
		return nil, err
	}

	stringField := appfield.AppField{}
	stringField.Type = types.String
	stringField.Code = DocumentNameFieldCode
	stringField.Required = true
	stringField.View = appfield.FieldView{
		Key:    true,
		System: true,
		Data:   stringFieldData,
	}

	fields := []appfield.AppField{
		fileField,
		stringField,
	}

	return fields, nil
}

func getAppByCode(code string, apps []AppView) *AppView {
	for _, app := range apps {
		if app.Code == code {
			return &app
		}
	}

	return nil
}

func getFieldByCode(code string, fields []appfield.AppField) *appfield.AppField {
	for _, field := range fields {
		if field.Code == code {
			return &field
		}
	}

	return nil
}

// appviewWithCurrentUserPermissions описывает модель для описания appview и прав доступа текущего пользователя
//
//easyjson:json
type appviewWithCurrentUserPermissions struct {
	AppView
	CurrentUserPermissions []permissions.PermissionType `json:"currentUserPermissions"`
}
