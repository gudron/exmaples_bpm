package appview

import (
	"context"
	"encoding/json"
	"time"

	"git.elewise.com/elma365/main/src/app/appfield"
	"git.elewise.com/elma365/main/src/auth"
	"git.elewise.com/elma365/main/src/auth/claims"
	"git.elewise.com/elma365/main/src/collection"
	"git.elewise.com/elma365/main/src/common/queries"
	"git.elewise.com/elma365/main/src/disk"
	"git.elewise.com/elma365/main/src/page"

	"git.elewise.com/elma365/types"

	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
)

type Builder struct {
	str  Store
	ctx  context.Context
	auth *auth.Auth

	userID  uuid.UUID
	ts      time.Time
	appData CreateAppData
	fields  []appfield.AppField
	result  *BuilderResult
}

func NewBuilder(str Store, ctx context.Context, auth *auth.Auth) *Builder {
	return &Builder{str: str, ctx: ctx, auth: auth}
}

func (b *Builder) Create(appData CreateAppData) (res *BuilderResult, err error) {
	defer func() {
		var ok bool
		r := recover()
		if r != nil {
			err, ok = r.(error)
			if !ok {
				panic(r)
			}
		}
	}()

	b.initData(appData)
	b.createCollection()
	b.createAppView()
	b.createPage()
	b.createAppDir()

	return b.result, nil
}

func (b *Builder) initData(appData CreateAppData) {
	b.appData = appData
	b.result = &BuilderResult{}
	b.userID = claims.UserIDFromContext(b.ctx)
	b.ts = time.Now().UTC()
	b.initFields()
}

func (b *Builder) createCollection() {
	id := queries.CollectionID(b.appData.Namespace, b.appData.Code)
	var collectionFields types.Fields
	for _, appField := range b.fields {
		collectionFields = append(collectionFields, appField.Field)
	}
	b.result.Collection = &collection.Collection{
		Namespace:  b.appData.Namespace,
		ID:         id,
		Code:       b.appData.Code,
		Name:       b.appData.Name,
		AccessType: queries.AccessTypeNone,
		Fields:     collectionFields,
		CreatedAt:  b.ts,
		UpdatedAt:  b.ts,
		CreatedBy:  b.userID,
		UpdatedBy:  b.userID,
	}
}

func (b *Builder) createAppView() {
	b.result.AppView = &AppView{
		ID:          b.result.Collection.ID,
		Name:        b.appData.Name,
		ElementName: b.appData.ElementName,
		Code:        b.appData.Code,
		Namespace:   b.appData.Namespace,
		Type:        b.appData.AppViewType,
		Fields:      b.fields,
		CreatedAt:   b.ts,
		UpdatedAt:   b.ts,
		CreatedBy:   b.userID,
		UpdatedBy:   b.userID,
	}
}

func (b *Builder) createPage() {
	b.result.Page = &page.Page{
		ID:        b.result.Collection.ID,
		Name:      b.appData.Name,
		Code:      b.appData.Code,
		Namespace: b.appData.Namespace,
		System:    b.appData.System,
		Sort:      b.appData.Sort,
		Icon:      b.appData.Icon,
		Type:      b.appData.PageType,
		Data:      b.appData.PageData,
		CreatedAt: b.ts,
		UpdatedAt: b.ts,
		CreatedBy: b.userID,
		UpdatedBy: b.userID,
	}
}
func (b *Builder) createAppDir() {
	namespaceDir := b.createIfNotExistsNamespaceDir()
	app := b.result.AppView
	dir := new(disk.Directory)
	dir.ID = app.ID
	dir.Name = app.Code
	dir.Parent = namespaceDir.ID
	if err := b.str.CreateDir(b.ctx, b.auth, dir); err != nil {
		panic(errors.WithStack(err))
	}
}

func (b *Builder) createIfNotExistsNamespaceDir() *disk.Directory {
	app := b.result.AppView
	directories, err := b.str.GetChildrenList(b.ctx, disk.AppDirUUID)
	if err != nil {
		panic(errors.WithStack(err))
	}

	for _, dir := range directories {
		if app.Namespace == dir.Name {
			return &dir
		}
	}
	newDir := new(disk.Directory)
	newDir.Name = app.Namespace
	newDir.Parent = disk.AppDirUUID

	if err = b.str.CreateDir(b.ctx, b.auth, newDir); err != nil {
		panic(errors.WithStack(err))
	}

	return newDir
}

func (b *Builder) initFields() {
	b.fields = []appfield.AppField{}
	if b.appData.AppViewType == Document {
		b.buildDocumentMandatoryFields()
	}
}

func (b *Builder) buildDocumentMandatoryFields() {
	fileField := appfield.AppField{}
	fileField.Type = types.File
	fileField.Code = DocumentFileFieldCode
	fileField.Deleted = false
	fileField.Required = true
	fileField.Array = true
	fileField.View = appfield.FieldView{System: true}
	stringFieldViewData := appfield.FieldViewData{
		AdditionalType: "string",
	}
	stringFieldViewDataJson, err := json.Marshal(stringFieldViewData)
	if err != nil {
		panic(errors.WithStack(err))
	}
	stringField := appfield.AppField{}
	stringField.Type = types.String
	stringField.Code = DocumentNameFieldCode
	stringField.Required = true
	stringField.View = appfield.FieldView{
		Key:    true,
		System: true,
		Data:   stringFieldViewDataJson,
	}

	b.fields = append(b.fields,
		fileField,
		stringField,
	)

}

type BuilderResult struct {
	Page       *page.Page             `json:"page"`
	Collection *collection.Collection `json:"collection"`
	AppView    *AppView               `json:"appView"`
}
