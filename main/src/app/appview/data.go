package appview

//go:generate easyjson data.go

import "encoding/json"

//easyjson:json
type CreateAppData struct {
	Name        string `json:"name" valid:"required"`
	ElementName string `json:"elementName" valid:"required"`
	Code        string `json:"code" valid:"required,code"`
	Namespace   string `json:"namespace" valid:"required,code"`
	AppViewType string `json:"appViewType" valid:"in(STANDARD|DOCUMENT),required"`

	Icon     string          `json:"icon"`
	PageData json.RawMessage `json:"pageData"`
	PageType string          `json:"pageType" valid:"in(PAGE|NAMESPACE|LINK|APPLICATION),required"`
	Sort     int             `json:"sort"`
	System   bool            `json:"system"`
}
