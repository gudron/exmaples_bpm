package filter

//go:generate easyjson filter.go

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"time"

	"github.com/asaskevich/govalidator"
	"github.com/satori/go.uuid"
)

// Filter описывает объект фильтра приложения
//
//easyjson:json
type Filter struct {
	ID        uuid.UUID       `json:"__id"`
	CreatedAt time.Time       `json:"__createdAt"`
	Name      string          `json:"name" valid:"required"`
	Values    json.RawMessage `json:"values"`
}

// Validate валидирует фильтр
func (f *Filter) Validate() error {
	if _, err := govalidator.ValidateStruct(f); err != nil {
		return err
	}

	return nil
}

// Filters описывает массив фильтров приложения
//
//easyjson:json
type Filters []Filter

// Scan implements sql.Scanner interface
func (ff *Filters) Scan(pSrc interface{}) error {
	var afs []Filter

	switch src := pSrc.(type) {
	case nil:
		return nil

	case []byte:
		if err := json.Unmarshal(src, &afs); err != nil {
			return err
		}

	default:
		return fmt.Errorf("Filters.Scan: cannot scan type %T into Filters", pSrc)
	}

	*ff = Filters(afs)
	return nil
}

// Value implements sql.Valuer interface
func (ff Filters) Value() (value driver.Value, err error) {
	return ff.MarshalJSON()
}
