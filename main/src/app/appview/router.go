package appview

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"git.elewise.com/elma365/main/src/app/appfield"
	"git.elewise.com/elma365/main/src/app/appfield/appfields"
	"git.elewise.com/elma365/main/src/app/appview/filter"
	"git.elewise.com/elma365/main/src/app/middleware"
	"git.elewise.com/elma365/main/src/auth"
	"git.elewise.com/elma365/main/src/auth/claims"
	"git.elewise.com/elma365/main/src/auth/pb"
	"git.elewise.com/elma365/main/src/collection"
	"git.elewise.com/elma365/main/src/common/config"
	"git.elewise.com/elma365/main/src/common/errs"
	"git.elewise.com/elma365/main/src/common/tracing"
	"git.elewise.com/elma365/main/src/common/util"

	"git.elewise.com/elma365/types"

	"github.com/asaskevich/govalidator"
	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
	"go.uber.org/zap"
)

// NewRouter is a app endpoint
func NewRouter(cfg *config.Config, auth *auth.Auth, str Store) chi.Router {
	r := chi.NewRouter()
	rtx := r.With(str.WithTx)

	rtx.With(auth.AdministrationGuard).
		Post("/", createApp(str, auth))

	r.With(auth.AdministrationGuard).Get("/{namespace}", getAppviewList(str))

	r.Route("/{namespace}/{code}", func(r chi.Router) {
		r.Use(middleware.WithAppParams)
		ar := r.With(auth.AdministrationGuard)

		r.Get("/", getApp(str, auth))

		r.Get("/permissions-settings", getPermissionsSettings(str))
		ar.Put("/permissions-settings", updatePermissionsSettings(str))

		ar.With(str.WithTx).Put("/fields", updateFields(str))
		ar.Put("/settings", updateSettings(str))

		r.Route("/filters", func(r chi.Router) {
			r.Use(auth.AdministrationGuard)

			r.Post("/", createFilter(str))
			r.Delete("/{id}", deleteFilter(str))
			r.Post("/{id}/rename", renameFilter(str))
		})
	})

	return r
}

func getAppviewList(str Store) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := struct {
			Namespace string `valid:"required"`
		}{
			chi.URLParam(r, "namespace"),
		}
		if _, err := govalidator.ValidateStruct(params); err != nil {
			errs.Handle(l, w, err)
			return
		}
		apps, err := str.GetAppsByNamespace(ctx, params.Namespace)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}
		util.SendData(w, apps, l)
	})
}

func getApp(str Store, auth *auth.Auth) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := middleware.AppParamsFromContext(ctx)

		// Администратор получает любое приложение, в том числе удаленное
		activeOnly := !claims.HasPrivilege(ctx, pb.Privilege_administration)
		app, err := str.QueryApp(ctx, params.Namespace, params.Code, activeOnly)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}
		col, err := str.QueryCollection(ctx, params.Namespace, params.Code)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		ptypes, err := col.GetPermissionTypesForCurrentUser(ctx, str, auth)
		if err != nil {
			errs.Internal(l, w, err)
			return
		}

		// Расширяем AppView для возврата текущих прав доступа
		appModel := appviewWithCurrentUserPermissions{
			AppView:                AppView(*app),
			CurrentUserPermissions: ptypes,
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		je := json.NewEncoder(w)
		if err := je.Encode(appModel); err != nil {
			l.Error(err.Error(), errs.ZapStack(err))
		}
	})
}

func createApp(str Store, auth *auth.Auth) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)

		jd := json.NewDecoder(r.Body)

		d := &CreateAppData{}
		if err := jd.Decode(d); err != nil {
			l.Debug("unmarshal", zap.Error(err))
			http.Error(w, errors.Wrap(err, "unmarshal").Error(), http.StatusBadRequest)
			return
		}
		if _, err := govalidator.ValidateStruct(d); err != nil {
			l.Debug("validate", zap.Error(err))
			http.Error(w, errors.Wrap(err, "validate").Error(), http.StatusBadRequest)
			return
		}
		res, err := str.CreateApp(ctx, str, auth, d)
		if err != nil {

			l.Debug("db:createApp", zap.Error(err))
			if strings.Contains(err.Error(), "pq: duplicate key") {
				http.Error(w, errors.Wrap(err, "db:createApp").Error(), http.StatusConflict)
			} else {
				http.Error(w, errors.Wrap(err, "db:createApp").Error(), http.StatusInternalServerError)
			}
			return
		}
		app := res.AppView

		w.WriteHeader(http.StatusCreated)
		w.Header().Set("Content-Type", "application/json")
		je := json.NewEncoder(w)
		if err := je.Encode(app); err != nil {
			l.Error(err.Error(), errs.ZapStack(err))
		}
	})
}

func createFilter(str Store) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := middleware.AppParamsFromContext(ctx)

		var filter filter.Filter
		jd := json.NewDecoder(r.Body)
		if err := jd.Decode(&filter); err != nil {
			l.Debug("unmarshal", zap.Error(err))
			http.Error(w, errors.Wrap(err, "unmarshal").Error(), http.StatusBadRequest)
			return
		}

		err := filter.Validate()
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		filter.ID = uuid.NewV4()

		app, err := str.GetApp(ctx, params.Namespace, params.Code)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		app.Filters = append(app.Filters, filter)
		err = str.Save(ctx, app)
		if err != nil {
			errs.Internal(l, w, err)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		je := json.NewEncoder(w)
		if err := je.Encode(filter); err != nil {
			errs.Internal(l, w, err)
		}
	})
}

func deleteFilter(str Store) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := middleware.AppParamsFromContext(ctx)
		id := uuid.FromStringOrNil(chi.URLParam(r, "id"))

		app, err := str.GetApp(ctx, params.Namespace, params.Code)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		isFound := false
		for index, filter := range app.Filters {
			if filter.ID == id {
				isFound = true
				app.Filters = append(app.Filters[:index], app.Filters[index+1:]...)
				break
			}
		}

		if !isFound {
			errs.Handle(l, w, errs.ModelNotFound)
			return
		}

		err = str.Save(ctx, app)
		if err != nil {
			errs.Internal(l, w, err)
			return
		}

		w.WriteHeader(http.StatusOK)
	})
}

func renameFilter(str Store) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := middleware.AppParamsFromContext(ctx)
		id := uuid.FromStringOrNil(chi.URLParam(r, "id"))

		var payload filter.Filter
		jd := json.NewDecoder(r.Body)
		if err := jd.Decode(&payload); err != nil {
			http.Error(w, errors.Wrap(err, "unmarshal").Error(), http.StatusBadRequest)
			return
		}

		err := payload.Validate()
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		app, err := str.GetApp(ctx, params.Namespace, params.Code)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		isFound := false
		for index, filter := range app.Filters {
			if filter.ID == id {
				isFound = true
				app.Filters[index].Name = payload.Name
				break
			}
		}

		if !isFound {
			errs.Handle(l, w, errs.ModelNotFound)
			return
		}

		err = str.Save(ctx, app)
		if err != nil {
			errs.Internal(l, w, err)
			return
		}

		w.WriteHeader(http.StatusOK)
	})
}

func updateFields(str Store) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := middleware.AppParamsFromContext(ctx)

		var fields appfields.AppFields
		jd := json.NewDecoder(r.Body)
		if err := jd.Decode(&fields); err != nil {
			l.Debug("unmarshal", zap.Error(err))
			http.Error(w, errors.Wrap(err, "unmarshal").Error(), http.StatusBadRequest)
			return
		}

		err := validateFields(fields)
		if err != nil {
			l.Debug("validate fields", zap.Error(err))
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		app, err := str.GetApp(ctx, params.Namespace, params.Code)
		if err != nil {
			errs.Internal(l, w, err)
			return
		}

		err = app.UpdateFields(ctx, str, fields)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}
	})
}

func updateSettings(str Store) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := middleware.AppParamsFromContext(ctx)

		app, err := str.GetApp(ctx, params.Namespace, params.Code)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}
		app.Settings, err = ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, errors.Wrap(err, "read body").Error(), http.StatusBadRequest)
			return
		}
		if !json.Valid(app.Settings) {
			http.Error(w, errors.Wrap(err, "wrong json format").Error(), http.StatusBadRequest)
			return
		}
		err = str.Save(ctx, app)
		if err != nil {
			errs.Internal(l, w, err)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		appJSON, err := json.Marshal(app)
		if err != nil {
			errs.Internal(l, w, err)
			return
		}
		io.Copy(w, bytes.NewBuffer(appJSON))
	})
}

func getPermissionsSettings(str Store) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := middleware.AppParamsFromContext(ctx)

		// Проверяем есть ли доступ до приложения
		_, err := str.QueryApp(ctx, params.Namespace, params.Code, true)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		settings, err := str.GetCollectionPermissionsSettings(ctx, params.Namespace, params.Code)
		if err != nil {
			errs.Internal(l, w, err)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		je := json.NewEncoder(w)
		if err := je.Encode(settings); err != nil {
			l.Error(err.Error(), errs.ZapStack(err))
		}
	}
}

func updatePermissionsSettings(str Store) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := middleware.AppParamsFromContext(ctx)

		settings := collection.PermissionsSettings{}
		jd := json.NewDecoder(r.Body)
		if err := jd.Decode(&settings); err != nil {
			l.Debug("unmarshal", zap.Error(err))
			http.Error(w, errors.Wrap(err, "unmarshal").Error(), http.StatusBadRequest)
			return
		}

		if err := settings.Validate(); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}

		// Проверяем есть ли доступ до приложения
		_, err := str.QueryApp(ctx, params.Namespace, params.Code, true)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		col, err := str.QueryCollection(ctx, params.Namespace, params.Code)
		if err != nil {
			errs.Internal(l, w, err)
			return
		}

		err = str.UpdateCollectionPermissionsSettings(ctx, col, settings)
		if err != nil {
			errs.Internal(l, w, err)
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}

func validateFields(fields appfields.AppFields) error {
	for _, field := range fields {
		if _, err := govalidator.ValidateStruct(field); err != nil {
			return errors.Wrap(err, "validate")
		}

		if err := field.ValidateDefault(); err != nil {
			return errors.Wrap(err, "validate field default value")
		}
	}

	if err := validateCodes(fields); err != nil {
		return err
	}

	return validateKeyFields(fields)
}

// validateCodes проверяет уникальность кодов полей
func validateCodes(fields []appfield.AppField) error {
	var codes = make(map[string]struct{})
	for _, field := range fields {
		if len(field.Code) == 0 {
			return errors.New("field code can't be empty")
		}
		if _, exists := codes[field.Code]; exists {
			return errors.Errorf("duplicate field code <%s>", field.Code)
		}
		codes[field.Code] = struct{}{}
	}

	return nil
}

// validateKeyFields проверяет допустимость ключевых полей
func validateKeyFields(fields []appfield.AppField) error {
	isKeyDefined := false
	for _, field := range fields {
		// Удаленные поля не проверяем на ключи
		if field.View.Key && !field.Deleted {
			// Проверка допустимости установки признака ключевого поля
			if field.Type != types.String {
				return errors.Errorf("field <%s> not allowed to be key field", field.Code)
			}

			// Проверка уникальности ключевого поля
			if isKeyDefined {
				return errors.Errorf("multiple key fields found")
			}

			isKeyDefined = true
		}
	}

	if !isKeyDefined {
		return errors.New("key field should be defined")
	}

	return nil
}
