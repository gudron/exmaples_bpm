package appview

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"git.elewise.com/elma365/main/src/app/appfield"
	"git.elewise.com/elma365/main/src/app/middleware"
	"git.elewise.com/elma365/main/src/common/errs"
	_ "git.elewise.com/elma365/main/src/common/validator"

	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
)

func TestCreateApp(t *testing.T) {
	tests := []struct {
		testname string
		testcode int
		testbody string

		name        string
		elementName string
		code        string
		namespace   string
		appViewType string
		pageType    string
		pageData    json.RawMessage

		duplicate bool
	}{
		{
			testname:    "no data",
			name:        "",
			elementName: "",
			code:        "",
			namespace:   "",
			appViewType: "",
			pageType:    "",
			pageData:    json.RawMessage("{}"),
			testcode:    http.StatusBadRequest,
			testbody:    "validate: name: non zero value required;elementName: non zero value required;code: non zero value required;namespace: non zero value required;appViewType: non zero value required;pageType: non zero value required;\n",
		},
		{
			testname:    "invalid data",
			name:        "a",
			elementName: "a",
			code:        "ab",
			namespace:   "cb",
			appViewType: "INVALIDTYPE",
			pageType:    "INVALIDTYPE",
			pageData:    json.RawMessage("{}"),
			testcode:    http.StatusBadRequest,
			testbody:    "validate: appViewType: INVALIDTYPE does not validate as in(STANDARD|DOCUMENT);pageType: INVALIDTYPE does not validate as in(PAGE|NAMESPACE|LINK|APPLICATION);\n",
		},
		{
			testname:    "ok",
			name:        "CV",
			elementName: "CV",
			code:        "CV",
			namespace:   "HR",
			appViewType: "STANDARD",
			pageType:    "NAMESPACE",
			pageData:    json.RawMessage(`{"redirectTo":null,"widgets":[],"columns":1}`),
			testcode:    http.StatusCreated,
		},
		{
			testname:    "duplicate",
			name:        "CV",
			elementName: "CV",
			code:        "CV",
			namespace:   "HR",
			appViewType: "STANDARD",
			pageType:    "NAMESPACE",
			pageData:    json.RawMessage("{}"),
			testcode:    http.StatusConflict,
			testbody:    "db:createApp: pq: duplicate key value violates unique constraint \"collection_code_namespace_uindex\"\n",

			duplicate: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.testname, func(t *testing.T) {
			mctrl := gomock.NewController(t)
			defer mctrl.Finish()

			str := NewMockStore(mctrl)
			appdata := &CreateAppData{
				Name:        tt.name,
				ElementName: tt.elementName,
				Namespace:   tt.namespace,
				Code:        tt.code,
				AppViewType: tt.appViewType,
				PageType:    tt.pageType,
				PageData:    tt.pageData,
			}
			builderResult := &BuilderResult{}
			call := str.EXPECT().
				CreateApp(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Eq(appdata)).
				AnyTimes()
			if tt.duplicate {
				call.Return(nil, errors.New(`pq: duplicate key value violates unique constraint "collection_code_namespace_uindex"`))
			} else {
				call.Return(builderResult, nil)
			}

			h := createApp(str, nil)

			w := httptest.NewRecorder()
			r := httptest.NewRequest(http.MethodPost, "/no-matter", bytes.NewBufferString(
				fmt.Sprintf(
					`{"name": %q, "elementName": %q, "code": %q, "namespace": %q, "appViewType": %q, "pageType": %q, "pageData": %s}`,
					tt.name, tt.elementName, tt.code, tt.namespace, tt.appViewType, tt.pageType, tt.pageData),
			))
			h(w, r)

			assert.Equal(t, tt.testcode, w.Code)
			if tt.testcode != http.StatusCreated {
				assert.Equal(t, tt.testbody, w.Body.String())
			} else {
				assert.Equal(t, tt.testcode, w.Code)
			}
		})
	}
}

func TestUpdateFields(t *testing.T) {
	tests := []struct {
		testname string
		testcode int
		testbody string

		code      string
		namespace string
		name      string
		fields    json.RawMessage
		exists    bool

		getAppfields json.RawMessage
	}{
		{
			testname:  "update non existant collection",
			name:      "newname",
			fields:    json.RawMessage("null"),
			namespace: "HR",
			code:      "CV",
			exists:    false,
			testcode:  http.StatusBadRequest,
			testbody:  "key field should be defined\n",
		},
		{
			testname:  "update fields",
			code:      "CV",
			namespace: "HR",
			testcode:  http.StatusOK,
			testbody:  "",
			exists:    true,
			fields: json.RawMessage(`[
				{
					"code": "test0code",
					"type": "STRING",
					"view": {
						"key": true
					}
				},
				{
					"code": "test1code",
					"type": "MONEY",
					"indexed": true,
					"deleted": false,
					"array": false,
					"required": false,
					"view": {
						"sort": 1,
						"name": "test1",
						"tooltip": "heyhey",
						"data": {
							"a": "b"
						}
					}
				}
			]`),
		},
		{
			testname:  "field default value",
			code:      "CV",
			namespace: "HR",
			testcode:  http.StatusOK,
			testbody:  "",
			exists:    true,
			fields: json.RawMessage(`[
				{
					"code": "test0code",
					"type": "STRING",
					"view": {
						"key": true
					}
				},
				{
					"code": "test1code",
					"type": "INTEGER",
					"defaultValue": 3
				}
			]`),
		},
		{
			testname:  "invalid field default value",
			code:      "CV",
			namespace: "HR",
			testcode:  http.StatusBadRequest,
			testbody:  "validate field default value: test1code: cannot unmarshal number into field of type STRING\n",
			exists:    true,
			fields: json.RawMessage(`[
				{
					"code": "test1code",
					"type": "STRING",
					"defaultValue": 3,
					"view": {
						"key": true
					}
				}
			]`),
		},
		{
			testname:  "duplicate fields code",
			code:      "CV",
			namespace: "HR",
			testcode:  http.StatusBadRequest,
			testbody:  "duplicate field code <dup>\n",
			exists:    true,
			fields: json.RawMessage(`[
				{
					"code": "dup",
					"type": "STRING"
				},
				{
					"code": "dup",
					"type": "MONEY"
				}
			]`),
		},
		{
			testname:  "missing key field",
			code:      "CV",
			namespace: "HR",
			testcode:  http.StatusBadRequest,
			testbody:  "key field should be defined\n",
			exists:    true,
			fields: json.RawMessage(`[
				{
					"code": "code1",
					"type": "STRING"
				}
			]`),
		},
		{
			testname:  "multiple key fields",
			code:      "CV",
			namespace: "HR",
			testcode:  http.StatusBadRequest,
			testbody:  "multiple key fields found\n",
			exists:    true,
			fields: json.RawMessage(`[
				{
					"code": "code1",
					"type": "STRING",
					"view": {
						"key": true
					}
				},
				{
					"code": "code2",
					"type": "STRING",
					"view": {
						"key": true
					}
				}
			]`),
		},
		{
			testname:  "invalid type for key field",
			code:      "CV",
			namespace: "HR",
			testcode:  http.StatusBadRequest,
			testbody:  "field <code1> not allowed to be key field\n",
			exists:    true,
			fields: json.RawMessage(`[
				{
					"code": "code1",
					"type": "FLOAT",
					"view": {
						"key": true
					}
				}
			]`),
		},
		{
			testname:  "unable to change type for already saved field",
			code:      "CV",
			namespace: "HR",
			testcode:  http.StatusBadRequest,
			testbody:  "\"field '__collection_CV' already saved and should have type 'SYS_COLLECTION'\"\n",
			exists:    true,
			fields: json.RawMessage(`[
				{
					"code": "code1",
					"type": "STRING",
					"view": {
						"key": true
					}
				},
				{
					"code": "__collection_CV",
					"type": "STRING"
				}
			]`),
			getAppfields: json.RawMessage(`[
				{
					"code": "code1",
					"type": "STRING",
					"view": {
						"key": true
					}
				},
				{
					"code": "__collection_CV",
					"type": "SYS_COLLECTION"
				}
			]`),
		},
	}
	for _, tt := range tests {
		t.Run(tt.testname, func(t *testing.T) {
			mctrl := gomock.NewController(t)
			defer mctrl.Finish()

			str := NewMockStore(mctrl)

			var fields []appfield.AppField
			if tt.getAppfields != nil {
				err := json.Unmarshal(tt.getAppfields, &fields)
				assert.NoError(t, err)
			} else {
				err := json.Unmarshal(tt.fields, &fields)
				assert.NoError(t, err)
			}

			callGetApp := str.EXPECT().GetApp(gomock.Any(), tt.namespace, tt.code).AnyTimes()
			callGetApp.Return(&AppView{
				Code:      tt.code,
				Namespace: tt.namespace,
				Fields:    fields,
			}, nil)

			callGetAppsByNamespace := str.EXPECT().GetAppsByNamespace(gomock.Any(), tt.namespace).AnyTimes()
			callGetAppsByNamespace.Return([]AppView{
				{
					Code: tt.code,
				},
				{
					Code: tt.code + "2", // Всегда возвращаем второй апп с заранее известным кодом
				},
			}, nil)

			callSaveFields := str.EXPECT().SaveFields(gomock.Any(), gomock.Any()).AnyTimes()
			if tt.exists {
				callSaveFields.Return(nil)
			} else {
				callSaveFields.Return(errs.ModelNotFound)
			}

			h := updateFields(str)
			w := httptest.NewRecorder()
			r := httptest.NewRequest(http.MethodPut, "/no-matter", bytes.NewBufferString(fmt.Sprintf(`%s`, tt.fields)))

			params := middleware.AppParams{
				Namespace: tt.namespace,
				Code:      tt.code,
			}
			ctx := middleware.ContextWithAppParams(r.Context(), params)
			h(w, r.WithContext(ctx))

			assert.Equal(t, tt.testcode, w.Code)
			if tt.testcode != http.StatusOK {
				assert.Equal(t, tt.testbody, w.Body.String())
			} else {
				assert.Equal(t, tt.testcode, w.Code)
				assert.Empty(t, w.Body.String())
			}
		})
	}
}

func TestUpdateSysCollectionFields(t *testing.T) {
	tests := []struct {
		testname string
		testcode int
		testbody string

		code      string
		namespace string
		name      string
		fields    json.RawMessage

		getAppfields json.RawMessage
	}{
		{
			testname:  "missing array field",
			code:      "CV",
			namespace: "HR",
			testcode:  http.StatusBadRequest,
			testbody:  "\"field 'link' array value should be true\"\n",
			fields: json.RawMessage(`[
				{
					"code": "code1",
					"type": "STRING",
					"view": {
						"key": true
					}
				},
				{
					"code": "link",
					"type": "SYS_COLLECTION",
					"data": {
						"single": false
					}
				}
			]`),
		},
		{
			testname:  "code NOT from current namespace",
			code:      "CV",
			namespace: "HR",
			testcode:  http.StatusBadRequest,
			testbody:  "\"linked application 'not-existing-code' not found in namespace 'HR'\"\n",
			fields: json.RawMessage(`[
				{
					"code": "code1",
					"type": "STRING",
					"view": {
						"key": true
					}
				},
				{
					"code": "link",
					"type": "SYS_COLLECTION",
					"array": true,
					"single": false,
					"data": {
						"namespace": "HR",
						"code": "not-existing-code"
					}
				}
			]`),
		},
		{
			testname:  "single field changed from false to true",
			code:      "CV",
			namespace: "HR",
			testcode:  http.StatusBadRequest,
			testbody:  "\"field 'link' 'single' property can't be changed\"\n",
			fields: json.RawMessage(`[
				{
					"code": "code1",
					"type": "STRING",
					"view": {
						"key": true
					}
				},
				{
					"code": "link",
					"type": "SYS_COLLECTION",
					"array": true,
					"single": true,
					"data": {
						"namespace": "namespace",
						"code": "code"
					}
				}
			]`),
			getAppfields: json.RawMessage(`[
				{
					"code": "code1",
					"type": "STRING",
					"view": {
						"key": true
					}
				},
				{
					"code": "link",
					"type": "SYS_COLLECTION",
					"array": true,
					"single": false,
					"data": {
						"namespace": "namespace",
						"code": "code"
					}
				}
			]`),
		},
		{
			testname:  "valid type system collection",
			code:      "CV",
			namespace: "HR",
			testcode:  http.StatusOK,
			testbody:  "",
			fields: json.RawMessage(`[
				{
					"code": "code1",
					"type": "STRING",
					"view": {
						"key": true
					}
				},
				{
					"code": "link",
					"type": "SYS_COLLECTION",
					"array": true,
					"single": false,
					"data": {
						"namespace": "HR",
						"code": "CV2"
					}
				}
			]`),
		},
	}
	for _, tt := range tests {
		t.Run(tt.testname, func(t *testing.T) {
			mctrl := gomock.NewController(t)
			defer mctrl.Finish()

			str := NewMockStore(mctrl)

			var fields []appfield.AppField
			if tt.getAppfields != nil {
				err := json.Unmarshal(tt.getAppfields, &fields)
				assert.NoError(t, err)
			} else {
				err := json.Unmarshal(tt.fields, &fields)
				assert.NoError(t, err)
			}

			callGetApp := str.EXPECT().GetApp(gomock.Any(), tt.namespace, tt.code).AnyTimes()
			callGetApp.Return(&AppView{
				Code:      tt.code,
				Namespace: tt.namespace,
				Fields:    fields,
			}, nil)

			callGetAppsByNamespace := str.EXPECT().GetAppsByNamespace(gomock.Any(), tt.namespace).AnyTimes()
			callGetAppsByNamespace.Return([]AppView{
				{
					Code: tt.code,
				},
				{
					Code: tt.code + "2", // Всегда возвращаем второй апп с заранее известным кодом
				},
			}, nil)

			str.EXPECT().SaveFields(gomock.Any(), gomock.Any()).AnyTimes().Return(nil)

			h := updateFields(str)
			w := httptest.NewRecorder()
			r := httptest.NewRequest(http.MethodPut, "/no-matter", bytes.NewBufferString(fmt.Sprintf(`%s`, tt.fields)))

			params := middleware.AppParams{
				Namespace: tt.namespace,
				Code:      tt.code,
			}
			ctx := middleware.ContextWithAppParams(r.Context(), params)
			h(w, r.WithContext(ctx))

			assert.Equal(t, tt.testcode, w.Code)
			if tt.testcode != http.StatusOK {
				assert.Equal(t, tt.testbody, w.Body.String())
			} else {
				assert.Equal(t, tt.testcode, w.Code)
				assert.Empty(t, w.Body.String())
			}
		})
	}
}
