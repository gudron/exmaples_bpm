package settings

//go:generate easyjson settings.go

import (
	"encoding/json"

	"github.com/satori/go.uuid"
)

// ApplicationSettings - настройки приложения
//
// easyjson:json
type ApplicationSettings struct {
	ListCard             json.RawMessage      `json:"listCard"`
	UserGuide            json.RawMessage      `json:"userGuide"`
	AdvancedForms        json.RawMessage      `json:"advancedForms"`
	ListSettings         ListSettings         `json:"listSettings"`
	ViewSettings         ViewSettings         `json:"viewSettings"`
	DocumentSettings     json.RawMessage      `json:"documentSettings"`
	RegistrationSettings RegistrationSettings `json:"registrationSettings"`
	Status               StatusSettings       `json:"status"`
}

// RegistrationSettings - настройки регистрации
//
// easyjson:json
type RegistrationSettings struct {
	Enabled                  bool   `json:"enabled"`
	Template                 string `json:"template"`
	ManualGenerate           bool   `json:"manualGenerate"`
	ManualGenerateByTemplate bool   `json:"manualGenerateByTemplate"`
	ManualTemplate           string `json:"manualTemplate"`
	ManualChangeEnabled      bool   `json:"manualChangeEnabled"`
	ReserveEnabled           bool   `json:"reserveEnabled"`
	SerialCode               string `json:"serialCode"`
	EditAfterRegistration    bool   `json:"editAfterRegistration"`
	ReserveOnCreate          bool   `json:"reserveOnCreate"`
}

// ListSettings - настройки списочного представления
//
// easyjson:json
type ListSettings struct {
	Buttons Buttons `json:"buttons"`
}

// ViewSettings - настройки представления элемента
//
// easyjson:json
type ViewSettings struct {
	Buttons Buttons `json:"buttons"`
}

// Buttons - набор кнопок
//
// easyjson:json
type Buttons []ProcessButton

// ProcessButton - кнопка запуска процесса
//
// easyjson:json
type ProcessButton struct {
	Label          string     `json:"label"`
	Type           string     `json:"type"`
	Namespace      string     `json:"namespace"`
	Code           string     `json:"code"`
	ProcessID      *uuid.UUID `json:"processId"`
	UseSimpleForm  bool       `json:"useSimpleForm"`
	SimpleFormSize string     `json:"simpleFormSize"`
	Permission     *string    `json:"permission"`
}
