package settings

import (
	"fmt"

	"git.elewise.com/elma365/main/src/common/errs"
	statusValue "git.elewise.com/elma365/types/complextypes/status"
)

//go:generate easyjson status.go

const (
	// JumpMethodAll - статус можно сменить на любой
	JumpMethodAll = "ALL"
	// JumpMethodNext - статус можно сменить только на один из следующих
	JumpMethodNext = "NEXT"
	// JumpMethodToStatuses - статус можно сменить только на статус из списка (StatusItem.JumpToStatuses)
	JumpMethodToStatuses = "TO_STATUSES"
)

// StatusSettings - struct of status field settings
//
//easyjson:json
type StatusSettings struct {
	Enabled      bool         `json:"enabled"`
	ManualChange bool         `json:"manualChange"`
	NextID       int32        `json:"nextId"`
	Items        []StatusItem `json:"items"`
}

// StatusItem - struct of status variant
//
//easyjson:json
type StatusItem struct {
	ID             uint32   `json:"id"`
	Name           string   `json:"name"`
	Event          string   `json:"event"`
	IsFinished     bool     `json:"isFinished"`
	JumpMethod     string   `json:"jumpMethod"`
	JumpToStatuses []uint32 `json:"jumpToStatuses"`
}

// GetDefaultItem возвращает статус по умолчанию
func (settings *StatusSettings) GetDefaultItem() (*StatusItem, error) {
	if len(settings.Items) == 0 {
		return nil, errs.ModelCollision.WithData("status items not found")
	}
	defaultItem := settings.Items[0]
	return &defaultItem, nil
}

// GetItemByStatusValue возвращает статус по значению
func (settings *StatusSettings) GetItemByStatusValue(value statusValue.Status) (*StatusItem, error) {
	for _, item := range settings.Items {
		if item.ID == value.Status {
			return &item, nil
		}
	}
	return nil, errs.ModelNotFound.WithData("status field item not found")
}

// IsNewStatusAllowed - проверка возможности сменить статус currentStatus на newStatus
func (settings *StatusSettings) IsNewStatusAllowed(currentStatus StatusItem, newStatus StatusItem) (bool, error) {
	if currentStatus.ID == newStatus.ID {
		return true, nil
	}
	switch currentStatus.JumpMethod {
	case JumpMethodAll:
		return true, nil
	case JumpMethodNext:
		var currentPosition uint32
		var newPosition uint32
		for i, item := range settings.Items {
			if item.ID == currentStatus.ID {
				currentPosition = uint32(i + 1)
			}
			if item.ID == newStatus.ID {
				newPosition = uint32(i + 1)
			}
		}
		if currentPosition == 0 || newPosition == 0 {
			return false, errs.ModelCollision.WithData("invalid status")
		}
		if currentPosition > newPosition {
			message := fmt.Sprintf("unable to change status from %s to %s", currentStatus.Name, newStatus.Name)
			return false, errs.ModelCollision.WithData(message)
		}
		return true, nil
	case JumpMethodToStatuses:
		for _, newID := range currentStatus.JumpToStatuses {
			if newID == newStatus.ID {
				return true, nil
			}
		}
		message := fmt.Sprintf("unable to change status from %s to %s", currentStatus.Name, newStatus.Name)
		return false, errs.ModelCollision.WithData(message)
	}
	return false, errs.ModelNotFound.WithData("invalid jump method")
}
