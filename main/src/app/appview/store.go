package appview

//go:generate mockgen -source=store.go -destination=store_mock_test.go -package=appview

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"git.elewise.com/elma365/main/src/app/appfield"
	"git.elewise.com/elma365/main/src/auth"
	"git.elewise.com/elma365/main/src/auth/claims"
	"git.elewise.com/elma365/main/src/collection"
	"git.elewise.com/elma365/main/src/common/errs"
	"git.elewise.com/elma365/main/src/common/event"
	"git.elewise.com/elma365/main/src/common/mw"
	"git.elewise.com/elma365/main/src/common/queries"
	"git.elewise.com/elma365/main/src/common/tracing"
	"git.elewise.com/elma365/main/src/common/util"
	"git.elewise.com/elma365/main/src/disk"
	"git.elewise.com/elma365/main/src/page"
	uuid "github.com/satori/go.uuid"

	"git.elewise.com/elma365/types"

	"github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

const (
	// NS is a appview namespace
	NS = event.NSSystem
	// Code is a appview code
	Code = "appview"
	// Table name in postgres
	Table util.TableName = "appviews"
)

// InnerStore describes items strore uniq methods
type InnerStore interface {
	GetApp(ctx context.Context, namespace, code string) (*AppView, error)
	QueryApp(ctx context.Context, namespace, code string, activeOnly bool) (*AppView, error)
	CreateApp(context.Context, Store, *auth.Auth, *CreateAppData) (*BuilderResult, error)
	CreateAppview(ctx context.Context, appview *AppView) error
	SaveFields(context.Context, []AppView) error
	Save(context.Context, *AppView) error
	SaveAppview(ctx context.Context, app *AppView) error
	GetAppByNamespaceCode(ctx context.Context, namespace string, code string) (*AppView, error)
	GetAppsByNamespace(ctx context.Context, namespace string) ([]AppView, error)
	GetAppsByIds(ctx context.Context, ids []string) ([]AppView, error)
	DeleteApps(ctx context.Context, apps []AppView) error
	DeleteAppsByIds(ctx context.Context, ids []string) error
	RecoverAppsByIds(ctx context.Context, ids []string) error
	SearchAppsByIds(ctx context.Context, ids []string) ([]AppView, error)
}

// OuterStore describes shares methods
type OuterStore interface {
	WithTx(next http.Handler) http.Handler

	GetCollection(ctx context.Context, namespace, code string) (*collection.Collection, error)
	QueryCollection(ctx context.Context, namespace, code string) (*collection.Collection, error)
	GetCollectionPermissionsSettings(ctx context.Context, namespace, code string) (*collection.PermissionsSettings, error)
	UpdateCollectionPermissionsSettings(ctx context.Context, col *collection.Collection, settings collection.PermissionsSettings) error
	CreateCollection(ctx context.Context, col *collection.Collection) error

	CreatePage(ctx context.Context, pg *page.Page) error

	QueryDirsInDir(context.Context, string) ([]disk.Directory, error)
	GetChildrenList(ctx context.Context, parentID uuid.UUID) ([]disk.Directory, error)
	CreateDir(context.Context, disk.Auth, *disk.Directory) error
}

// Store describes all store methods
type Store interface {
	InnerStore
	OuterStore
}

// AppStore is a methods to work with apps
type AppStore struct {
	dbx *sqlx.DB
	sbt squirrel.StatementBuilderType
	qc  *queries.Client
}

// NewStore creates app store
func NewStore(dbx *sqlx.DB, sbt squirrel.StatementBuilderType, qc *queries.Client) *AppStore {
	return &AppStore{dbx, sbt, qc}
}

// CreateAppview - просто создание аппа и отправка события об этом. Названа так, т.к. CreateApp занято
func (s *AppStore) CreateAppview(ctx context.Context, appview *AppView) error {
	tx := mw.TxFromContext(ctx)
	company := mw.CompanyFromContext(ctx) //+

	query, args, err := s.sbt.
		Insert(fmt.Sprintf("%q.appviews", company)).
		Columns("id", "name", "element_name", "code", "namespace", "type", "fields", "created_at", "updated_at", "created_by", "updated_by").
		Values(appview.ID, appview.Name, appview.ElementName, appview.Code, appview.Namespace, appview.Type, appview.Fields, appview.CreatedAt, appview.UpdatedAt, appview.CreatedBy, appview.UpdatedBy).
		ToSql()
	if err != nil {
		return errors.WithStack(err)
	}
	_, err = tx.ExecContext(ctx, query, args...)
	if err != nil {
		return errors.WithStack(err)
	}

	rmqItems := []event.Item{event.Item{
		Code: "appview",
		ID:   appview.ID,
		NS:   event.NSSystem,
		Type: event.CollectionItemType,
		Data: appview,
	}}
	if err := event.Save(ctx, tx, appview.UpdatedAt, rmqItems); err != nil {
		return errors.WithStack(err)
	}
	return nil
}

// CreateApp creates collection, appview and page for elma app
func (s *AppStore) CreateApp(ctx context.Context, str Store, auth *auth.Auth, appdata *CreateAppData) (*BuilderResult, error) {
	var builderResult *BuilderResult
	err := tracing.WithDBSpan(ctx, "CreateApp", func(ctx context.Context) error {
		// Создать все необходимые сущности
		builder := NewBuilder(str, ctx, auth)
		var err error
		builderResult, err = builder.Create(*appdata)

		if err != nil {
			return errors.WithStack(err)
		}

		// Сохранить коллекцию
		err = str.CreateCollection(ctx, builderResult.Collection)
		if err != nil {
			return errors.WithStack(err)
		}

		// сохранить appview
		if err := s.CreateAppview(ctx, builderResult.AppView); err != nil {
			return errors.WithStack(err)
		}

		// сохранить page
		if err := str.CreatePage(ctx, builderResult.Page); err != nil {
			return errors.WithStack(err)
		}

		return nil
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}
	return builderResult, nil
}

func (s *AppStore) createAppDir(ctx context.Context, str Store, auth *auth.Auth, app AppView) (*disk.Directory, error) {
	namespaceDir, err := s.createIfNotExistsNamespaceDir(ctx, str, auth, app)
	if err != nil {
		return nil, err
	}

	dir := new(disk.Directory)
	dir.ID = app.ID
	dir.Name = app.Code
	dir.Parent = namespaceDir.ID
	if err = str.CreateDir(ctx, auth, dir); err != nil {
		return nil, err
	}

	return dir, nil
}

func (s *AppStore) createIfNotExistsNamespaceDir(ctx context.Context, str Store, auth *auth.Auth, app AppView) (*disk.Directory, error) {

	directoies, err := str.QueryDirsInDir(ctx, disk.AppDirUUID.String())
	if err != nil {
		return nil, err
	}

	for _, dir := range directoies {
		if app.Namespace == dir.Name {
			return &dir, nil
		}
	}

	newDir := new(disk.Directory)
	newDir.Name = app.Namespace
	newDir.Parent = disk.AppDirUUID

	if err = str.CreateDir(ctx, auth, newDir); err != nil {
		return nil, err
	}

	return newDir, nil
}

// Save сохраняет приложение
func (s *AppStore) SaveAppview(ctx context.Context, app *AppView) error {
	return tracing.WithDBSpan(ctx, "SaveAppview", func(ctx context.Context) error {
		tx := mw.TxFromContext(ctx)
		// Сохраняем поля в приложение
		query, args, err := s.sbt.
			Update(Table.WithSchema(ctx)).
			Set("fields", app.Fields).
			Set("filters", app.Filters).
			Set("settings", app.Settings).
			Set("updated_at", app.UpdatedAt).
			Set("updated_by", app.UpdatedBy).
			Where(squirrel.Eq{"id": app.ID}).
			ToSql()
		if err != nil {
			return errors.WithStack(err)
		}

		_, err = tx.ExecContext(ctx, query, args...)
		if err != nil {
			return errors.WithStack(err)
		}

		var rmqItems []event.Item
		rmqItems = append(rmqItems, event.Item{
			Code: "appview",
			ID:   app.ID,
			NS:   event.NSSystem,
			Type: event.CollectionItemType,
			Data: app,
		})
		if err := event.Save(ctx, tx, app.UpdatedAt, rmqItems); err != nil {
			return errors.WithStack(err)
		}
		return nil
	})
}

// Save сохраняет приложение
func (s *AppStore) Save(ctx context.Context, app *AppView) error {
	return tracing.WithDBSpan(ctx, "Save", func(ctx context.Context) error {
		userID := claims.UserIDFromContext(ctx)

		tx, err := s.dbx.BeginTxx(ctx, nil)
		if err != nil {
			return errors.WithStack(err)
		}
		defer tx.Rollback()

		if err := util.EnsureNotChangedAfter(ctx, tx, Table, app.ID, app.UpdatedAt); err != nil {
			return errors.WithStack(err)
		}

		app.UpdatedAt = time.Now().UTC()

		// Сохраняем поля в приложение
		query, args, err := s.sbt.
			Update(Table.WithSchema(ctx)).
			Set("fields", app.Fields).
			Set("filters", app.Filters).
			Set("settings", app.Settings).
			Set("updated_at", app.UpdatedAt).
			Set("updated_by", userID).
			Where(squirrel.Eq{"namespace": app.Namespace}).
			Where(squirrel.Eq{"code": app.Code}).
			Where(squirrel.Eq{"deleted_at": nil}).
			ToSql()
		if err != nil {
			return errors.WithStack(err)
		}

		_, err = tx.ExecContext(ctx, query, args...)
		if err != nil {
			return errors.WithStack(err)
		}

		var rmqItems []event.Item
		rmqItems = append(rmqItems, event.Item{
			Code: "appview",
			ID:   app.ID,
			NS:   event.NSSystem,
			Type: event.CollectionItemType,
			Data: app,
		})
		if err := event.Save(ctx, tx, app.UpdatedAt, rmqItems); err != nil {
			return err
		}
		return errors.WithStack(tx.Commit())
	})
}

// SaveFields updates fields for specified applications
func (s *AppStore) SaveFields(ctx context.Context, apps []AppView) error {
	return tracing.WithDBSpan(ctx, "SaveFields", func(ctx context.Context) error {
		if len(apps) == 0 {
			return nil
		}
		tx := mw.TxFromContext(ctx)

		ts := time.Now().UTC()
		userID := claims.UserIDFromContext(ctx)
		var rmqItems []event.Item

		for _, app := range apps {
			app.UpdatedAt = ts
			app.UpdatedBy = userID

			appEvent, err := s.saveFieldsForApplication(ctx, app, tx)
			if err != nil {
				return err
			}
			rmqItems = append(rmqItems, *appEvent)

			collEvent, err := s.saveFieldsForCollection(ctx, app, tx)
			if err != nil {
				return err
			}
			rmqItems = append(rmqItems, *collEvent)
		}

		if err := event.Save(ctx, tx, ts, rmqItems); err != nil {
			return err
		}

		return nil
	})

}

// GetApp gets application by namespace and code
func (s *AppStore) GetApp(ctx context.Context, namespace, code string) (*AppView, error) {
	company := mw.CompanyFromContext(ctx)

	var appview AppView
	query, args, err := s.sbt.
		Select("id", "name", "element_name", "code", "namespace", "type", "fields", "filters", "settings",
			"created_at", "updated_at", "deleted_at", "created_by", "updated_by").
		From(fmt.Sprintf("%q.appviews", company)).
		Where(squirrel.Eq{"namespace": namespace}).
		Where(squirrel.Eq{"code": code}).
		Where(squirrel.Eq{"deleted_at": nil}).
		ToSql()
	if err != nil {
		return nil, errors.WithStack(err)
	}

	err = s.dbx.GetContext(ctx, &appview, query, args...)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errs.ModelNotFound
		}
		return nil, errors.WithStack(err)
	}

	return &appview, nil
}

// QueryApp gets application by namespace and code from elasticsearch
func (s *AppStore) QueryApp(ctx context.Context, namespace, code string, activeOnly bool) (*AppView, error) {
	id := queries.CollectionID(namespace, code).String()
	request := queries.NewCollectionItemRequest("system", "appview", id)
	request.Active = activeOnly
	res, err := request.Execute(ctx, s.qc)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	var app = AppView{}
	err = json.Unmarshal(*res.Item, &app)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return &app, nil
}

// GetAppsByNamespace gets applications by namespace
func (s *AppStore) GetAppsByNamespace(ctx context.Context, namespace string) ([]AppView, error) {
	apps := []AppView{}
	err := tracing.WithDBSpan(ctx, "GetAppsByNamespace", func(ctx context.Context) error {
		company := mw.CompanyFromContext(ctx)

		query, args, err := s.sbt.
			Select("id, name, element_name, code, namespace, type, fields, settings, created_at, updated_at, deleted_at, created_by, updated_by").
			From(fmt.Sprintf("%q.appviews", company)).
			Where(squirrel.Eq{"namespace": namespace}).
			Where(squirrel.Eq{"deleted_at": nil}).
			ToSql()
		if err != nil {
			return errors.WithStack(err)
		}

		return errors.WithStack(s.dbx.SelectContext(ctx, &apps, query, args...))
	})

	return apps, err
}

// GetAppByNamespaceCode gets applications by namespace and code
func (s *AppStore) GetAppByNamespaceCode(ctx context.Context, namespace string, code string) (*AppView, error) {
	apps := []AppView{}
	err := tracing.WithDBSpan(ctx, "GetAppsByNamespace", func(ctx context.Context) error {
		company := mw.CompanyFromContext(ctx)

		query, args, err := s.sbt.
			Select("id, name, element_name, code, namespace, type, fields, settings, created_at, updated_at, deleted_at, created_by, updated_by").
			From(fmt.Sprintf("%q.appviews", company)).
			Where(squirrel.Eq{"namespace": namespace}).
			Where(squirrel.Eq{"code": code}).
			Where(squirrel.Eq{"deleted_at": nil}).
			ToSql()
		if err != nil {
			return errors.WithStack(err)
		}

		return errors.WithStack(s.dbx.SelectContext(ctx, &apps, query, args...))
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}
	if len(apps) == 0 {
		return nil, errs.ModelNotFound
	}
	return &apps[0], nil
}

// SearchAppsByIds gets application by ids
func (s *AppStore) SearchAppsByIds(ctx context.Context, ids []string) ([]AppView, error) {
	if len(ids) == 0 {
		return nil, nil
	}

	request := queries.NewCollectionItemsRequest(NS, Code)
	request.Filter.Ids = ids
	request.Filter.Active = false

	res, err := request.Execute(ctx, s.qc)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	apps := make([]AppView, res.Total)
	for index, item := range res.Items {
		var app AppView
		err = json.Unmarshal(*item, &app)
		if err != nil {
			return nil, errors.WithStack(err)
		}

		apps[index] = app
	}

	return apps, nil
}

// GetAppsByIds gets application by ids
func (s *AppStore) GetAppsByIds(ctx context.Context, ids []string) ([]AppView, error) {
	if len(ids) == 0 {
		return nil, nil
	}

	request := queries.NewCollectionItemsRequest(NS, Code)
	request.Filter.Ids = ids
	request.Filter.Active = true

	res, err := request.Execute(ctx, s.qc)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	apps := make([]AppView, res.Total)
	for index, item := range res.Items {
		var app AppView
		err = json.Unmarshal(*item, &app)
		if err != nil {
			return nil, errors.WithStack(err)
		}

		apps[index] = app
	}

	if res.Total != int64(len(ids)) {
		// Вернем ИД ненайденных элементов в данных ошибки
		missingIds := make([]string, 0)
		for _, id := range ids {
			isMissing := true
			for _, app := range apps {
				if id == app.ID.String() {
					isMissing = false
				}
			}

			if isMissing {
				missingIds = append(missingIds, id)
			}
		}

		return nil, errors.WithStack(errs.ModelNotFound.WithData(missingIds))
	}

	return apps, nil
}

// RecoverApps восстанавливает из корзины заданные приложения
func (s *AppStore) RecoverApps(ctx context.Context, apps []AppView) error {
	return tracing.WithDBSpan(ctx, "DeleteApps", func(ctx context.Context) error {
		tx := mw.TxFromContext(ctx)

		ts := time.Now().UTC()
		userID := claims.UserIDFromContext(ctx)
		events := make([]event.Item, len(apps))

		for index, app := range apps {
			err := util.EnsureNotChangedAfter(ctx, tx, Table, app.ID, ts)
			if err != nil {
				return err
			}

			query, args, err := s.sbt.
				Update(Table.WithSchema(ctx)).
				Set("deleted_at", nil).
				Set("updated_at", ts).
				Set("updated_by", userID).
				Where(squirrel.Eq{"id": app.ID}).
				ToSql()
			if err != nil {
				return errors.WithStack(err)
			}

			_, err = tx.ExecContext(ctx, query, args...)
			if err != nil {
				return errors.WithStack(err)
			}

			app.UpdatedAt = ts
			app.UpdatedBy = userID
			app.DeletedAt = nil

			events[index] = event.Item{
				NS:   NS,
				Code: Code,
				ID:   app.ID,
				Type: event.CollectionItemType,
				Data: app,
			}
		}

		err := event.Save(ctx, tx, ts, events)
		if err != nil {
			return errors.WithStack(err)
		}

		return nil
	})
}

// RecoverAppsByIds удаляет в корзину приложения по заданным ИД
func (s *AppStore) RecoverAppsByIds(ctx context.Context, ids []string) error {
	apps, err := s.SearchAppsByIds(ctx, ids)
	if err != nil {
		return errors.WithStack(err)
	}
	if len(apps) == 0 {
		return nil
	}
	return s.RecoverApps(ctx, apps)
}

// DeleteApps удаляет в корзину заданные приложения
func (s *AppStore) DeleteApps(ctx context.Context, apps []AppView) error {
	return tracing.WithDBSpan(ctx, "DeleteApps", func(ctx context.Context) error {
		tx := mw.TxFromContext(ctx)

		ts := time.Now().UTC()
		userID := claims.UserIDFromContext(ctx)
		events := make([]event.Item, len(apps))

		for index, app := range apps {
			err := util.EnsureNotChangedAfter(ctx, tx, Table, app.ID, ts)
			if err != nil {
				return err
			}

			query, args, err := s.sbt.
				Update(Table.WithSchema(ctx)).
				Set("deleted_at", ts).
				Set("updated_at", ts).
				Set("updated_by", userID).
				Where(squirrel.Eq{"id": app.ID}).
				ToSql()
			if err != nil {
				return errors.WithStack(err)
			}

			_, err = tx.ExecContext(ctx, query, args...)
			if err != nil {
				return errors.WithStack(err)
			}

			app.UpdatedAt = ts
			app.UpdatedBy = userID
			app.DeletedAt = &ts

			events[index] = event.Item{
				NS:   NS,
				Code: Code,
				ID:   app.ID,
				Type: event.CollectionItemType,
				Data: app,
			}
		}

		err := event.Save(ctx, tx, ts, events)
		if err != nil {
			return errors.WithStack(err)
		}

		return nil
	})
}

// DeleteAppsByIds удаляет в корзину приложения по заданным ИД
func (s *AppStore) DeleteAppsByIds(ctx context.Context, ids []string) error {
	apps, err := s.GetAppsByIds(ctx, ids)
	if err != nil {
		return err
	}

	return s.DeleteApps(ctx, apps)
}

func (s *AppStore) saveFieldsForApplication(ctx context.Context, app AppView, tx *sqlx.Tx) (*event.Item, error) {
	company := mw.CompanyFromContext(ctx)
	userID := claims.UserIDFromContext(ctx)

	err := util.EnsureNotChangedAfter(ctx, tx, Table, app.ID, app.UpdatedAt)
	if err != nil {
		return nil, err
	}

	// Сохраняем поля в приложение
	query, args, err := s.sbt.
		Update(fmt.Sprintf("%q.appviews", company)).
		Set("fields", app.Fields).
		Set("updated_at", app.UpdatedAt).
		Set("updated_by", userID).
		Where(squirrel.Eq{"namespace": app.Namespace}).
		Where(squirrel.Eq{"code": app.Code}).
		Where(squirrel.Eq{"deleted_at": nil}).
		ToSql()
	if err != nil {
		return nil, errors.WithStack(err)
	}

	_, err = tx.ExecContext(ctx, query, args...)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return &event.Item{
		Code: "appview",
		ID:   app.ID,
		NS:   event.NSSystem,
		Type: event.CollectionItemType,
		Data: app,
	}, nil
}

func (s *AppStore) saveFieldsForCollection(ctx context.Context, app AppView, tx *sqlx.Tx) (*event.Item, error) {
	// Сохраняем поля в приложение
	company := mw.CompanyFromContext(ctx)

	// Получаем справочник
	var collection collection.Collection
	query, args, err := s.sbt.
		Select("id", "name", "code", "namespace", "access_type", "fields", "created_at", "updated_at", "deleted_at").
		From(fmt.Sprintf("%q.collections", company)).
		Where(squirrel.Eq{"namespace": app.Namespace}).
		Where(squirrel.Eq{"code": app.Code}).
		Where(squirrel.Eq{"deleted_at": nil}).
		ToSql()
	if err != nil {
		return nil, errors.WithStack(err)
	}

	err = tx.GetContext(ctx, &collection, query, args...)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errs.ModelNotFound
		}
		return nil, errors.WithStack(err)
	}

	collection.UpdatedAt = app.UpdatedAt
	collection.UpdatedBy = app.UpdatedBy

	// Cохраняем поля в справочник
	var collectionFields types.Fields
	for _, appField := range app.Fields {
		collectionFields = append(collectionFields, appField.Field)
	}
	collection.Fields = collectionFields

	query, args, err = s.sbt.
		Update(fmt.Sprintf("%q.collections", company)).
		Set("fields", collectionFields).
		Set("updated_at", collection.UpdatedAt).
		Set("updated_by", collection.UpdatedBy).
		Where(squirrel.Eq{"namespace": app.Namespace}).
		Where(squirrel.Eq{"code": app.Code}).
		Where(squirrel.Eq{"deleted_at": nil}).
		ToSql()
	if err != nil {
		return nil, errors.WithStack(err)
	}

	_, err = tx.ExecContext(ctx, query, args...)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return &event.Item{
		Code: "collection",
		ID:   collection.ID,
		NS:   event.NSSystem,
		Type: event.CollectionItemType,
		Data: collection,
	}, nil
}

func getDeletedFields(oldfields, newfields []appfield.AppField) []appfield.AppField {
	var deletedFields []appfield.AppField

ofLoop:
	for _, of := range oldfields {
		for _, nf := range newfields {
			if nf.Code == of.Code {
				continue ofLoop
			}
		}
		of.Deleted = true
		deletedFields = append(deletedFields, of)
	}

	return deletedFields
}
