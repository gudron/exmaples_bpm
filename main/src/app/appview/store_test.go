package appview

import (
	"testing"

	"git.elewise.com/elma365/main/src/app/appfield"
	"git.elewise.com/elma365/types"

	"github.com/stretchr/testify/assert"
)

func TestGetDeletedFields(t *testing.T) {
	oldFields := []appfield.AppField{
		{
			Field: types.Field{
				Code:    "f1",
				Type:    types.String,
				Deleted: false,
			},
			View: appfield.FieldView{
				Name: "f1name",
			},
		},
		{
			Field: types.Field{
				Code:    "f2",
				Type:    types.String,
				Deleted: false,
			},
			View: appfield.FieldView{
				Name: "f2name",
			},
		},
	}

	newFields := []appfield.AppField{
		{
			Field: types.Field{
				Code:    "f3",
				Type:    types.String,
				Deleted: false,
			},
			View: appfield.FieldView{
				Name: "f3name",
			},
		},
		{
			Field: types.Field{
				Code:    "f4",
				Type:    types.String,
				Deleted: false,
			},
			View: appfield.FieldView{
				Name: "f4name",
			},
		},
	}

	resultFields := []appfield.AppField{
		{
			Field: types.Field{
				Code:    "f1",
				Type:    types.String,
				Deleted: true,
			},
			View: appfield.FieldView{
				Name: "f1name",
			},
		},
		{
			Field: types.Field{
				Code:    "f2",
				Type:    types.String,
				Deleted: true,
			},
			View: appfield.FieldView{
				Name: "f2name",
			},
		},
		{
			Field: types.Field{
				Code:    "f3",
				Type:    types.String,
				Deleted: false,
			},
			View: appfield.FieldView{
				Name: "f3name",
			},
		},
		{
			Field: types.Field{
				Code:    "f4",
				Type:    types.String,
				Deleted: false,
			},
			View: appfield.FieldView{
				Name: "f4name",
			},
		},
	}

	deletedFields := getDeletedFields(oldFields, newFields)

	resFields := newFields
	resFields = append(deletedFields, resFields...)

	assert.EqualValues(t, resFields, resultFields, "should be equal")
}
