package item

//go:generate easyjson item.go
//go:generate mockgen -source=item.go -destination=item_mock_test.go -package=item

import (
	"context"
	"encoding/json"
	"strings"
	"time"

	"git.elewise.com/elma365/main/src/app/appfield"
	"git.elewise.com/elma365/main/src/app/appfield/appfields"
	"git.elewise.com/elma365/main/src/auth"
	"git.elewise.com/elma365/main/src/auth/claims"
	"git.elewise.com/elma365/main/src/collection"
	"git.elewise.com/elma365/main/src/collection/roles"
	"git.elewise.com/elma365/main/src/common/errs"
	"git.elewise.com/elma365/main/src/common/event"
	"git.elewise.com/elma365/main/src/common/permissions"
	"git.elewise.com/elma365/main/src/common/queries"
	"git.elewise.com/elma365/main/src/common/util"

	"git.elewise.com/elma365/types/complextypes/status"

	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
)

const (
	// Table name in postgres
	Table util.TableName = "items"

	// ID field name for elastic
	ID = "__id"

	// CreatedAt field name for elastic
	CreatedAt = "__createdAt"

	// UpdatedAt field name for elastic
	UpdatedAt = "__updatedAt"

	// DeletedAt field name for elastic
	DeletedAt = "__deletedAt"

	// CreatedBy field name for elastic
	CreatedBy = "__createdBy"

	// UpdatedBy field name for elastic
	UpdatedBy = "__updatedBy"

	// Permissions field name for elastic
	Permissions = "__permissions"

	// CurrentUserPermissions virtual field
	CurrentUserPermissions = "__currentUserPermissions"
)

var SystemFieldCodes = []string{
	ID,
	CreatedAt,
	UpdatedAt,
	DeletedAt,
	CreatedBy,
	UpdatedBy,
	Permissions,
	CurrentUserPermissions,
}

// StatusItem is an element of data
//
//easyjson:json
type Item struct {
	ID                  uuid.UUID       `db:"id" json:"__id"`
	CollectionNamespace string          `db:"collection_namespace" json:"collectionNamespace"`
	CollectionCode      string          `db:"collection_code" json:"collectionCode"`
	Body                json.RawMessage `db:"body" json:"body"`
	CreatedAt           time.Time       `db:"created_at" json:"__createdAt"`
	UpdatedAt           time.Time       `db:"updated_at" json:"__updatedAt"`
	DeletedAt           *time.Time      `db:"deleted_at" json:"__deletedAt"`
	CreatedBy           uuid.UUID       `db:"created_by" json:"__createdBy"`
	UpdatedBy           uuid.UUID       `db:"updated_by" json:"__updatedBy"`
}

// NewItem создает новый элемент на основе json
func NewItem(namespace, code string, raw json.RawMessage) (*Item, error) {
	var item Item
	err := json.Unmarshal(raw, &item)
	if err != nil {
		return nil, err
	}
	item.CollectionNamespace = namespace
	item.CollectionCode = code
	item.Body = raw

	return &item, nil
}

// Update обновляет элемент
func (item *Item) Update(ctx context.Context, auth *auth.Auth, str Store, payload map[string]json.RawMessage) error {
	// Проверка прав
	permissionType := permissions.UPDATE
	if item.CreatedAt.IsZero() {
		permissionType = permissions.CREATE
	}
	err := item.ensureHasPermission(ctx, str, auth, permissionType)
	if err != nil {
		return err
	}

	app, err := str.GetApp(ctx, item.CollectionNamespace, item.CollectionCode)
	if err != nil {
		return err
	}

	// Удаление полей, недоступных для изменения, на случай если пользователь их передал
	for _, systemFieldCode := range SystemFieldCodes {
		delete(payload, systemFieldCode)
	}

	// Получаем видимые и не удаленные поля
	realFields := make([]appfield.AppField, 0)
	for _, field := range app.Fields {
		if !field.Deleted && !field.View.Hidden {
			realFields = append(realFields, field)
		}
	}
	// Валидируем типы полей
	var realAppFields appfields.AppFields = realFields
	err = realAppFields.Validate(payload)
	if err != nil {
		return errs.ValidationError.WithData(err.Error())
	}

	// Получение измененных связанных элементов
	linkedItems, err := item.getChangedLinkedItems(ctx, str, payload, realAppFields)
	if err != nil {
		return err
	}

	if err := item.applyPayload(payload); err != nil {
		return errors.WithStack(err)
	}
	items := []Item{*item}
	items = append(items, linkedItems...)

	defaultPermissions := item.getDefaultPermissions()
	updatedItems, err := str.SaveItems(ctx, items, defaultPermissions)
	if err != nil {
		if strings.Contains(err.Error(), "pq: duplicate key") {
			return errs.ModelCollision.WithData(err.Error())
		}
		return err
	}

	*item = updatedItems[0]
	return nil
}

// UpdateStatus устанавливает элементу статус
func (item *Item) UpdateStatus(ctx context.Context, auth *auth.Auth, str Store, newStatus status.Status) error {
	err := item.ensureHasPermission(ctx, str, auth, permissions.UPDATE)
	if err != nil {
		return err
	}

	canSet, err := item.canSetStatus(ctx, str, newStatus)
	if err != nil {
		return err
	}
	if !canSet {
		return errs.ModelCollision.WithData("status not allowed")
	}

	data, err := json.Marshal(newStatus)
	if err != nil {
		return errors.WithStack(err)
	}
	item.setFieldValue(StatusFieldCode, data)

	_, err = str.SaveItems(ctx, []Item{*item}, nil)
	return err
}

// UpdatePermissions обновляет права доступа элемента
func (item *Item) UpdatePermissions(ctx context.Context, auth *auth.Auth, str Store,
	newPermissions permissions.Permissions) error {

	err := item.ensureHasPermission(ctx, str, auth, permissions.ASSIGN)
	if err != nil {
		return err
	}

	err = str.SaveItemPermissions(ctx, item, newPermissions)
	if err != nil {
		return err
	}

	return nil
}

// HasPermission проверяет наличи заданного права у текущего пользователя на элемент
func (item *Item) HasPermission(ctx context.Context, str dataStore, auth authStore, ptype permissions.PermissionType) (
	bool, error) {

	col, err := str.QueryCollection(ctx, item.CollectionNamespace, item.CollectionCode)
	if err != nil {
		return false, err
	}

	hasPermission, err := col.HasPermission(ctx, auth, str, ptype)
	if err != nil {
		return false, err
	}
	if hasPermission {
		// Доступ разрешен на уровне справочника
		return true, nil
	}

	if ptype == permissions.CREATE {
		// Права на создание элемента задаются на уровне справочника. Поэтому дальнейшие проверки не имею смысла.
		return false, nil
	}

	if col.AccessType == queries.AccessTypeCollection {
		// Для доступа к "на уровне таблицы" не учитываются ни права автора ни элемента
		return false, nil
	}

	settings, err := str.GetCollectionPermissionsSettings(ctx, item.CollectionNamespace, item.CollectionCode)
	if err != nil {
		return false, err
	}

	// Проверяем автора
	hasCreatorPermission, err := item.isCurrentUserHasCreatorPermission(ctx, auth, ptype, settings.Permissions)
	if err != nil {
		return false, err
	}
	if hasCreatorPermission {
		return true, nil
	}

	currentUserID := claims.UserIDFromContext(ctx)
	perms, err := str.GetItemPermissions(ctx, item.CollectionNamespace, item.CollectionCode, item.ID.String())
	if err != nil {
		return false, err
	}
	// Проверим наличие заданного права для текущего пользователя
	hasItemPermission, err := auth.HasPermission(ctx, currentUserID, perms.Values, ptype)
	if err != nil {
		return false, err
	}

	return hasItemPermission, nil
}

// GetPermissionTypesForCurrentUser получение объединенных типов прав доступа для текущего пользователя
func (item *Item) GetPermissionTypesForCurrentUser(ctx context.Context, str dataStore, auth authStore,
	excludeItemPermissions bool, col *collection.Collection) ([]permissions.PermissionType, error) {

	if col == nil {
		var err error
		col, err = str.QueryCollection(ctx, item.CollectionNamespace, item.CollectionCode)
		if err != nil {
			return nil, err
		}
	}

	// Получим права текущего пользоателя на справочник
	collectionPTypes, err := col.GetPermissionTypesForCurrentUser(ctx, str, auth)
	if err != nil {
		return nil, err
	}
	if col.AccessType != queries.AccessTypeRow || arePermissionsContain(collectionPTypes, permissions.FullAccess) {
		return collectionPTypes, nil
	}

	settings, err := str.GetCollectionPermissionsSettings(ctx, item.CollectionNamespace, item.CollectionCode)
	if err != nil {
		return nil, err
	}

	// Получим права автора
	creatorPTypes, err := item.getCreatorPermissionTypesForCurrentUser(ctx, auth, settings.Permissions)
	if err != nil {
		return nil, err
	}

	itemPTypes := make([]permissions.PermissionType, 0)
	if !excludeItemPermissions {
		itemPerms, err := item.getPermissionsFromBodyOrStore(ctx, str)
		if err != nil {
			return nil, err
		}

		userID := claims.UserIDFromContext(ctx)
		itemPTypes, err = auth.GetUserPermissionTypes(ctx, userID, itemPerms.Values)
		if err != nil {
			return nil, err
		}
	}

	// Объединим права справочника и элемента
	collectionMask := permissions.PTypesToMask(collectionPTypes)
	creatorMask := permissions.PTypesToMask(creatorPTypes)
	itemMask := permissions.PTypesToMask(itemPTypes)
	totalMask := collectionMask | creatorMask | itemMask

	return permissions.MaskToPTypes(totalMask), nil
}

// PrepareEvent creates event
func (item *Item) PrepareEvent() event.Item {
	return event.Item{
		ID:   item.ID,
		NS:   item.CollectionNamespace,
		Code: item.CollectionCode,
		Data: item.Body,
		Type: event.CollectionItemType,
	}
}

func (item *Item) applyPayload(payload map[string]json.RawMessage) error {
	data := map[string]json.RawMessage{}
	if item.Body != nil {
		err := json.Unmarshal(item.Body, &data)
		if err != nil {
			return errors.WithStack(err)
		}
	}

	for key, value := range payload {
		data[key] = value
	}

	body, err := json.Marshal(data)
	if err != nil {
		return errors.WithStack(err)
	}
	item.Body = body

	return nil
}

func (item *Item) getDefaultPermissions() *permissions.Permissions {
	// Записываем не null в хранилище, что бы по умолчанию эти записи не были видны всем пользователям.
	// См. реализацию выборки списка по правам в /src/common/queries/collection.go
	return &permissions.Permissions{
		InheritParent: true,
	}
}

func (item *Item) ensureHasPermission(ctx context.Context, str Store, auth *auth.Auth,
	ptype permissions.PermissionType) error {

	hasPermission, err := item.HasPermission(ctx, str, auth, ptype)
	if err != nil {
		return err
	}

	if !hasPermission {
		return errs.PermissionDenied
	}

	return nil
}

func (item *Item) getCreatorPermissionTypesForCurrentUser(ctx context.Context, auth authStore,
	perms *permissions.Permissions) ([]permissions.PermissionType, error) {

	currentUserID := claims.UserIDFromContext(ctx)
	isParent, err := auth.IsUserParentForEmployee(ctx, currentUserID, item.CreatedBy)
	if err != nil {
		return nil, err
	}
	if !isParent {
		return nil, nil
	}

	for _, permission := range perms.Values {
		if permission.Group.Type == permissions.Role && uuid.Equal(permission.Group.ID, roles.Creator) {
			return permission.Types, nil
		}
	}

	return nil, nil
}

func (item *Item) isCurrentUserHasCreatorPermission(ctx context.Context, auth authStore, ptype permissions.PermissionType,
	perms *permissions.Permissions) (bool, error) {

	creatorPTypes, err := item.getCreatorPermissionTypesForCurrentUser(ctx, auth, perms)
	if err != nil {
		return false, err
	}
	if creatorPTypes == nil {
		return false, nil
	}

	for _, creatorPType := range creatorPTypes {
		if ptype == creatorPType {
			return true, nil
		}
	}
	return false, nil
}

func (item *Item) getPermissionsFromBodyOrStore(ctx context.Context, str dataStore) (*permissions.Permissions, error) {
	if item.Body != nil {
		// Проверим наличие прав в данных текущего элемента
		var itemMap map[string]json.RawMessage
		err := json.Unmarshal(item.Body, &itemMap)
		if err != nil {
			return nil, err
		}

		permsJSON, exists := itemMap[Permissions]
		if exists {
			var itemPerms *permissions.Permissions
			err := json.Unmarshal(permsJSON, itemPerms)
			if err != nil {
				return nil, err
			}
			return itemPerms, nil
		}
	}

	return str.GetItemPermissions(ctx, item.CollectionNamespace, item.CollectionCode, item.ID.String())
}

func arePermissionsContain(list, perms []permissions.PermissionType) bool {
	listMask := permissions.PTypesToMask(list)
	permsMask := permissions.PTypesToMask(perms)
	return listMask&permsMask == permsMask
}

type dataStore interface {
	QueryCollection(ctx context.Context, namespace, code string) (*collection.Collection, error)
	GetCollectionPermissionsSettings(ctx context.Context, namespace, code string) (*collection.PermissionsSettings, error)
	GetItemPermissions(ctx context.Context, namespace, code, id string) (*permissions.Permissions, error)
}

type authStore interface {
	HasPermission(ctx context.Context, userID uuid.UUID, perms []permissions.Permission,
		ptypes ...permissions.PermissionType) (bool, error)
	IsUserParentForEmployee(ctx context.Context, userID uuid.UUID, employeeID uuid.UUID) (bool, error)
	GetUserPermissionTypes(ctx context.Context, userID uuid.UUID, perms []permissions.Permission) (
		[]permissions.PermissionType, error)
}
