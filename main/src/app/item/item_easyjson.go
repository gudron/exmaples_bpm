// Code generated by easyjson for marshaling/unmarshaling. DO NOT EDIT.

package item

import (
	json "encoding/json"
	easyjson "github.com/mailru/easyjson"
	jlexer "github.com/mailru/easyjson/jlexer"
	jwriter "github.com/mailru/easyjson/jwriter"
	time "time"
)

// suppress unused package warning
var (
	_ *json.RawMessage
	_ *jlexer.Lexer
	_ *jwriter.Writer
	_ easyjson.Marshaler
)

func easyjsonA80d3b19DecodeGitElewiseComElma365MainSrcAppItem(in *jlexer.Lexer, out *Item) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "__id":
			if data := in.UnsafeBytes(); in.Ok() {
				in.AddError((out.ID).UnmarshalText(data))
			}
		case "collectionNamespace":
			out.CollectionNamespace = string(in.String())
		case "collectionCode":
			out.CollectionCode = string(in.String())
		case "body":
			if data := in.Raw(); in.Ok() {
				in.AddError((out.Body).UnmarshalJSON(data))
			}
		case "__createdAt":
			if data := in.Raw(); in.Ok() {
				in.AddError((out.CreatedAt).UnmarshalJSON(data))
			}
		case "__updatedAt":
			if data := in.Raw(); in.Ok() {
				in.AddError((out.UpdatedAt).UnmarshalJSON(data))
			}
		case "__deletedAt":
			if in.IsNull() {
				in.Skip()
				out.DeletedAt = nil
			} else {
				if out.DeletedAt == nil {
					out.DeletedAt = new(time.Time)
				}
				if data := in.Raw(); in.Ok() {
					in.AddError((*out.DeletedAt).UnmarshalJSON(data))
				}
			}
		case "__createdBy":
			if data := in.UnsafeBytes(); in.Ok() {
				in.AddError((out.CreatedBy).UnmarshalText(data))
			}
		case "__updatedBy":
			if data := in.UnsafeBytes(); in.Ok() {
				in.AddError((out.UpdatedBy).UnmarshalText(data))
			}
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjsonA80d3b19EncodeGitElewiseComElma365MainSrcAppItem(out *jwriter.Writer, in Item) {
	out.RawByte('{')
	first := true
	_ = first
	if !first {
		out.RawByte(',')
	}
	first = false
	out.RawString("\"__id\":")
	out.RawText((in.ID).MarshalText())
	if !first {
		out.RawByte(',')
	}
	first = false
	out.RawString("\"collectionNamespace\":")
	out.String(string(in.CollectionNamespace))
	if !first {
		out.RawByte(',')
	}
	first = false
	out.RawString("\"collectionCode\":")
	out.String(string(in.CollectionCode))
	if !first {
		out.RawByte(',')
	}
	first = false
	out.RawString("\"body\":")
	out.Raw((in.Body).MarshalJSON())
	if !first {
		out.RawByte(',')
	}
	first = false
	out.RawString("\"__createdAt\":")
	out.Raw((in.CreatedAt).MarshalJSON())
	if !first {
		out.RawByte(',')
	}
	first = false
	out.RawString("\"__updatedAt\":")
	out.Raw((in.UpdatedAt).MarshalJSON())
	if !first {
		out.RawByte(',')
	}
	first = false
	out.RawString("\"__deletedAt\":")
	if in.DeletedAt == nil {
		out.RawString("null")
	} else {
		out.Raw((*in.DeletedAt).MarshalJSON())
	}
	if !first {
		out.RawByte(',')
	}
	first = false
	out.RawString("\"__createdBy\":")
	out.RawText((in.CreatedBy).MarshalText())
	if !first {
		out.RawByte(',')
	}
	first = false
	out.RawString("\"__updatedBy\":")
	out.RawText((in.UpdatedBy).MarshalText())
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v Item) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjsonA80d3b19EncodeGitElewiseComElma365MainSrcAppItem(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v Item) MarshalEasyJSON(w *jwriter.Writer) {
	easyjsonA80d3b19EncodeGitElewiseComElma365MainSrcAppItem(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *Item) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjsonA80d3b19DecodeGitElewiseComElma365MainSrcAppItem(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *Item) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjsonA80d3b19DecodeGitElewiseComElma365MainSrcAppItem(l, v)
}
