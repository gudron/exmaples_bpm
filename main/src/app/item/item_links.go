package item

import (
	"context"
	"encoding/json"

	"git.elewise.com/elma365/main/src/app/appfield"
	"git.elewise.com/elma365/main/src/app/appfield/syscollection"

	"git.elewise.com/elma365/types"

	"github.com/pkg/errors"
)

// getChangedLinkedItems получает оригиналы связанных элементов, в случе изменения связей
func (item *Item) getChangedLinkedItems(ctx context.Context, str Store, payload map[string]json.RawMessage,
	fields []appfield.AppField) ([]Item, error) {

	sysCollectionFields := make([]appfield.AppField, 0)
	for _, field := range fields {
		if field.Type == types.SysCollection {
			sysCollectionFields = append(sysCollectionFields, field)
		}
	}

	itemsIds := make([]string, 0)
	changes := make([]linkChange, 0)

	for _, field := range sysCollectionFields {
		var fieldData syscollection.Data
		err := json.Unmarshal(field.Data, &fieldData)
		if err != nil {
			return nil, errors.Wrapf(err, "can't unmarshal field <%s> data", field.Code)
		}

		if len(fieldData.LinkedFieldCode) == 0 {
			// Связанное поле не установлено. В этом случае не создаем обратных связей.
			continue
		}

		createdIds, deletedIds, err := getChangedLinkedItemIdsForField(field, item.Body, payload)
		if err != nil {
			return nil, errors.WithStack(err)
		}

		if len(createdIds) == 0 && len(deletedIds) == 0 {
			// Значение поля не поменялось
			continue
		}
		itemsIds = append(itemsIds, createdIds...)
		itemsIds = append(itemsIds, deletedIds...)

		backLinkField, err := item.getLinkedField(ctx, str, field, fieldData)
		if err != nil {
			return nil, err
		}

		changes = append(changes, linkChange{fieldData, *backLinkField, createdIds, deletedIds})
	}

	if len(changes) == 0 {
		return []Item{}, nil
	}

	// Получим все измененные элементы одним запросом и построим мапу по ним
	items, err := str.GetItemsByIds(ctx, itemsIds)
	if err != nil {
		return nil, err
	}
	itemsCache := map[string]Item{}
	for _, item := range items {
		itemsCache[item.ID.String()] = item
	}

	changesBuilder := linksChanges{changes, item.ID.String(), itemsCache}
	return changesBuilder.Apply()
}

// getChangedLinkedItemIdsForField возвращает идентификаторы созданных и удаленных (из текушего элемента)
// связанынных элементов
func getChangedLinkedItemIdsForField(field appfield.AppField, body json.RawMessage, payload map[string]json.RawMessage) (
	createdIds []string, deletedIds []string, err error) {

	// Получение текущих связанных элементов
	originValues := make([]string, 0) // Всегда храним массив
	if body != nil {
		// Элемент может быть еще не сохранен
		var bodyMap map[string]json.RawMessage
		err := json.Unmarshal(body, &bodyMap)
		if err != nil {
			return nil, nil, errors.WithStack(err)
		}

		if _, exists := bodyMap[field.Code]; exists {
			// Поля еще может не быть
			err = json.Unmarshal(bodyMap[field.Code], &originValues)
			if err != nil {
				return nil, nil, errors.WithStack(err)
			}
		}
	}

	// Получение новых связанных элементов
	updatedValues := make([]string, 0)
	if _, exists := payload[field.Code]; exists {
		err := json.Unmarshal(payload[field.Code], &updatedValues)
		if err != nil {
			return nil, nil, errors.WithStack(err)
		}
	}

	createdIds, deletedIds = getArraysDiff(updatedValues, originValues)
	return createdIds, deletedIds, nil
}

// getLinkedField получает обратное поле связанного справочника
func (item *Item) getLinkedField(ctx context.Context, str Store, field appfield.AppField, fieldData syscollection.Data) (
	*appfield.AppField, error) {

	linkedApp, err := str.GetApp(ctx, fieldData.Namespace, fieldData.Code)
	if err != nil {
		return nil, err
	}

	var linkedField *appfield.AppField
	for _, field := range linkedApp.Fields {
		if field.Code == fieldData.LinkedFieldCode {
			linkedField = &field
			break
		}
	}
	if linkedField == nil {
		return nil, errors.Errorf("field <%s> not found", fieldData.LinkedFieldCode)
	}

	return linkedField, nil
}

func getArraysDiff(newValues, baseValues []string) (created, deleted []string) {
	created = make([]string, 0)
	for _, newValue := range newValues {
		if !isStringInSlice(newValue, baseValues) {
			created = append(created, newValue)
		}
	}

	deleted = make([]string, 0)
	for _, baseValue := range baseValues {
		if !isStringInSlice(baseValue, newValues) {
			deleted = append(deleted, baseValue)
		}
	}

	return created, deleted
}

func isStringInSlice(value string, elements []string) bool {
	for _, element := range elements {
		if element == value {
			return true
		}
	}
	return false
}
