// Code generated by MockGen. DO NOT EDIT.
// Source: item.go

package item

import (
	context "context"
	collection "git.elewise.com/elma365/main/src/collection"
	permissions "git.elewise.com/elma365/main/src/common/permissions"
	gomock "github.com/golang/mock/gomock"
	go_uuid "github.com/satori/go.uuid"
	reflect "reflect"
)

// MockdataStore is a mock of dataStore interface
type MockdataStore struct {
	ctrl     *gomock.Controller
	recorder *MockdataStoreMockRecorder
}

// MockdataStoreMockRecorder is the mock recorder for MockdataStore
type MockdataStoreMockRecorder struct {
	mock *MockdataStore
}

// NewMockdataStore creates a new mock instance
func NewMockdataStore(ctrl *gomock.Controller) *MockdataStore {
	mock := &MockdataStore{ctrl: ctrl}
	mock.recorder = &MockdataStoreMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (_m *MockdataStore) EXPECT() *MockdataStoreMockRecorder {
	return _m.recorder
}

// QueryCollection mocks base method
func (_m *MockdataStore) QueryCollection(ctx context.Context, namespace string, code string) (*collection.Collection, error) {
	ret := _m.ctrl.Call(_m, "QueryCollection", ctx, namespace, code)
	ret0, _ := ret[0].(*collection.Collection)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// QueryCollection indicates an expected call of QueryCollection
func (_mr *MockdataStoreMockRecorder) QueryCollection(arg0, arg1, arg2 interface{}) *gomock.Call {
	return _mr.mock.ctrl.RecordCallWithMethodType(_mr.mock, "QueryCollection", reflect.TypeOf((*MockdataStore)(nil).QueryCollection), arg0, arg1, arg2)
}

// GetCollectionPermissionsSettings mocks base method
func (_m *MockdataStore) GetCollectionPermissionsSettings(ctx context.Context, namespace string, code string) (*collection.PermissionsSettings, error) {
	ret := _m.ctrl.Call(_m, "GetCollectionPermissionsSettings", ctx, namespace, code)
	ret0, _ := ret[0].(*collection.PermissionsSettings)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetCollectionPermissionsSettings indicates an expected call of GetCollectionPermissionsSettings
func (_mr *MockdataStoreMockRecorder) GetCollectionPermissionsSettings(arg0, arg1, arg2 interface{}) *gomock.Call {
	return _mr.mock.ctrl.RecordCallWithMethodType(_mr.mock, "GetCollectionPermissionsSettings", reflect.TypeOf((*MockdataStore)(nil).GetCollectionPermissionsSettings), arg0, arg1, arg2)
}

// GetItemPermissions mocks base method
func (_m *MockdataStore) GetItemPermissions(ctx context.Context, namespace string, code string, id string) (*permissions.Permissions, error) {
	ret := _m.ctrl.Call(_m, "GetItemPermissions", ctx, namespace, code, id)
	ret0, _ := ret[0].(*permissions.Permissions)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetItemPermissions indicates an expected call of GetItemPermissions
func (_mr *MockdataStoreMockRecorder) GetItemPermissions(arg0, arg1, arg2, arg3 interface{}) *gomock.Call {
	return _mr.mock.ctrl.RecordCallWithMethodType(_mr.mock, "GetItemPermissions", reflect.TypeOf((*MockdataStore)(nil).GetItemPermissions), arg0, arg1, arg2, arg3)
}

// MockauthStore is a mock of authStore interface
type MockauthStore struct {
	ctrl     *gomock.Controller
	recorder *MockauthStoreMockRecorder
}

// MockauthStoreMockRecorder is the mock recorder for MockauthStore
type MockauthStoreMockRecorder struct {
	mock *MockauthStore
}

// NewMockauthStore creates a new mock instance
func NewMockauthStore(ctrl *gomock.Controller) *MockauthStore {
	mock := &MockauthStore{ctrl: ctrl}
	mock.recorder = &MockauthStoreMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (_m *MockauthStore) EXPECT() *MockauthStoreMockRecorder {
	return _m.recorder
}

// HasPermission mocks base method
func (_m *MockauthStore) HasPermission(ctx context.Context, userID go_uuid.UUID, perms []permissions.Permission, ptypes ...permissions.PermissionType) (bool, error) {
	_s := []interface{}{ctx, userID, perms}
	for _, _x := range ptypes {
		_s = append(_s, _x)
	}
	ret := _m.ctrl.Call(_m, "HasPermission", _s...)
	ret0, _ := ret[0].(bool)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// HasPermission indicates an expected call of HasPermission
func (_mr *MockauthStoreMockRecorder) HasPermission(arg0, arg1, arg2 interface{}, arg3 ...interface{}) *gomock.Call {
	_s := append([]interface{}{arg0, arg1, arg2}, arg3...)
	return _mr.mock.ctrl.RecordCallWithMethodType(_mr.mock, "HasPermission", reflect.TypeOf((*MockauthStore)(nil).HasPermission), _s...)
}

// IsUserParentForEmployee mocks base method
func (_m *MockauthStore) IsUserParentForEmployee(ctx context.Context, userID go_uuid.UUID, employeeID go_uuid.UUID) (bool, error) {
	ret := _m.ctrl.Call(_m, "IsUserParentForEmployee", ctx, userID, employeeID)
	ret0, _ := ret[0].(bool)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// IsUserParentForEmployee indicates an expected call of IsUserParentForEmployee
func (_mr *MockauthStoreMockRecorder) IsUserParentForEmployee(arg0, arg1, arg2 interface{}) *gomock.Call {
	return _mr.mock.ctrl.RecordCallWithMethodType(_mr.mock, "IsUserParentForEmployee", reflect.TypeOf((*MockauthStore)(nil).IsUserParentForEmployee), arg0, arg1, arg2)
}

// GetUserPermissionTypes mocks base method
func (_m *MockauthStore) GetUserPermissionTypes(ctx context.Context, userID go_uuid.UUID, perms []permissions.Permission) ([]permissions.PermissionType, error) {
	ret := _m.ctrl.Call(_m, "GetUserPermissionTypes", ctx, userID, perms)
	ret0, _ := ret[0].([]permissions.PermissionType)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetUserPermissionTypes indicates an expected call of GetUserPermissionTypes
func (_mr *MockauthStoreMockRecorder) GetUserPermissionTypes(arg0, arg1, arg2 interface{}) *gomock.Call {
	return _mr.mock.ctrl.RecordCallWithMethodType(_mr.mock, "GetUserPermissionTypes", reflect.TypeOf((*MockauthStore)(nil).GetUserPermissionTypes), arg0, arg1, arg2)
}
