package item

import (
	"context"
	"encoding/json"
	"strconv"
	"strings"

	"git.elewise.com/elma365/main/src/app/appview/settings"
	"git.elewise.com/elma365/main/src/common/errs"
	"git.elewise.com/elma365/main/src/namespace/serial"
	"git.elewise.com/elma365/main/src/registry"

	"github.com/pkg/errors"
)

const (
	NameFieldCode = "__register_name"

	RuleEnabled          = "enabled"
	RuleTemplateGenerate = "template_generate"
	RuleManualGenerate   = "manual_generate"
	RuleReserveEnabled   = "reserve_enabled"
	RuleNotReserved      = "not_reserved"
	RuleIsReserved       = "is_reserved"
	RuleNotRegistered    = "not_registered"
	RuleIsRegistered     = "is_registered"
	RuleChangeEnabled    = "change_enabled"
	RuleReserveOnCreate  = "reserve_on_create"

	SerialTemplateCode = "__enum"
	DefaultTemplate    = "{$__enum}"
)

func (item *Item) RegReserve(ctx context.Context, str Store) error {
	regSettings, err := item.GetRegSettings(ctx, str)
	if err != nil {
		return errors.WithStack(err)
	}
	srl, err := str.GetSerial(ctx, item.CollectionNamespace, regSettings.SerialCode)
	if err != nil {
		return errors.WithStack(err)
	}
	rules := []string{
		RuleEnabled,
		RuleTemplateGenerate,
		RuleReserveEnabled,
		RuleNotReserved,
	}
	if err = item.regValidateRules(rules, regSettings, str, ctx); err != nil {
		return errors.WithStack(err)
	}
	_, err = item.reserve(ctx, str, &srl, regSettings)
	return err
}

func (item *Item) RegRegisterNew(ctx context.Context, str Store, name string, value int64) error {

	regSettings, err := item.GetRegSettings(ctx, str)
	if err != nil {
		return errors.WithStack(err)
	}
	srl, err := str.GetSerial(ctx, item.CollectionNamespace, regSettings.SerialCode)
	if err != nil {
		return errors.WithStack(err)
	}
	rules := []string{
		RuleEnabled,
		RuleTemplateGenerate,
		RuleNotRegistered,
		RuleNotReserved,
		RuleReserveOnCreate,
	}
	if err = item.regValidateRules(rules, regSettings, str, ctx); err != nil {
		return errors.WithStack(err)
	}
	entry, err := str.GetSerialEntry(ctx, &srl, value)
	if err != nil {
		return errors.WithStack(err)
	}
	entry.Register(ctx, str)

	item.setFieldValue(NameFieldCode, []byte(strconv.Quote(name)))

	_, err = item.registerWithEntry(ctx, str, entry, name)
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func (item *Item) RegRegister(ctx context.Context, str Store) error {
	regSettings, err := item.GetRegSettings(ctx, str)
	if err != nil {
		return errors.WithStack(err)
	}
	srl, err := str.GetSerial(ctx, item.CollectionNamespace, regSettings.SerialCode)
	if err != nil {
		return errors.WithStack(err)
	}
	rules := []string{
		RuleEnabled,
		RuleTemplateGenerate,
		RuleNotRegistered,
	}
	if err = item.regValidateRules(rules, regSettings, str, ctx); err != nil {
		return errors.WithStack(err)
	}

	var entry *serial.Entry
	if err = item.regValidateRule(RuleIsReserved, regSettings, str, ctx); err != nil {
		entry, err = item.reserve(ctx, str, &srl, regSettings)
		if err != nil {
			return errors.WithStack(err)
		}
	} else {
		entry, err = str.GetSerialReservedEntry(ctx, item.ID)
		if err != nil {
			return errors.WithStack(err)
		}
	}
	entry.Register(ctx, str)
	nameJson, err := item.getFieldValue(NameFieldCode)
	var name string
	err = json.Unmarshal(nameJson, &name)
	if err != nil {
		return errors.WithStack(err)
	}
	_, err = item.registerWithEntry(ctx, str, entry, name)
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func (item *Item) registerWithEntry(ctx context.Context, str Store, entry *serial.Entry, name string) (*registry.Registry, error) {
	registry := registry.Registry{}
	registry.ItemId = item.ID
	registry.Namespace = item.CollectionNamespace
	registry.Code = item.CollectionCode
	registry.SetEntry(entry)
	registry.RegisterName = name
	if err := str.CreateRegistryEntry(ctx, &registry); err != nil {
		return nil, errors.WithStack(err)
	}
	return &registry, nil
}

func (item *Item) reserve(ctx context.Context, str Store, srl *serial.Serial, regSettings *settings.RegistrationSettings) (*serial.Entry, error) {
	entry, err := srl.ReserveValueForItem(ctx, str, item.ID)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	srlStr := strconv.FormatInt(entry.Value, 10)
	values := map[string]string{}
	values[SerialTemplateCode] = srlStr
	temp := regSettings.Template
	if temp == "" {
		temp = DefaultTemplate
	}
	name, err := template(temp, values)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	item.setFieldValue(NameFieldCode, []byte(strconv.Quote(name)))

	return entry, nil
}

func (item *Item) RegManual(ctx context.Context, str Store, regName string) error {
	regSettings, err := item.GetRegSettings(ctx, str)
	if err != nil {
		return errors.WithStack(err)
	}
	rules := []string{
		RuleEnabled,
		RuleManualGenerate,
		RuleNotRegistered,
	}
	if err = item.regValidateRules(rules, regSettings, str, ctx); err != nil {
		return errors.WithStack(err)
	}
	item.setFieldValue(NameFieldCode, []byte(strconv.Quote(regName)))
	_, err = item.registerWithoutEntry(ctx, str, regName)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func (item *Item) registerWithoutEntry(ctx context.Context, str Store, name string) (*registry.Registry, error) {
	registry := registry.Registry{}
	registry.ItemId = item.ID
	registry.Namespace = item.CollectionNamespace
	registry.Code = item.CollectionCode
	registry.RegisterName = name
	if err := str.CreateRegistryEntry(ctx, &registry); err != nil {
		return nil, errors.WithStack(err)
	}
	return &registry, nil
}

func (item *Item) RegRelease(ctx context.Context, str Store) error {
	regSettings, err := item.GetRegSettings(ctx, str)
	if err != nil {
		return errors.WithStack(err)
	}

	rules := []string{
		RuleEnabled,
		RuleTemplateGenerate,
		RuleNotRegistered,
		RuleIsReserved,
	}
	if err = item.regValidateRules(rules, regSettings, str, ctx); err != nil {
		return errors.WithStack(err)
	}
	entry, err := str.GetSerialReservedEntry(ctx, item.ID)
	if err != nil {
		return errors.WithStack(err)
	}
	err = entry.Release(ctx, str)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func (item *Item) RegChange(ctx context.Context, str Store, regName string) error {
	regSettings, err := item.GetRegSettings(ctx, str)
	if err != nil {
		return errors.WithStack(err)
	}
	rules := []string{
		RuleEnabled,
		RuleIsRegistered,
		RuleChangeEnabled,
	}
	if err = item.regValidateRules(rules, regSettings, str, ctx); err != nil {
		return errors.WithStack(err)
	}
	registry, err := str.GetRegistryEntry(ctx, item.ID)
	if err != nil {
		return errors.WithStack(err)
	}
	registry.RegisterName = regName
	if err = str.UpdateRegistryEntry(ctx, registry); err != nil {
		return errors.WithStack(err)
	}
	item.setFieldValue(NameFieldCode, []byte(strconv.Quote(regName)))
	return nil
}

func (item *Item) GetRegSettings(ctx context.Context, str Store) (*settings.RegistrationSettings, error) {
	app, err := str.GetApp(ctx, item.CollectionNamespace, item.CollectionCode)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	settings, err := app.GetSettings()
	if err != nil {
		return nil, errors.WithStack(err)
	}
	regSettings := settings.RegistrationSettings
	return &regSettings, nil
}

func (item *Item) regValidateRules(rules []string, regSettings *settings.RegistrationSettings, str Store, ctx context.Context) error {
	for i := 0; i < len(rules); i++ {
		err := item.regValidateRule(rules[i], regSettings, str, ctx)
		if err != nil {
			return err
		}
	}
	return nil
}

func (item *Item) regValidateRule(rule string, regSettings *settings.RegistrationSettings, str Store, ctx context.Context) error {
	switch rule {
	case RuleEnabled:
		if !regSettings.Enabled {
			return errors.New("registration disabled")
		}
	case RuleTemplateGenerate:
		if regSettings.ManualGenerate {
			return errors.New("only for template generate")
		}
	case RuleReserveEnabled:
		if !regSettings.ReserveEnabled {
			return errors.New("reserve disabled")
		}
	case RuleManualGenerate:
		if !regSettings.ManualGenerate {
			return errors.New("only for manual generate")
		}
	case RuleIsReserved:
		_, err := str.GetSerialReservedEntry(ctx, item.ID)
		if err != nil {
			return errors.WithStack(err)
		}
	case RuleNotReserved:
		_, err := str.GetSerialReservedEntry(ctx, item.ID)
		if err != nil {
			if err != errs.ModelNotFound {
				return errors.WithStack(err)
			}
		} else {
			return errors.New("item already reserved")
		}
	case RuleIsRegistered:
		_, err := str.GetRegistryEntry(ctx, item.ID)
		if err != nil {
			return errors.WithStack(err)
		}
	case RuleNotRegistered:
		_, err := str.GetRegistryEntry(ctx, item.ID)
		if err != nil {
			if err != errs.ModelNotFound {
				return errors.WithStack(err)
			}
		} else {
			return errors.New("item already registered")
		}
	case RuleChangeEnabled:
		if !regSettings.EditAfterRegistration {
			return errors.New("change regname after registration disabled")
		}
	case RuleReserveOnCreate:
		if !regSettings.ReserveOnCreate {
			return errors.New("reserve on create disabled")
		}
	default:
		return errors.New("undefined rule")
	}
	return nil
}

func template(temp string, values map[string]string) (string, error) {
	res := ""
	strLen := len(temp)
	for i := 0; i < strLen; {
		workString := temp[i:strLen]
		workStringLen := len(workString)
		start := strings.Index(workString, "{$")
		if start == -1 {
			res += workString
			break
		}
		res += workString[0:start]
		end := strings.Index(workString[start:workStringLen], "}")
		if end == -1 {
			return "", errors.New("} not found")
		}
		key := workString[start+2 : start+end]
		res += values[key]
		i += start + end + 1
	}
	return res, nil
}
