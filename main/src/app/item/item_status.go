package item

import (
	"context"
	"encoding/json"

	"git.elewise.com/elma365/main/src/app/appfield"
	appSettings "git.elewise.com/elma365/main/src/app/appview/settings"
	"git.elewise.com/elma365/main/src/common/errs"
	"git.elewise.com/elma365/types/complextypes/status"

	"github.com/pkg/errors"
)

const (
	// FieldCode - Code of status field
	StatusFieldCode = "__status"
)

// canSetStatus проверяет, может ли быть установлен новый статус
func (item *Item) canSetStatus(ctx context.Context, str Store, newStatus status.Status) (bool, error) {
	settings, err := item.getStatusSettings(ctx, str)
	if err != nil {
		return false, errors.WithStack(err)
	}

	if !settings.ManualChange {
		return false, errs.ModelCollision.WithData("manual change disabled")
	}

	newStatusItem, err := settings.GetItemByStatusValue(newStatus)
	if err != nil {
		return false, errors.WithStack(err)
	}

	currentStatusItem, err := item.getCurentStatusItem(settings)
	if err != nil {
		return false, errors.WithStack(err)
	}

	return settings.IsNewStatusAllowed(*currentStatusItem, *newStatusItem)
}

// getCurentStatusItem возвращает текущий статус
func (item *Item) getCurentStatusItem(settings *appSettings.StatusSettings) (*appSettings.StatusItem, error) {
	currentStatusValue, err := item.getFieldValue(StatusFieldCode)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	var currentStatusItem *appSettings.StatusItem
	if currentStatusValue == nil {
		currentStatusItem, err = settings.GetDefaultItem()
		if err != nil {
			return nil, errors.WithStack(err)
		}
	} else {
		var currentStatus status.Status
		err = json.Unmarshal(currentStatusValue, &currentStatus)
		if err != nil {
			return nil, errors.WithStack(err)
		}
		currentStatusItem, err = settings.GetItemByStatusValue(currentStatus)
		if err != nil {
			switch errors.Cause(err) {
			case errs.ModelNotFound:
				currentStatusItem, err = settings.GetDefaultItem()
				if err != nil {
					return nil, errors.WithStack(err)
				}
			default:
				return nil, errors.WithStack(err)
			}
		}
	}
	if currentStatusItem == nil {
		return nil, errs.ModelNotFound.WithData("current status item not found")
	}
	return currentStatusItem, nil
}

// getStatusSettings возвращает настройки статуса для приложения
func (item *Item) getStatusSettings(ctx context.Context, str Store) (*appSettings.StatusSettings, error) {
	app, err := str.GetApp(ctx, item.CollectionNamespace, item.CollectionCode)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	settings, err := app.GetSettings()
	if err != nil {
		return nil, errors.WithStack(err)
	}
	return &settings.Status, nil
}

// getAppField возвращает поле приложения по его коду
func (item *Item) getAppField(ctx context.Context, str Store, code string) (*appfield.AppField, error) {
	app, err := str.GetApp(ctx, item.CollectionNamespace, item.CollectionCode)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	var statusField *appfield.AppField
	for _, field := range app.Fields {
		if field.Code == code {
			statusField = &field
			break
		}
	}
	if statusField == nil {
		return nil, errs.ModelNotFound.WithData("field not found")
	}
	return statusField, nil
}

// setFieldValue устанавливает значение поля без сохранения
func (item *Item) setFieldValue(key string, value json.RawMessage) error {
	var fields map[string]json.RawMessage
	var err error
	fields, err = item.getFieldsMap()
	if err != nil {
		return errors.WithStack(err)
	}
	fields[key] = value
	item.Body, err = json.Marshal(fields)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

// getFieldValue возвращает значение поля
func (item *Item) getFieldValue(key string) (json.RawMessage, error) {
	fields, err := item.getFieldsMap()
	if err != nil {
		return nil, errors.WithStack(err)
	}
	field := fields[key]
	if field == nil || len(field) == 0 || (len(field) == 4 && string(field[:4]) == "null") {
		return nil, nil
	}
	return field, nil
}

// GetFieldsMap возвращает хэш значений полей
func (item *Item) getFieldsMap() (map[string]json.RawMessage, error) {
	fields := map[string]json.RawMessage{}
	if item.Body == nil {
		return fields, nil
	}
	err := json.Unmarshal(item.Body, &fields)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	return fields, nil
}
