package item

import (
	"context"
	"testing"

	"git.elewise.com/elma365/main/src/auth/claims"
	"git.elewise.com/elma365/main/src/auth/pb"
	"git.elewise.com/elma365/main/src/collection"
	"git.elewise.com/elma365/main/src/collection/roles"
	"git.elewise.com/elma365/main/src/common/permissions"
	"git.elewise.com/elma365/main/src/common/queries"

	"github.com/golang/mock/gomock"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestHasPermission(t *testing.T) {
	UserID := "00000000-0000-0000-0001-000000000000"

	tests := []struct {
		name                  string
		isAdmin               bool
		accessType            queries.AccessType
		permissionType        permissions.PermissionType
		hasCommonPermissions  bool
		collectionPermissions *permissions.Permissions
		isCreator             bool
		expected              bool
	}{
		{
			name:       "Админ имеет доступ при любом типе прав на справочник",
			isAdmin:    true,
			accessType: queries.AccessTypeCollection,
			expected:   true,
		},
		{
			name:       "None: полный доступ ко всему",
			accessType: queries.AccessTypeNone,
			expected:   true,
		},

		// Collection
		{
			name:                 "Collection: доступ разрешен при наличии разрешений для пользователя",
			accessType:           queries.AccessTypeCollection,
			hasCommonPermissions: true,
			expected:             true,
		},
		{
			name:                  "Collection: автор не учитывается",
			accessType:            queries.AccessTypeCollection,
			permissionType:        permissions.UPDATE,
			collectionPermissions: getCreatorPermissionsFor(permissions.UPDATE),
			isCreator:             true,
			expected:              false,
		},

		// Row
		{
			name:                  "Row: автор НЕ может менять элемент если у него нет разрешения",
			accessType:            queries.AccessTypeRow,
			permissionType:        permissions.UPDATE,
			collectionPermissions: getCreatorPermissionsFor(permissions.DELETE),
			isCreator:             true,
			expected:              false,
		},
		{
			name:                  "Row: автор может менять элемент если у него есть разрешение",
			accessType:            queries.AccessTypeRow,
			permissionType:        permissions.UPDATE,
			collectionPermissions: getCreatorPermissionsFor(permissions.UPDATE),
			isCreator:             true,
			expected:              true,
		},
		{
			name:                  "Row: НЕ автор не может менять элемент",
			accessType:            queries.AccessTypeRow,
			permissionType:        permissions.UPDATE,
			collectionPermissions: getCreatorPermissionsFor(permissions.UPDATE),
			isCreator:             false,
			expected:              false,
		},
		{
			name:                  "Row: автор не может создавать элемент",
			accessType:            queries.AccessTypeRow,
			permissionType:        permissions.CREATE,
			collectionPermissions: getCreatorPermissionsFor(permissions.CREATE),
			isCreator:             true,
			expected:              false,
		},
		{
			name:                 "Row: можно создавать элемент если есть разрешение на уровне группы пользователя",
			accessType:           queries.AccessTypeRow,
			permissionType:       permissions.CREATE,
			hasCommonPermissions: true,
			expected:             true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mctrl := gomock.NewController(t)
			defer mctrl.Finish()

			ctx := claims.ContextWithClaims(context.Background(), &claims.Claims{
				Privileges: 0,
				UserID:     uuid.FromStringOrNil(UserID),
			})
			if tt.isAdmin {
				ctx = claims.ContextWithClaims(context.Background(), &claims.Claims{
					Privileges: claims.PrivilegeToMask(pb.Privilege_administration),
				})
			}

			collectionPermissions := &permissions.Permissions{
				Values: []permissions.Permission{},
			}
			if tt.collectionPermissions != nil {
				collectionPermissions = tt.collectionPermissions
			}

			col := collection.Collection{
				AccessType: tt.accessType,
			}
			settings := collection.PermissionsSettings{
				AccessType:  tt.accessType,
				Permissions: collectionPermissions,
			}

			dataStore := NewMockdataStore(mctrl)
			dataStore.EXPECT().QueryCollection(gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes().
				Return(&col, nil)
			dataStore.EXPECT().GetCollectionPermissionsSettings(gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes().
				Return(&settings, nil)
			dataStore.EXPECT().GetItemPermissions(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes().
				Return(&permissions.Permissions{}, nil)

			authStore := NewMockauthStore(mctrl)
			authStore.EXPECT().HasPermission(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes().
				Return(tt.hasCommonPermissions, nil)
			authStore.EXPECT().IsUserParentForEmployee(gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes().
				Return(tt.isCreator, nil)

			item := Item{}
			hasPermissons, err := item.HasPermission(ctx, dataStore, authStore, tt.permissionType)
			assert.NoError(t, err)
			assert.Equal(t, tt.expected, hasPermissons)
		})
	}
}

func TestGetPermissionTypesForCurrentUser(t *testing.T) {
	tests := []struct {
		name             string
		isAdmin          bool
		accessType       queries.AccessType
		collectionPTypes []permissions.PermissionType
		itemPTypes       []permissions.PermissionType
		expected         []permissions.PermissionType
	}{
		{
			name:     "admin has full access",
			isAdmin:  true,
			expected: permissions.FullAccess,
		},
		{
			name:             "collection access type skip item permissions",
			accessType:       queries.AccessTypeCollection,
			collectionPTypes: []permissions.PermissionType{permissions.READ},
			itemPTypes:       []permissions.PermissionType{permissions.UPDATE},
			expected:         []permissions.PermissionType{permissions.READ},
		},
		{
			name:             "row access type with full access",
			accessType:       queries.AccessTypeRow,
			collectionPTypes: permissions.FullAccess,
			expected:         permissions.FullAccess,
		},
		{
			name:             "row access type use both collection and item permissions",
			accessType:       queries.AccessTypeRow,
			collectionPTypes: []permissions.PermissionType{permissions.READ},
			itemPTypes:       []permissions.PermissionType{permissions.UPDATE},
			expected:         []permissions.PermissionType{permissions.READ, permissions.UPDATE},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mctrl := gomock.NewController(t)
			defer mctrl.Finish()

			ctx := claims.ContextWithClaims(context.Background(), &claims.Claims{
				Privileges: 0,
			})
			if tt.isAdmin {
				ctx = claims.ContextWithClaims(context.Background(), &claims.Claims{
					Privileges: claims.PrivilegeToMask(pb.Privilege_administration),
				})
			}

			col := collection.Collection{
				AccessType: tt.accessType,
			}
			settings := collection.PermissionsSettings{
				AccessType:  tt.accessType,
				Permissions: &permissions.Permissions{},
			}

			dataStore := NewMockdataStore(mctrl)
			dataStore.EXPECT().QueryCollection(gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes().
				Return(&col, nil)
			dataStore.EXPECT().GetCollectionPermissionsSettings(gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes().
				Return(&settings, nil)
			dataStore.EXPECT().GetItemPermissions(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes().
				Return(&permissions.Permissions{}, nil)

			authStore := NewMockauthStore(mctrl)
			authStore.EXPECT().IsUserParentForEmployee(gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes().
				Return(true, nil)
			if tt.collectionPTypes != nil {
				authStore.EXPECT().GetUserPermissionTypes(gomock.Any(), gomock.Any(), gomock.Any()).Times(1).
					Return(tt.collectionPTypes, nil)
			}
			if tt.accessType == queries.AccessTypeRow && tt.itemPTypes != nil {
				authStore.EXPECT().GetUserPermissionTypes(gomock.Any(), gomock.Any(), gomock.Any()).Times(1).
					Return(tt.itemPTypes, nil)
			}

			item := Item{}
			permissionTypes, err := item.GetPermissionTypesForCurrentUser(ctx, dataStore, authStore, false, nil)
			assert.NoError(t, err)
			assert.Equal(t, tt.expected, permissionTypes)
		})
	}
}

func getCreatorPermissionsFor(ptype permissions.PermissionType) *permissions.Permissions {
	return &permissions.Permissions{
		Values: []permissions.Permission{
			{
				Group: permissions.PermissionGroup{
					Type: permissions.Role,
					ID:   roles.Creator,
				},
				Types: []permissions.PermissionType{
					ptype,
				},
			},
		},
	}
}
