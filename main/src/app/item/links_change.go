package item

import (
	"encoding/json"

	"git.elewise.com/elma365/main/src/app/appfield"
	"git.elewise.com/elma365/main/src/app/appfield/syscollection"

	"github.com/pkg/errors"
)

// linkChange описывает изменение значения связей элементов в поле типа SYSTEM_COLLECTION
type linkChange struct {
	FieldData  syscollection.Data
	BackField  appfield.AppField
	CreatedIds []string
	DeletedIds []string
}

// ApplyLinkCreation создает обратные ссылки на элемент itemID для элементов кэша
func (c *linkChange) CreateBackLinksForChachedItems(itemID string, itemsCache map[string]Item) error {
	for _, id := range c.CreatedIds {
		linkedItem := itemsCache[id]
		originBody := map[string]json.RawMessage{}
		err := json.Unmarshal(linkedItem.Body, &originBody)
		if err != nil {
			return errors.WithStack(err)
		}

		// Код поля в связанном справочнике. Берем из fieldData потому что так удобно.
		fieldCode := c.FieldData.LinkedFieldCode

		var updatedIds []string
		originValue, exists := originBody[fieldCode]
		if !exists || string(originValue) == "null" {
			updatedIds = []string{itemID}
		} else {
			updatedIds, err = c.updateLinkedValues(originValue, itemID)
			if err != nil {
				return err
			}
		}

		updatedBody, err := setBodyField(updatedIds, fieldCode, originBody)
		if err != nil {
			return err
		}
		linkedItem.Body = updatedBody

		itemsCache[id] = linkedItem
	}

	return nil
}

// ApplyLinkDeletion удаляет обратные ссылки на элемент itemID из элементов кэша
func (c *linkChange) DeleteBackLinksForChachedItems(itemID string, itemsCache map[string]Item) error {
	for _, id := range c.DeletedIds {
		linkedItem := itemsCache[id]
		originBody := map[string]json.RawMessage{}
		err := json.Unmarshal(linkedItem.Body, &originBody)
		if err != nil {
			return errors.WithStack(err)
		}

		// Код поля в связанном справочнике. Берем из fieldData потому что так удобно.
		fieldCode := c.FieldData.LinkedFieldCode

		originValue, exists := originBody[fieldCode]
		if !exists {
			return errors.Errorf("source field <%s> not found in dest body", fieldCode)
		}
		if string(originValue) == "null" {
			return errors.Errorf("source field <%s> value expects array, but got null", fieldCode)
		}

		var ids []string
		err = json.Unmarshal(originValue, &ids)
		if err != nil {
			return errors.Wrapf(err, "can't unmarshal dest field <%s> value", fieldCode)
		}

		ids = removeFromSlice(ids, itemID)
		updatedBody, err := setBodyField(ids, fieldCode, originBody)
		if err != nil {
			return err
		}
		linkedItem.Body = updatedBody

		itemsCache[id] = linkedItem
	}

	return nil
}

// insertIntoLinkedValues добавляет ИД текушего элемента в соответствующее поле связанного элемента
func (c *linkChange) updateLinkedValues(originValues json.RawMessage, itemID string) (
	[]string, error) {

	var originIds []string
	err := json.Unmarshal(originValues, &originIds)
	if err != nil {
		return nil, errors.Wrapf(err, "can't unmarshal field <%s> value", c.FieldData.LinkedFieldCode)
	}

	if c.BackField.Single {
		// В случае единственности хранимого элемента перетираем все остальные данные
		return []string{itemID}, nil
	}

	if !isStringInSlice(itemID, originIds) {
		return append(originIds, itemID), nil
	}

	return originIds, nil
}

// removeFromSlice удаляет заданный элемент из списка. Оригинальный список будет модифицирован.
func removeFromSlice(s []string, r string) []string {
	for i, v := range s {
		if v == r {
			return append(s[:i], s[i+1:]...)
		}
	}

	return s
}

func setBodyField(values []string, fieldCode string, bodyMap map[string]json.RawMessage) ([]byte, error) {
	bytes := make([]byte, 0)
	bytes, err := json.Marshal(values)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	bodyMap[fieldCode] = bytes
	bodyJSON, err := json.Marshal(bodyMap)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return bodyJSON, nil
}
