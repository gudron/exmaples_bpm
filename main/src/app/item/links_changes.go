package item

// linksChanges
type linksChanges struct {
	changes       []linkChange
	currentItemID string
	itemsCache    map[string]Item
}

// GetItemsForUpdate применяет изменения связей к заданным элементам
func (cc *linksChanges) Apply() ([]Item, error) {
	for _, change := range cc.changes {
		err := change.CreateBackLinksForChachedItems(cc.currentItemID, cc.itemsCache)
		if err != nil {
			return nil, err
		}

		err = change.DeleteBackLinksForChachedItems(cc.currentItemID, cc.itemsCache)
		if err != nil {
			return nil, err
		}
	}

	items := make([]Item, 0, len(cc.itemsCache))
	for _, item := range cc.itemsCache {
		items = append(items, item)
	}

	return items, nil
}
