package item

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestApply(t *testing.T) {
	tests := []struct {
		name          string
		changes       json.RawMessage
		items         json.RawMessage
		expectedItems json.RawMessage
		expectedError string
	}{
		{
			name:          "empty",
			changes:       json.RawMessage(`[]`),
			items:         json.RawMessage(`[]`),
			expectedItems: json.RawMessage(`[]`),
		},
		// Добавление связей
		{
			name: "many to one: added new link",
			changes: json.RawMessage(`[
				{
					"fieldData": {
						"namespace": "GLOBAL",
						"code": "PERSON",
						"linkedFieldCode": "person",
						"single": true
					},
					"backField": {
						"code": "person",
						"single": true
					},
					"createdIds": ["2fb8e159-8027-40cd-b268-222222222222"],
					"deletedIds": []
				}
			]`),
			items: json.RawMessage(`[
				{
					"__id": "2fb8e159-8027-40cd-b268-222222222222",
				    "__createdAt": "2017-10-02T00:00:00Z",
				    "__updatedAt": "2017-10-02T00:00:00Z",
				    "collectionNamespace": "GLOBAL",
				    "collectionCode": "PERSON",
				    "body": {
				    	"__id": "2fb8e159-8027-40cd-b268-222222222222"
					}
				}
			]`),
			expectedItems: json.RawMessage(`[
				{
					"__id": "2fb8e159-8027-40cd-b268-222222222222",
					"__createdAt": "2017-10-02T00:00:00Z",
					"__updatedAt": "2017-10-02T00:00:00Z",
					"collectionNamespace": "GLOBAL",
					"collectionCode": "PERSON",
					"body": {
						"__id": "2fb8e159-8027-40cd-b268-222222222222",
						"person": ["2fb8e159-8027-40cd-b268-111111111111"]
					}
				}
			]`),
		},
		{
			name: "many to one: drop old linked items",
			changes: json.RawMessage(`[
				{
					"fieldData": {
						"namespace": "GLOBAL",
						"code": "PERSON",
						"linkedFieldCode": "person",
						"single": true
					},
					"backField": {
						"code": "person",
						"single": true
					},
					"createdIds": ["2fb8e159-8027-40cd-b268-222222222222"],
					"deletedIds": []
				}
			]`),
			items: json.RawMessage(`[
				{
					"__id": "2fb8e159-8027-40cd-b268-222222222222",
				    "__createdAt": "2017-10-02T00:00:00Z",
				    "__updatedAt": "2017-10-02T00:00:00Z",
				    "collectionNamespace": "GLOBAL",
				    "collectionCode": "PERSON",
				    "body": {
						"__id": "2fb8e159-8027-40cd-b268-222222222222",
						"person": ["2fb8e159-8027-40cd-b268-000000000000", "2fb8e159-8027-40cd-b268-aaaaaaaaaaaa"]
					}
				}
			]`),
			expectedItems: json.RawMessage(`[
				{
					"__id": "2fb8e159-8027-40cd-b268-222222222222",
					"__createdAt": "2017-10-02T00:00:00Z",
					"__updatedAt": "2017-10-02T00:00:00Z",
					"collectionNamespace": "GLOBAL",
					"collectionCode": "PERSON",
					"body": {
						"__id": "2fb8e159-8027-40cd-b268-222222222222",
						"person": ["2fb8e159-8027-40cd-b268-111111111111"]
					}
				}
			]`),
		},
		{
			name: "many to many: add new link",
			changes: json.RawMessage(`[
				{
					"fieldData": {
						"namespace": "GLOBAL",
						"code": "PERSON",
						"linkedFieldCode": "person",
						"single": true
					},
					"backField": {
						"code": "person",
						"single": false
					},
					"createdIds": ["2fb8e159-8027-40cd-b268-222222222222"],
					"deletedIds": []
				}
			]`),
			items: json.RawMessage(`[
				{
					"__id": "2fb8e159-8027-40cd-b268-222222222222",
				    "__createdAt": "2017-10-02T00:00:00Z",
				    "__updatedAt": "2017-10-02T00:00:00Z",
				    "collectionNamespace": "GLOBAL",
				    "collectionCode": "PERSON",
				    "body": {
						"__id": "2fb8e159-8027-40cd-b268-222222222222",
						"person": ["2fb8e159-8027-40cd-b268-000000000000"]
					}
				}
			]`),
			expectedItems: json.RawMessage(`[
				{
					"__id": "2fb8e159-8027-40cd-b268-222222222222",
					"__createdAt": "2017-10-02T00:00:00Z",
					"__updatedAt": "2017-10-02T00:00:00Z",
					"collectionNamespace": "GLOBAL",
					"collectionCode": "PERSON",
					"body": {
						"__id": "2fb8e159-8027-40cd-b268-222222222222",
						"person": ["2fb8e159-8027-40cd-b268-000000000000","2fb8e159-8027-40cd-b268-111111111111"]
					}
				}
			]`),
		},
		{
			name: "do not link with already linked item",
			changes: json.RawMessage(`[
				{
					"fieldData": {
						"namespace": "GLOBAL",
						"code": "PERSON",
						"linkedFieldCode": "person",
						"single": true
					},
					"backField": {
						"code": "person",
						"single": false
					},
					"createdIds": ["2fb8e159-8027-40cd-b268-222222222222"],
					"deletedIds": []
				}
			]`),
			items: json.RawMessage(`[
				{
					"__id": "2fb8e159-8027-40cd-b268-222222222222",
				    "__createdAt": "2017-10-02T00:00:00Z",
				    "__updatedAt": "2017-10-02T00:00:00Z",
				    "collectionNamespace": "GLOBAL",
				    "collectionCode": "PERSON",
				    "body": {
						"__id": "2fb8e159-8027-40cd-b268-222222222222",
						"person": ["2fb8e159-8027-40cd-b268-111111111111"]
					}
				}
			]`),
			expectedItems: json.RawMessage(`[
				{
					"__id": "2fb8e159-8027-40cd-b268-222222222222",
					"__createdAt": "2017-10-02T00:00:00Z",
					"__updatedAt": "2017-10-02T00:00:00Z",
					"collectionNamespace": "GLOBAL",
					"collectionCode": "PERSON",
					"body": {
						"__id": "2fb8e159-8027-40cd-b268-222222222222",
						"person": ["2fb8e159-8027-40cd-b268-111111111111"]
					}
				}
			]`),
		},

		// Удаление связей
		{
			name: "delete link: single value",
			changes: json.RawMessage(`[
				{
					"fieldData": {
						"namespace": "GLOBAL",
						"code": "PERSON",
						"linkedFieldCode": "person",
						"single": true
					},
					"backField": {
						"code": "person",
						"single": false
					},
					"createdIds": [],
					"deletedIds": ["2fb8e159-8027-40cd-b268-222222222222"]
				}
			]`),
			items: json.RawMessage(`[
				{
					"__id": "2fb8e159-8027-40cd-b268-222222222222",
				    "__createdAt": "2017-10-02T00:00:00Z",
				    "__updatedAt": "2017-10-02T00:00:00Z",
				    "collectionNamespace": "GLOBAL",
				    "collectionCode": "PERSON",
				    "body": {
						"__id": "2fb8e159-8027-40cd-b268-222222222222",
						"person": ["2fb8e159-8027-40cd-b268-111111111111"]
					}
				}
			]`),
			expectedItems: json.RawMessage(`[
				{
					"__id": "2fb8e159-8027-40cd-b268-222222222222",
					"__createdAt": "2017-10-02T00:00:00Z",
					"__updatedAt": "2017-10-02T00:00:00Z",
					"collectionNamespace": "GLOBAL",
					"collectionCode": "PERSON",
					"body": {
						"__id": "2fb8e159-8027-40cd-b268-222222222222",
						"person": []
					}
				}
			]`),
		},
		{
			name: "delete link: multiple values",
			changes: json.RawMessage(`[
				{
					"fieldData": {
						"namespace": "GLOBAL",
						"code": "PERSON",
						"linkedFieldCode": "person",
						"single": true
					},
					"backField": {
						"code": "person",
						"single": false
					},
					"createdIds": [],
					"deletedIds": ["2fb8e159-8027-40cd-b268-222222222222"]
				}
			]`),
			items: json.RawMessage(`[
				{
					"__id": "2fb8e159-8027-40cd-b268-222222222222",
				    "__createdAt": "2017-10-02T00:00:00Z",
				    "__updatedAt": "2017-10-02T00:00:00Z",
				    "collectionNamespace": "GLOBAL",
				    "collectionCode": "PERSON",
				    "body": {
						"__id": "2fb8e159-8027-40cd-b268-222222222222",
						"person": ["2fb8e159-8027-40cd-b268-000000000000", "2fb8e159-8027-40cd-b268-111111111111"]
					}
				}
			]`),
			expectedItems: json.RawMessage(`[
				{
					"__id": "2fb8e159-8027-40cd-b268-222222222222",
					"__createdAt": "2017-10-02T00:00:00Z",
					"__updatedAt": "2017-10-02T00:00:00Z",
					"collectionNamespace": "GLOBAL",
					"collectionCode": "PERSON",
					"body": {
						"__id": "2fb8e159-8027-40cd-b268-222222222222",
						"person": ["2fb8e159-8027-40cd-b268-000000000000"]
					}
				}
			]`),
		},
		{
			name: "delete link: null value",
			changes: json.RawMessage(`[
				{
					"fieldData": {
						"namespace": "GLOBAL",
						"code": "PERSON",
						"linkedFieldCode": "person",
						"single": true
					},
					"backField": {
						"code": "person",
						"single": false
					},
					"createdIds": [],
					"deletedIds": ["2fb8e159-8027-40cd-b268-222222222222"]
				}
			]`),
			items: json.RawMessage(`[
				{
					"__id": "2fb8e159-8027-40cd-b268-222222222222",
				    "__createdAt": "2017-10-02T00:00:00Z",
				    "__updatedAt": "2017-10-02T00:00:00Z",
				    "collectionNamespace": "GLOBAL",
				    "collectionCode": "PERSON",
				    "body": {
						"__id": "2fb8e159-8027-40cd-b268-222222222222",
						"person": null
					}
				}
			]`),
			expectedError: "source field <person> value expects array, but got null",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var changes []linkChange
			if tt.changes != nil {
				err := json.Unmarshal(tt.changes, &changes)
				require.NoError(t, err)
			}

			var items []Item
			if tt.items != nil {
				err := json.Unmarshal(tt.items, &items)
				require.NoError(t, err)
			}

			itemsCache := map[string]Item{}
			for _, item := range items {
				itemsCache[item.ID.String()] = item
			}

			c := linksChanges{changes, "2fb8e159-8027-40cd-b268-111111111111", itemsCache}
			items, err := c.Apply()
			if len(tt.expectedError) > 0 {
				require.Error(t, err)
				require.Equal(t, tt.expectedError, err.Error())
				return
			}

			require.NoError(t, err)

			itemsData, err := json.MarshalIndent(items, "", "    ")
			require.NoError(t, err)

			var expectedItems []Item
			err = json.Unmarshal(tt.expectedItems, &expectedItems)
			require.NoError(t, err)
			expectedItemsData, err := json.MarshalIndent(expectedItems, "", "    ")
			require.NoError(t, err)

			require.Equal(t, string(expectedItemsData), string(itemsData))
		})
	}
}
