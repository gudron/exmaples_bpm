package item

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"

	"git.elewise.com/elma365/main/src/app/middleware"
	"git.elewise.com/elma365/main/src/auth"
	"git.elewise.com/elma365/main/src/common/config"
	"git.elewise.com/elma365/main/src/common/errs"
	"git.elewise.com/elma365/main/src/common/list"
	"git.elewise.com/elma365/main/src/common/permissions"
	"git.elewise.com/elma365/main/src/common/tracing"
	"git.elewise.com/elma365/types/complextypes/status"
	"github.com/bfg-dev/crypto-core/_data/pkg/dep/sources/https---github.com-satori-go.uuid"

	"github.com/asaskevich/govalidator"
	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
	"go.uber.org/zap"
)

// NewRouter is a collection items endpoint
func NewRouter(cfg *config.Config, auth *auth.Auth, str Store) chi.Router {
	r := chi.NewRouter()
	r.Use(middleware.WithAppParams)
	rtx := r.With(str.WithTx)

	r.Get("/", listItems(cfg, auth, str))
	rtx.Post("/", create(str, auth))
	rtx.Post("/keyfields", getItemsWithKeyFields(str))

	r.Route("/{id}", func(r chi.Router) {
		r.Use(middleware.WithItemParams)
		rtx := r.With(str.WithTx)

		r.Get("/", get(cfg, str, auth))
		rtx.Put("/", update(str, cfg, auth))
		rtx.Put("/status", updateStatus(str, auth))

		rtx.Post("/save", createOrUpdate(str, auth))

		r.Get("/permissions", getPermissions(str, cfg))
		r.Put("/permissions", updatePermissions(str, auth, cfg))

		rtx.Get("/serial/entry", getSerialEntry(str))

		r.Mount("/reg", NewRegRouter(cfg, auth, str))
	})

	return r
}

func getSerialEntry(str Store) http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := struct {
			Id string `valid:"required,uuid"`
		}{
			chi.URLParam(r, "id"),
		}
		if _, err := govalidator.ValidateStruct(params); err != nil {
			l.Debug("validate params", zap.Error(err))
			http.Error(w, errors.Wrap(err, "validate").Error(), http.StatusBadRequest)
			return
		}
		itemId, err := uuid.FromString(params.Id)
		if err != nil {
			l.Debug("uuid convert", zap.Error(err))
			http.Error(w, errors.Wrap(err, "validate").Error(), http.StatusBadRequest)
			return
		}
		entry, err := str.GetSerialReservedEntry(ctx, itemId)
		if err != nil {
			if err == errs.ModelNotFound {
				http.Error(w, errors.Wrap(err, "get serial entry").Error(), http.StatusNotFound)
				return
			} else {
				l.Debug("get serial entry", zap.Error(err))
				http.Error(w, errors.Wrap(err, "get serial entry").Error(), http.StatusBadRequest)
				return
			}

		}
		entryJSON, err := json.Marshal(entry)
		if err != nil {
			errs.Internal(l, w, err)
			return
		}

		io.Copy(w, bytes.NewBuffer(entryJSON))
	}
	return http.HandlerFunc(fn)
}

func createOrUpdate(str Store, auth *auth.Auth) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := middleware.ItemParamsFromContext(ctx)

		payload := map[string]json.RawMessage{}
		jd := json.NewDecoder(r.Body)
		if err := jd.Decode(&payload); err != nil {
			l.Debug("unmarshal", zap.Error(err))
			http.Error(w, errors.Wrap(err, "unmarshal").Error(), http.StatusBadRequest)
			return
		}
		isNew := false
		item, err := str.GetItemByID(ctx, params.ID)
		if err != nil {
			if err == errs.ModelNotFound {
				id, err := uuid.FromString(params.ID)
				if err != nil {
					l.Debug("convert uuid", zap.Error(err))
					http.Error(w, errors.Wrap(err, "convert uui").Error(), http.StatusInternalServerError)
				}
				item = &Item{
					ID:                  id,
					CollectionCode:      params.Code,
					CollectionNamespace: params.Namespace,
				}
				isNew = true
			} else {
				errs.Handle(l, w, err)
				return
			}
		}

		err = item.Update(ctx, auth, str, payload)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		// Вычисляем и дописываем права текущего пользователя на элемент
		itemPTypes, err := item.GetPermissionTypesForCurrentUser(ctx, str, auth, isNew, nil)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		model := itemWithCurrentUserPermissions{
			Item: item,
			CurrentUserPermissions: itemPTypes,
		}

		// Возвращаем измененный элемент
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		je := json.NewEncoder(w)
		if err := je.Encode(model); err != nil {
			l.Error(err.Error(), errs.ZapStack(err))
		}
	})
}

func create(str Store, auth *auth.Auth) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := middleware.AppParamsFromContext(ctx)

		req := struct {
			Payload  map[string]json.RawMessage `json:"payload"`
			TempData TempData                   `json:"tempData"`
		}{}

		jd := json.NewDecoder(r.Body)
		if err := jd.Decode(&req); err != nil {
			l.Debug("unmarshal", zap.Error(err))
			http.Error(w, errors.Wrap(err, "unmarshal").Error(), http.StatusBadRequest)
			return
		}

		payload := req.Payload

		// Сохраняем
		item := Item{
			ID:                  uuid.NewV4(),
			CollectionCode:      params.Code,
			CollectionNamespace: params.Namespace,
		}
		if err := req.TempData.process(ctx, str, &item); err != nil {
			errs.Handle(l, w, err)
			return
		}

		if err := item.Update(ctx, auth, str, payload); err != nil {
			errs.Handle(l, w, err)
			return
		}

		// Вычисляем и дописываем права текущего пользователя на элемент. При этом права самого элемента не учитывваются,
		// т.к. их еще нет - элемент только что создан.
		itemPTypes, err := item.GetPermissionTypesForCurrentUser(ctx, str, auth, true, nil)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		model := itemWithCurrentUserPermissions{
			Item: &item,
			CurrentUserPermissions: itemPTypes,
		}

		// Возвращаем созданный элемент
		w.WriteHeader(http.StatusCreated)
		w.Header().Set("Content-Type", "application/json")
		je := json.NewEncoder(w)
		if err := je.Encode(model); err != nil {
			l.Error(err.Error(), errs.ZapStack(err))
		}
	})
}

func update(str Store, cfg *config.Config, auth *auth.Auth) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := middleware.ItemParamsFromContext(ctx)

		payload := map[string]json.RawMessage{}
		jd := json.NewDecoder(r.Body)
		if err := jd.Decode(&payload); err != nil {
			l.Debug("unmarshal", zap.Error(err))
			http.Error(w, errors.Wrap(err, "unmarshal").Error(), http.StatusBadRequest)
			return
		}

		item, err := str.GetItemByID(ctx, params.ID)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		err = item.Update(ctx, auth, str, payload)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		// Вычисляем и дописываем права текущего пользователя на элемент
		itemPTypes, err := item.GetPermissionTypesForCurrentUser(ctx, str, auth, false, nil)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		model := itemWithCurrentUserPermissions{
			Item: item,
			CurrentUserPermissions: itemPTypes,
		}

		// Возвращаем измененный элемент
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		je := json.NewEncoder(w)
		if err := je.Encode(model); err != nil {
			l.Error(err.Error(), errs.ZapStack(err))
		}
	})
}

func listItems(cfg *config.Config, auth *auth.Auth, str Store) http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := middleware.AppParamsFromContext(ctx)

		listParams, err := list.NewParams(r)
		if err != nil {
			http.Error(w, errors.Wrap(err, "validate").Error(), http.StatusBadRequest)
			return
		}

		res, err := str.GetItems(ctx, params.Namespace, params.Code, listParams)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		if listParams.Options.WithPermissions {
			itemsWithCUP, err := fillItemsWithCurrentUserPermissions(ctx, auth, str, params, res.Items)
			if err != nil {
				errs.Handle(l, w, err)
				return
			}
			res.Items = itemsWithCUP
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		je := json.NewEncoder(w)
		if err := je.Encode(res); err != nil {
			l.Error(err.Error(), errs.ZapStack(err))
		}
	}

	return http.HandlerFunc(fn)
}

func get(cfg *config.Config, str Store, auth *auth.Auth) http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := middleware.ItemParamsFromContext(ctx)

		item, err := str.QueryItemByID(ctx, params.Namespace, params.Code, params.ID)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		// Вычисляем и дописываем права текущего пользователя на элемент
		itemPTypes, err := item.GetPermissionTypesForCurrentUser(ctx, str, auth, false, nil)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		model := itemWithCurrentUserPermissions{
			Item: item,
			CurrentUserPermissions: itemPTypes,
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		je := json.NewEncoder(w)
		if err := je.Encode(model); err != nil {
			l.Error(err.Error(), errs.ZapStack(err))
		}
	}

	return http.HandlerFunc(fn)
}

func getItemsWithKeyFields(str Store) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := middleware.AppParamsFromContext(ctx)

		payload := struct {
			Ids []string `valid:"required" json:"ids"`
		}{
			[]string{},
		}
		jd := json.NewDecoder(r.Body)
		if err := jd.Decode(&payload); err != nil {
			http.Error(w, errors.Wrap(err, "unmarshal").Error(), http.StatusBadRequest)
			return
		}
		if _, err := govalidator.ValidateStruct(payload); err != nil {
			http.Error(w, errors.Wrap(err, "validate").Error(), http.StatusBadRequest)
			return
		}

		app, err := str.QueryApp(ctx, params.Namespace, params.Code, true)
		if err != nil {
			errs.Internal(l, w, err)
			return
		}

		keyField, err := app.GetKeyField()
		if err != nil {
			errs.Internal(l, w, err)
			return
		}

		items, err := str.QueryItemsByIds(ctx, app, payload.Ids)
		if err != nil {
			errs.Internal(l, w, err)
			return
		}

		result := struct {
			KeyFieldCode string          `json:"keyFieldCode"`
			Result       json.RawMessage `json:"result"`
		}{
			keyField.Code,
			items,
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		je := json.NewEncoder(w)
		if err := je.Encode(result); err != nil {
			l.Error(err.Error(), errs.ZapStack(err))
		}
	}
}

func updateStatus(str Store, auth *auth.Auth) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := middleware.ItemParamsFromContext(ctx)

		item, err := str.GetItemByID(ctx, params.ID)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		var st status.Status
		jd := json.NewDecoder(r.Body)
		if err := jd.Decode(&st); err != nil {
			http.Error(w, errors.Wrap(err, "unmarshal").Error(), http.StatusBadRequest)
			return
		}

		if err := item.UpdateStatus(ctx, auth, str, st); err != nil {
			errs.Handle(l, w, err)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		io.Copy(w, bytes.NewBuffer(item.Body))
	})
}

func getPermissions(str Store, cfg *config.Config) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := middleware.ItemParamsFromContext(ctx)

		perms, err := str.GetItemPermissionsAsJSON(ctx, params.Namespace, params.Code, params.ID)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		je := json.NewEncoder(w)
		if err := je.Encode(perms); err != nil {
			l.Error(err.Error(), errs.ZapStack(err))
		}
	})
}

func updatePermissions(str Store, auth *auth.Auth, cfg *config.Config) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		params := middleware.ItemParamsFromContext(ctx)

		perms := permissions.Permissions{}
		jd := json.NewDecoder(r.Body)
		if err := jd.Decode(&perms); err != nil {
			http.Error(w, errors.Wrap(err, "unmarshal").Error(), http.StatusBadRequest)
			return
		}

		if _, err := govalidator.ValidateStruct(params); err != nil {
			l.Debug("validate", zap.Error(err))
			http.Error(w, errors.Wrap(err, "validate").Error(), http.StatusBadRequest)
			return
		}

		item, err := str.GetItemByID(ctx, params.ID)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		err = item.UpdatePermissions(ctx, auth, str, perms)
		if err != nil {
			errs.Handle(l, w, err)
			return
		}

		w.WriteHeader(http.StatusOK)
	})
}

func fillItemsWithCurrentUserPermissions(ctx context.Context, auth *auth.Auth, str Store,
	params middleware.AppParams, items []*json.RawMessage) ([]*json.RawMessage, error) {

	col, err := str.QueryCollection(ctx, params.Namespace, params.Code)
	if err != nil {
		return nil, err
	}

	itemsWithCUP := make([]*json.RawMessage, len(items))
	for index, itemJSON := range items {
		item, err := NewItem(params.Namespace, params.Code, *itemJSON)
		if err != nil {
			return nil, err
		}

		// Вычисляем и дописываем права текущего пользователя на элемент
		itemPTypes, err := item.GetPermissionTypesForCurrentUser(ctx, str, auth, false, col)
		if err != nil {
			return nil, err
		}

		model := itemWithCurrentUserPermissions{
			Item: item,
			CurrentUserPermissions: itemPTypes,
		}
		modelBytes, err := model.MarshalJSON()
		if err != nil {
			return nil, err
		}
		modelJSON := json.RawMessage(modelBytes)
		itemsWithCUP[index] = &modelJSON
	}

	return itemsWithCUP, nil
}

type itemWithCurrentUserPermissions struct {
	*Item
	CurrentUserPermissions []permissions.PermissionType
}

func (item itemWithCurrentUserPermissions) MarshalJSON() ([]byte, error) {
	if item.CurrentUserPermissions == nil {
		return item.Body, nil
	}

	permsJSON, err := json.Marshal(item.CurrentUserPermissions)
	if err != nil {
		return nil, err
	}

	bodyMap := map[string]json.RawMessage{}
	err = json.Unmarshal(item.Body, &bodyMap)
	if err != nil {
		return nil, err
	}
	bodyMap[CurrentUserPermissions] = permsJSON

	return json.Marshal(bodyMap)
}
