package item

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"

	"git.elewise.com/elma365/main/src/auth"
	"git.elewise.com/elma365/main/src/common/config"
	"git.elewise.com/elma365/main/src/common/errs"
	"git.elewise.com/elma365/main/src/common/tracing"
	"github.com/asaskevich/govalidator"
	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

// NewRouter is a collection items endpoint
func NewRegRouter(cfg *config.Config, auth *auth.Auth, str Store) chi.Router {
	r := chi.NewRouter()

	r.Use(str.WithTx)
	r.Post("/reserve", regReserve(str))
	r.Delete("/reserve", regRelease(str))
	r.Post("/register", regRegister(str))
	r.Put("/change", regChange(str))

	return r
}

func regReserve(str Store) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		item, l, ctx, err := getDataForRegOp(str, w, r)
		if err != nil {
			return
		}
		if err := item.RegReserve(ctx, str); err != nil {
			errs.Handle(l, w, err)
			return
		}
		_, err = str.SaveItems(ctx, []Item{*item}, nil)
		if err != nil {
			errs.Handle(l, w, err)
		}
		w.Header().Set("Content-Type", "application/json")
		io.Copy(w, bytes.NewBuffer(item.Body))
	})
}

func regRelease(str Store) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		item, l, ctx, err := getDataForRegOp(str, w, r)
		if err != nil {
			return
		}
		if err := item.RegRelease(ctx, str); err != nil {
			errs.Handle(l, w, err)
			return
		}
		_, err = str.SaveItems(ctx, []Item{*item}, nil)
		if err != nil {
			errs.Handle(l, w, err)
		}
		w.Header().Set("Content-Type", "application/json")
		io.Copy(w, bytes.NewBuffer(item.Body))
	})
}

func regRegister(str Store) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		item, l, ctx, err := getDataForRegOp(str, w, r)
		if err != nil {
			return
		}
		params := struct {
			Name  string `json:"name"`
			Value int64  `json:"value"`
		}{
			Name:  "",
			Value: 0,
		}

		jd := json.NewDecoder(r.Body)
		if err := jd.Decode(&params); err != nil {
			l.Debug("unmarshal", zap.Error(err))
			http.Error(w, errors.Wrap(err, "unmarshal").Error(), http.StatusBadRequest)
			return
		}
		if params.Name == "" {
			if err := item.RegRegister(ctx, str); err != nil {
				errs.Handle(l, w, err)
				return
			}
		} else if params.Value == 0 {
			if err := item.RegManual(ctx, str, params.Name); err != nil {
				errs.Handle(l, w, err)
				return
			}
		} else {
			if err := item.RegRegisterNew(ctx, str, params.Name, params.Value); err != nil {
				errs.Handle(l, w, err)
				return
			}
		}
		_, err = str.SaveItems(ctx, []Item{*item}, nil)
		if err != nil {
			errs.Handle(l, w, err)
		}
		w.Header().Set("Content-Type", "application/json")
		io.Copy(w, bytes.NewBuffer(item.Body))
	})
}

func regChange(str Store) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		item, l, ctx, err := getDataForRegOp(str, w, r)
		if err != nil {
			return
		}

		params := struct {
			Name string `json:"name"`
		}{
			Name: "",
		}

		jd := json.NewDecoder(r.Body)
		if err := jd.Decode(&params); err != nil {
			l.Debug("unmarshal", zap.Error(err))
			http.Error(w, errors.Wrap(err, "unmarshal").Error(), http.StatusBadRequest)
			return
		}

		if err := item.RegChange(ctx, str, params.Name); err != nil {
			errs.Handle(l, w, err)
			return
		}
		_, err = str.SaveItems(ctx, []Item{*item}, nil)
		if err != nil {
			errs.Handle(l, w, err)
		}
		w.Header().Set("Content-Type", "application/json")
		io.Copy(w, bytes.NewBuffer(item.Body))
	})
}

func getDataForRegOp(str Store, w http.ResponseWriter, r *http.Request) (*Item, *zap.Logger, context.Context, error) {
	ctx := r.Context()
	l := tracing.LoggerFromContext(ctx)
	params := struct {
		ID string `valid:"uuid,required"`
	}{
		chi.URLParam(r, "id"),
	}

	if _, err := govalidator.ValidateStruct(params); err != nil {
		l.Debug("validate", zap.Error(err))
		http.Error(w, errors.Wrap(err, "validate").Error(), http.StatusBadRequest)
		return nil, l, ctx, err
	}

	item, err := str.GetItemByID(ctx, params.ID)
	if err != nil {
		errs.Handle(l, w, err)
		return nil, l, ctx, err
	}
	return item, l, ctx, nil
}
