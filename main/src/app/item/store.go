package item

//go:generate mockgen -source=store.go -destination=store_mock_test.go -package=item

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"git.elewise.com/elma365/main/src/app/appview"
	"git.elewise.com/elma365/main/src/auth/claims"
	"git.elewise.com/elma365/main/src/collection"
	"git.elewise.com/elma365/main/src/common/errs"
	"git.elewise.com/elma365/main/src/common/event"
	"git.elewise.com/elma365/main/src/common/list"
	"git.elewise.com/elma365/main/src/common/mw"
	"git.elewise.com/elma365/main/src/common/permissions"
	"git.elewise.com/elma365/main/src/common/queries"
	"git.elewise.com/elma365/main/src/common/tracing"
	"git.elewise.com/elma365/main/src/common/util"
	"git.elewise.com/elma365/main/src/namespace/serial"
	"git.elewise.com/elma365/main/src/registry"

	"github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

// InnerStore describes items strore uniq methods
type InnerStore interface {
	GetItems(ctx context.Context, namespace, code string, params *list.Params) (*list.Result, error)
	GetItemByID(ctx context.Context, id string) (*Item, error)
	GetItemsByIds(ctx context.Context, ids []string) ([]Item, error)
	QueryItemByID(ctx context.Context, namespace, code, id string) (*Item, error)
	QueryItemsByIds(ctx context.Context, app *appview.AppView, ids []string) (json.RawMessage, error)
	SaveItems(ctx context.Context, items []Item, defaultPermissions *permissions.Permissions) ([]Item, error)

	GetItemPermissions(ctx context.Context, namespace, code, id string) (*permissions.Permissions, error)
	GetItemPermissionsAsJSON(ctx context.Context, namespace, code, id string) (json.RawMessage, error)
	SaveItemPermissions(ctx context.Context, item *Item, permissions permissions.Permissions) error
}

// OuterStore describes shares methods
type OuterStore interface {
	serial.Store

	registry.InnerStore

	GetApp(ctx context.Context, namespace, code string) (*appview.AppView, error)
	QueryApp(ctx context.Context, namespace, code string, activeOnly bool) (*appview.AppView, error)
	GetCollection(ctx context.Context, namespace, code string) (*collection.Collection, error)
	QueryCollection(ctx context.Context, namespace, code string) (*collection.Collection, error)
	GetCollectionPermissionsSettings(ctx context.Context, namespace, code string) (*collection.PermissionsSettings, error)
}

// Store describes all store methods
type Store interface {
	InnerStore
	OuterStore
}

// ItemStore is a methods to work with collection items
type ItemStore struct {
	dbx *sqlx.DB
	sbt squirrel.StatementBuilderType
	qc  *queries.Client
}

// NewStore collection store
func NewStore(dbx *sqlx.DB, sbt squirrel.StatementBuilderType, qc *queries.Client) *ItemStore {
	return &ItemStore{dbx, sbt, qc}
}

// GetItemByID gets application item
func (s *ItemStore) GetItemByID(ctx context.Context, id string) (*Item, error) {
	items, err := s.GetItemsByIds(ctx, []string{id})
	if err != nil {
		return nil, err
	}

	if len(items) == 0 {
		return nil, errs.ModelNotFound
	}
	if len(items) > 1 {
		return nil, errors.Errorf("found multiple items with id <%s>", id)
	}

	return &items[0], nil
}

// GetItemsByIds gets application items by ids
func (s *ItemStore) GetItemsByIds(ctx context.Context, ids []string) ([]Item, error) {
	items := []Item{}
	err := tracing.WithDBSpan(ctx, "GetItemsByIds", func(ctx context.Context) error {
		company := mw.CompanyFromContext(ctx)

		query, args, err := s.sbt.
			Select("id", "collection_namespace", "collection_code", "body", "created_at", "updated_at", "created_by", "updated_by").
			From(fmt.Sprintf("%q.items", company)).
			Where(squirrel.Eq{"id": ids}).
			Where(squirrel.Eq{"deleted_at": nil}).
			ToSql()
		if err != nil {
			return errors.WithStack(err)
		}

		return errors.WithStack(s.dbx.SelectContext(ctx, &items, query, args...))
	})

	return items, err
}

// QueryItemByID gets item
func (s *ItemStore) QueryItemByID(ctx context.Context, namespace, code, id string) (*Item, error) {
	request := queries.NewCollectionItemRequest(namespace, code, id)
	res, err := request.Execute(ctx, s.qc)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	item := Item{}
	err = json.Unmarshal(*res.Item, &item)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	item.CollectionNamespace = namespace
	item.CollectionCode = code
	item.Body = *res.Item

	return &item, nil
}

// GetItems gets application items
func (s *ItemStore) GetItems(ctx context.Context, namespace, code string, params *list.Params) (*list.Result, error) {
	request := queries.NewCollectionItemsRequest(namespace, code)
	request.Filter = params.Filter
	request.Options = params.Options

	res, err := request.Execute(ctx, s.qc)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return &list.Result{Items: res.Items, Total: res.Total}, nil
}

// QueryItemsByIds gets application items by ids from read model
func (s *ItemStore) QueryItemsByIds(ctx context.Context, app *appview.AppView, ids []string) (json.RawMessage, error) {

	request := queries.NewCollectionItemsRequest(app.Namespace, app.Code)
	request.Filter.Ids = ids
	request.Filter.Active = true

	res, err := request.Execute(ctx, s.qc)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	bytes, err := json.Marshal(res.Items)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return bytes, nil
}

// GetItemPermissions gets element permissions
func (s *ItemStore) GetItemPermissions(ctx context.Context, namespace, code, id string) (*permissions.Permissions, error) {
	permsJSON, err := s.GetItemPermissionsAsJSON(ctx, namespace, code, id)
	if err != nil {
		return nil, err
	}

	if permsJSON == nil {
		return &permissions.Permissions{}, nil
	}

	perms := permissions.Permissions{}
	err = json.Unmarshal(permsJSON, &perms)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return &perms, nil
}

// GetItemPermissionsAsJSON gets element permissions as json
func (s *ItemStore) GetItemPermissionsAsJSON(ctx context.Context, namespace, code, id string) (json.RawMessage, error) {
	// Тут повторяется QueryItemByID потому как в будущем QueryItemByID не будет отдавать лишние данные элемента,
	// в том числе права доступа
	request := queries.NewCollectionItemRequest(namespace, code, id)
	res, err := request.Execute(ctx, s.qc)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	itemMap := map[string]json.RawMessage{}
	err = json.Unmarshal(*res.Item, &itemMap)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return itemMap["__permissions"], nil
}

// SaveItems save items to db
func (s *ItemStore) SaveItems(ctx context.Context, items []Item, defaultPermissions *permissions.Permissions) (
	[]Item, error) {

	changedItems := make([]Item, 0)
	err := tracing.WithDBSpan(ctx, "SaveItems", func(ctx context.Context) error {
		if len(items) == 0 {
			return nil
		}
		tx := mw.TxFromContext(ctx)
		var err error = nil
		ts := time.Now().UTC()
		events := make([]event.Item, 0)
		for _, item := range items {
			var changedItem Item
			var evnts []event.Item

			if item.CreatedAt.IsZero() {
				changedItem, evnts, err = s.create(ctx, tx, item, ts, defaultPermissions)
			} else {
				changedItem, evnts, err = s.update(ctx, tx, item, ts)
			}
			if err != nil {
				return err
			}

			events = append(events, evnts...)
			changedItems = append(changedItems, changedItem)
		}

		err = event.Save(ctx, tx, ts, events)
		if err != nil {
			return errors.WithStack(err)
		}
		return errors.WithStack(err)
	})

	return changedItems, err
}

// SaveItemPermissions save item permissions
func (s *ItemStore) SaveItemPermissions(ctx context.Context, item *Item, permissions permissions.Permissions) error {
	err := tracing.WithDBSpan(ctx, "SaveItemPermissions", func(ctx context.Context) error {
		tx, err := s.dbx.BeginTxx(ctx, nil)
		if err != nil {
			return errors.WithStack(err)
		}
		defer tx.Rollback()

		ts := time.Now().UTC()
		err = util.EnsurePermissionsNotChangedAfter(ctx, tx, Table, item.ID, ts)
		if err != nil {
			return err
		}

		permissions.Timestamp = ts.Unix()
		query, args, err := s.sbt.
			Update(Table.WithSchema(ctx)).
			Set("permissions", permissions).
			Where(squirrel.Eq{"id": item.ID}).
			Where(squirrel.Eq{"deleted_at": nil}).
			ToSql()
		if err != nil {
			return errors.WithStack(err)
		}

		_, err = tx.ExecContext(ctx, query, args...)
		if err != nil {
			return errors.WithStack(err)
		}

		events := []event.Item{
			{
				NS:    item.CollectionNamespace,
				Code:  item.CollectionCode,
				ID:    item.ID,
				Type:  event.CollectionItemType,
				Scope: event.Permissions,
				Data:  permissions,
			},
		}
		if err := event.Save(ctx, tx, ts, events); err != nil {
			return err
		}

		return errors.WithStack(tx.Commit())
	})

	return err
}

func (s *ItemStore) create(ctx context.Context, tx *sqlx.Tx, item Item, ts time.Time,
	defaultPermissions *permissions.Permissions) (Item, []event.Item, error) {

	userID := claims.UserIDFromContext(ctx)

	item.CreatedAt = ts
	item.UpdatedAt = ts
	item.CreatedBy = userID
	item.UpdatedBy = userID

	syncedBody, err := syncSystemFields(item)
	if err != nil {
		return Item{}, nil, err
	}
	item.Body = syncedBody

	query, args, err := s.sbt.
		Insert(Table.WithSchema(ctx)).
		Columns("id", "collection_namespace", "collection_code", "body", "permissions",
			"created_at", "updated_at", "created_by", "updated_by").
		Values(item.ID, item.CollectionNamespace, item.CollectionCode, item.Body, defaultPermissions,
			item.CreatedAt, item.UpdatedAt, item.CreatedBy, item.UpdatedBy).
		ToSql()
	if err != nil {
		return Item{}, nil, errors.WithStack(err)
	}

	_, err = tx.ExecContext(ctx, query, args...)
	if err != nil {
		return Item{}, nil, errors.WithStack(err)
	}

	events := []event.Item{
		item.PrepareEvent(),
	}

	if defaultPermissions != nil {
		event := defaultPermissions.PrepareEvent(item.CollectionNamespace, item.CollectionCode, item.ID)
		events = append(events, event)
	}

	return item, events, nil
}

func (s *ItemStore) update(ctx context.Context, tx *sqlx.Tx, item Item, ts time.Time) (Item, []event.Item, error) {
	err := util.EnsureNotChangedAfter(ctx, tx, Table, item.ID, ts)
	if err != nil {
		return Item{}, nil, err
	}

	userID := claims.UserIDFromContext(ctx)

	item.UpdatedAt = ts
	item.UpdatedBy = userID

	syncedBody, err := syncSystemFields(item)
	if err != nil {
		return Item{}, nil, err
	}
	item.Body = syncedBody

	// Сохраняем данные
	query, args, err := s.sbt.
		Update(Table.WithSchema(ctx)).
		Set("body", item.Body).
		Set("updated_at", item.UpdatedAt).
		Set("updated_by", userID).
		Where(squirrel.Eq{"id": item.ID}).
		Where(squirrel.Eq{"deleted_at": nil}).
		ToSql()
	if err != nil {
		return Item{}, nil, errors.WithStack(err)
	}

	_, err = tx.ExecContext(ctx, query, args...)
	if err != nil {
		return Item{}, nil, errors.WithStack(err)
	}

	events := []event.Item{item.PrepareEvent()}
	return item, events, nil
}

// syncSystemFields синхронизирует системные поля элемента и его данных (body)
func syncSystemFields(item Item) ([]byte, error) {
	bodyMap := map[string]json.RawMessage{}
	err := json.Unmarshal(item.Body, &bodyMap)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	bodyMap[ID] = json.RawMessage(`"` + item.ID.String() + `"`)
	bodyMap[CreatedAt] = json.RawMessage(`"` + item.CreatedAt.Format(time.RFC3339) + `"`)
	bodyMap[UpdatedAt] = json.RawMessage(`"` + item.UpdatedAt.Format(time.RFC3339) + `"`)
	bodyMap[CreatedBy] = json.RawMessage(`"` + item.CreatedBy.String() + `"`)
	bodyMap[UpdatedBy] = json.RawMessage(`"` + item.UpdatedBy.String() + `"`)

	if item.DeletedAt != nil {
		bodyMap[DeletedAt] = json.RawMessage(`"` + item.DeletedAt.Format(time.RFC3339) + `"`)
	} else if _, ok := bodyMap[DeletedAt]; ok {
		// Удалим что бы не мешалось
		delete(bodyMap, DeletedAt)
	}

	data, err := json.Marshal(&bodyMap)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return data, nil
}
