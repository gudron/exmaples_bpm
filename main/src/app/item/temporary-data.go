package item

import (
	"context"

	"github.com/pkg/errors"
)

type TempData struct {
	Registration *RegistrationData `json:"registration"`
}

type RegistrationData struct {
	SerialEntryValue int64  `json:"serialEntryValue"`
	Name             string `json:"name"`
}

func (t *TempData) process(ctx context.Context, str Store, item *Item) error {
	if t.Registration != nil {
		if err := item.RegRegisterNew(ctx, str, t.Registration.Name, t.Registration.SerialEntryValue); err != nil {
			return errors.WithStack(err)
		}
	}
	return nil
}
