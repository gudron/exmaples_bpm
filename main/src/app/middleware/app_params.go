package middleware

import (
	"context"
	"net/http"

	"git.elewise.com/elma365/main/src/common/tracing"

	"github.com/asaskevich/govalidator"
	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

// AppParams определяет параметры приложения
type AppParams struct {
	Namespace string `valid:"required"`
	Code      string `valid:"required"`
}

type appParamsCtxKey struct{}

// WithAppParams сохраняет в контекст параметры приложения
func WithAppParams(handler http.Handler) http.Handler {
	wrapper := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)

		params := AppParams{
			chi.URLParam(r, "namespace"),
			chi.URLParam(r, "code"),
		}

		if _, err := govalidator.ValidateStruct(params); err != nil {
			l.Debug("validate params", zap.Error(err))
			http.Error(w, errors.Wrap(err, "validate").Error(), http.StatusBadRequest)
			return
		}

		ctx = context.WithValue(ctx, appParamsCtxKey{}, params)
		handler.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(wrapper)
}

// AppParamsFromContext получает параметры текущего приложения из контекста
func AppParamsFromContext(ctx context.Context) AppParams {
	return ctx.Value(appParamsCtxKey{}).(AppParams)
}

// ContextWithAppParams дополняет контекст заданными AppParams параметрами
func ContextWithAppParams(ctx context.Context, appParams AppParams) context.Context {
	return context.WithValue(ctx, appParamsCtxKey{}, appParams)
}

func hasAppParamsInContext(ctx context.Context) bool {
	return ctx.Value(appParamsCtxKey{}) != nil
}
