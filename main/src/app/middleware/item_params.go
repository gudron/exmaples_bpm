package middleware

import (
	"context"
	"net/http"

	"git.elewise.com/elma365/main/src/common/tracing"

	"github.com/asaskevich/govalidator"
	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

// ItemParams определяет параметры элемента
type ItemParams struct {
	AppParams
	ID string `valid:"uuid,required"`
}

type itemParamsCtxKey struct{}

// WithItemParams сохраняет в контекст параметры элемента
func WithItemParams(handler http.Handler) http.Handler {
	wrapper := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)

		params := ItemParams{}
		params.ID = chi.URLParam(r, "id")

		if hasAppParamsInContext(ctx) {
			params.AppParams = AppParamsFromContext(ctx)
		} else {
			params.AppParams = AppParams{
				chi.URLParam(r, "namespace"),
				chi.URLParam(r, "code"),
			}
		}

		if _, err := govalidator.ValidateStruct(params); err != nil {
			l.Debug("validate params", zap.Error(err))
			http.Error(w, errors.Wrap(err, "validate").Error(), http.StatusBadRequest)
			return
		}

		ctx = context.WithValue(ctx, itemParamsCtxKey{}, params)
		handler.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(wrapper)
}

// ItemParamsFromContext получает параметры текущего приложения из контекста
func ItemParamsFromContext(ctx context.Context) ItemParams {
	return ctx.Value(itemParamsCtxKey{}).(ItemParams)
}
