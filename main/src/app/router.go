package app

import (
	"git.elewise.com/elma365/main/src/app/appview"
	"git.elewise.com/elma365/main/src/app/item"
	"git.elewise.com/elma365/main/src/auth"
	"git.elewise.com/elma365/main/src/common/config"
	"git.elewise.com/elma365/main/src/namespace/serial"

	"github.com/go-chi/chi"
)

// NewRouter is a app endpoint
func NewRouter(cfg *config.Config, auth *auth.Auth, str Store) chi.Router {
	r := chi.NewRouter()

	r.Mount("/{namespace}/serial", serial.NewRouter(cfg, auth, str))
	r.Mount("/{namespace}/{code}/items", item.NewRouter(cfg, auth, str))
	r.Mount("/", appview.NewRouter(cfg, auth, str))

	return r
}
