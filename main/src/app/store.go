package app

import (
	"git.elewise.com/elma365/main/src/app/appview"
	"git.elewise.com/elma365/main/src/app/item"
	"git.elewise.com/elma365/main/src/collection"
	"git.elewise.com/elma365/main/src/disk"
	"git.elewise.com/elma365/main/src/namespace/serial"
	"git.elewise.com/elma365/main/src/page"
	"git.elewise.com/elma365/main/src/registry"
)

// Store describes all available methods
type Store interface {
	appview.InnerStore
	item.InnerStore
	collection.Store
	disk.InnerStore
	serial.Store
	registry.InnerStore
	page.InnerStore
}
