package auth

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"

	"git.elewise.com/elma365/main/src/auth/claims"
	"git.elewise.com/elma365/main/src/auth/pb"
	"git.elewise.com/elma365/main/src/common/config"
	"git.elewise.com/elma365/main/src/common/errs"
	"git.elewise.com/elma365/main/src/common/interceptor"
	"git.elewise.com/elma365/main/src/common/permissions"
	"git.elewise.com/elma365/main/src/common/tracing"
	"github.com/bfg-dev/crypto-core/_data/pkg/dep/sources/https---github.com-satori-go.uuid"

	"github.com/go-chi/chi"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/tracing/opentracing"
	"github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	// UserIDHeaderName used for internal api
	UserIDHeaderName  = "X-UserId"
	vahterTokenCookie = "vtoken"
)

// Auth is a serv_auth client wrapper
type Auth struct {
	pb.ServAuthClient
}

// New auth DI container
func New(cfg *config.Config) *Auth {
	conn, err := grpc.Dial(
		"serv-auth",
		grpc.WithInsecure(),
		grpc.WithDialer(cfg.GRPCDialer),
		grpc.WithUnaryInterceptor(grpc_middleware.ChainUnaryClient(
			grpc_opentracing.UnaryClientInterceptor(),
			interceptor.MDClientInterceptor(),
		)),
	)
	if err != nil {

		zap.L().Fatal(err.Error())
	}

	return &Auth{pb.NewServAuthClient(conn)}
}

// HasPermission returns true if user has listed permissions. ptypes is variadic param for checking few permissions at once
// that permissions united with 'AND' operator
func (a *Auth) HasPermission(ctx context.Context, userID uuid.UUID, perms []permissions.Permission, ptypes ...permissions.PermissionType) (bool, error) {
	if len(perms) == 0 {

		return false, nil
	}

	filter := make([]string, len(perms))
	for i, perm := range perms {
		filter[i] = perm.Group.ID.String()
	}

	res, err := a.GetGroups(ctx, &pb.GetGroupsRq{
		UserId: userID.String(),
		Filter: filter,
	})
	if err != nil {

		return false, errors.WithStack(err)
	}

	if len(res.Groups) == 0 {

		return false, nil
	}

	searchMask := permissions.PTypesToMask(ptypes)
	for _, perm := range perms {
		if !stringSliceContain(res.Groups, perm.Group.ID.String()) {

			continue
		}
		permMask := permissions.PTypesToMask(perm.Types)
		searchMask = searchMask ^ (permMask & searchMask)
		if searchMask == 0 {

			return true, nil
		}
	}

	return false, nil
}

// GetUserPermissionTypes gets joined permission types for specified user
func (a *Auth) GetUserPermissionTypes(ctx context.Context, userID uuid.UUID, perms []permissions.Permission) (
	[]permissions.PermissionType, error) {

	if len(perms) == 0 {
		return []permissions.PermissionType{}, nil
	}

	filter := make([]string, len(perms))
	for i, perm := range perms {
		filter[i] = perm.Group.ID.String()
	}

	res, err := a.GetGroups(ctx, &pb.GetGroupsRq{
		UserId: userID.String(),
		Filter: filter,
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	if len(res.Groups) == 0 {
		return []permissions.PermissionType{}, nil
	}

	// Фильтрация perms по полученным группам
	allowedPerms := make([]permissions.Permission, 0)
	for _, perm := range perms {
		for _, group := range res.Groups {
			if perm.Group.ID.String() == group {
				allowedPerms = append(allowedPerms, perm)
				break
			}
		}
	}

	// Объединение масок разрешенных групп
	totalMask := 0
	for _, perm := range allowedPerms {
		permMask := permissions.PTypesToMask(perm.Types)
		totalMask = totalMask | permMask
	}

	if totalMask == 0 {
		return []permissions.PermissionType{}, nil
	}

	return permissions.MaskToPTypes(totalMask), nil
}

func stringSliceContain(slice []string, s string) bool {
	for i := range slice {
		if slice[i] == s {

			return true
		}
	}

	return false
}

// IsUserParentForEmployee checks if employee under user in organisation structure
func (a *Auth) IsUserParentForEmployee(ctx context.Context, userID uuid.UUID, employeeID uuid.UUID) (bool, error) {
	// Получаем группы подчиненных текущего пользователя
	// OPTIMIZATION: Группы можно кэшировать в контексте и дергать лишний раз сервис
	req := pb.GetGroupsRq{
		UserId:           userID.String(),
		ExcludeGroups:    true,
		ExcludeOrgstruct: true,
	}

	res, err := a.GetGroups(ctx, &req)
	if err != nil {
		return false, err
	}

	employeeIDString := employeeID.String()
	for _, subordinateID := range res.Groups {
		if employeeIDString == subordinateID {
			return true, nil
		}
	}

	return false, nil
}

// WithClaimsSystem generate system claims
func (a *Auth) WithClaimsSystem(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		ctx = claims.ContextWithClaims(ctx, &claims.Claims{
			Privileges: claims.PrivilegesToMask([]pb.Privilege{
				pb.Privilege_system,
				pb.Privilege_administration,
			}),
			SessionID: uuid.NewV4(),
			UserID:    uuid.Nil,
		})
		sp := opentracing.SpanFromContext(ctx)
		if sp != nil {
			sp.SetTag("userId", uuid.Nil)
		}

		next.ServeHTTP(w, r.WithContext(ctx))
	}
	return http.HandlerFunc(fn)
}

// WithClaims middleware check Authorization header, parse, check and update token
//
// If token age more then renewTTL, updated claims passed in context and new token added to headers.
//
// Require mw.WithSpan, mw.WithCompany
func (a *Auth) WithClaims(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)

		fromQuery := chi.URLParam(r, "query_from")

		var clm *pb.Claims
		var err error

		switch fromQuery {
		case "internal":
			clm, err = a.getClaimsForInternal(ctx, r)
			if err != nil {
				l.Debug(err.Error(), errs.ZapStack(err))
				http.Error(w, "Unauthorized", http.StatusUnauthorized)
				return
			}
		case "external":
			clm, err = a.getClaimsForExternal(ctx, r)
			if err != nil {
				errs.Handle(l, w, err)
				return
			}
			if len(clm.Token) > 0 {
				if err = a.writeToken(ctx, w, clm.Token); err != nil {
					errs.Internal(l, w, err)
					return
				}
			}
		default:
			err := errors.New("auth method undefined")
			l.Debug(err.Error(), errs.ZapStack(err))
			http.Error(w, "Undefined auth method", http.StatusBadRequest)
			return
		}

		ctx = claims.ContextWithClaims(r.Context(), claims.ProtoToClaims(clm))
		sp := opentracing.SpanFromContext(ctx)
		if sp != nil {
			sp.SetTag("userId", clm.UserId)
		}

		next.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(fn)
}

func (a *Auth) getClaimsForExternal(ctx context.Context, r *http.Request) (*pb.Claims, error) {
	l := tracing.LoggerFromContext(ctx)

	var clm *pb.Claims
	var authError error
	token, err := a.getTokenFromRequest(ctx, r)
	if err != nil {
		// если нет токена, проверим куку
		var cookie *http.Cookie
		cookie, err = r.Cookie(vahterTokenCookie)
		if err != nil {
			return nil, errs.NotAuthorized.WithData("no token or cookie found")
		}

		clm, authError = a.Authorize(ctx, &pb.AuthorizeRq{Vtoken: cookie.Value})
	} else {
		clm, authError = a.CheckToken(ctx, &pb.CheckTokenRq{Token: token})
	}

	if authError != nil {
		s, ok := status.FromError(authError)
		if ok {
			if s.Code() == codes.NotFound || s.Code() == codes.Aborted || s.Code() == codes.FailedPrecondition {
				return nil, errs.NotAuthorized.WithData(s.Message())
			}
			if s.Code() != codes.Internal && s.Code() != codes.Unavailable {
				return nil, errs.NotAuthorized
			}
		}

		l.Debug(err.Error(), errs.ZapStack(authError))
		return nil, errs.InternalError
	}

	return clm, nil
}

func (a *Auth) getClaimsForInternal(ctx context.Context, r *http.Request) (*pb.Claims, error) {
	UserId := r.Header.Get(UserIDHeaderName)
	if len(UserId) == 0 {
		err := errors.New("no user id")
		return nil, err
	}
	clm, err := a.GetClaimsByUserID(ctx, &pb.UserIdRq{Id: UserId})
	if err != nil {
		return nil, errors.WithStack(err)
	}
	return clm, nil
}

// PrivilegeGuard is a middleware shorthand to check user privileges
func (a *Auth) PrivilegeGuard(privilege pb.Privilege) func(http.Handler) http.Handler {

	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()
			if !claims.HasPrivilege(ctx, privilege) {
				l := tracing.LoggerFromContext(ctx)
				l.Debug("insufficient privileges")
				http.Error(w, "Forbidden", http.StatusForbidden)
				return
			}
			next.ServeHTTP(w, r)
		}

		return http.HandlerFunc(fn)
	}
}

// AdministrationGuard check user to be administrator
func (a *Auth) AdministrationGuard(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		if !claims.HasPrivilege(ctx, pb.Privilege_administration) {
			l := tracing.LoggerFromContext(ctx)
			l.Debug("insufficient privileges")
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}
		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

func (a *Auth) getTokenFromRequest(ctx context.Context, r *http.Request) (string, error) {
	authHeader := r.Header.Get("Authorization")
	if len(authHeader) < 8 || strings.ToUpper(authHeader[:7]) != "BEARER " {

		return "", errors.New("no token")
	}

	return authHeader[7:], nil
}

// Router is an auth endpoint
func (a *Auth) Router() chi.Router {
	r := chi.NewRouter()

	r.With(a.WithClaims).Get("/", a.check)

	return r
}

// Check authorization and return claims
func (a *Auth) check(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	l := tracing.LoggerFromContext(ctx)

	clm, err := a.getClaimsForExternal(ctx, r)
	if err != nil {
		errs.Handle(l, w, err)
		return
	}
	if len(clm.Token) > 0 {
		if err = a.writeToken(ctx, w, clm.Token); err != nil {
			errs.Internal(l, w, err)
			return
		}
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	je := json.NewEncoder(w)
	if err := je.Encode(claims.ProtoToClaims(clm)); err != nil {
		l.Error(err.Error())
	}
}

func (a *Auth) writeToken(ctx context.Context, w http.ResponseWriter, token string) error {
	w.Header().Set("Token", token)

	return nil
}
