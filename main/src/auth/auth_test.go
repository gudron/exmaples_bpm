package auth

import (
	"context"
	"testing"

	"git.elewise.com/elma365/main/src/auth/pb"
	"git.elewise.com/elma365/main/src/auth/pb/mock"
	"git.elewise.com/elma365/main/src/common/permissions"

	"github.com/golang/mock/gomock"
	"github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestAuth_HasPermission(t *testing.T) {

	userID := uuid.NewV4()

	cases := []struct {
		name string

		permissions    []permissions.Permission
		checkPermType  []permissions.PermissionType
		responseGroups []string

		hasAccess bool
	}{
		{
			name: "empty source permissions",

			permissions:    []permissions.Permission{},
			checkPermType:  []permissions.PermissionType{permissions.CREATE},
			responseGroups: []string{},

			hasAccess: false,
		},
		{
			name: "no groups",

			permissions: []permissions.Permission{
				{
					Group: permissions.PermissionGroup{
						ID:   uuid.FromStringOrNil("a49305eb-bfcd-4958-b7ea-b7d7a7f80ddc"),
						Type: permissions.User,
					},
					Inherited: true,
					Types:     []permissions.PermissionType{permissions.READ},
				},
			},
			checkPermType:  []permissions.PermissionType{permissions.READ},
			responseGroups: []string{},

			hasAccess: false,
		},
		{
			name: "missed one of permissions",
			permissions: []permissions.Permission{
				{
					Group: permissions.PermissionGroup{
						ID:   uuid.FromStringOrNil("a49305eb-bfcd-4958-b7ea-b7d7a7f80ddc"),
						Type: permissions.User,
					},
					Inherited: true,
					Types:     []permissions.PermissionType{permissions.READ},
				},
			},
			checkPermType:  []permissions.PermissionType{permissions.READ, permissions.UPDATE},
			responseGroups: []string{"a49305eb-bfcd-4958-b7ea-b7d7a7f80ddc"},

			hasAccess: false,
		},
		{
			name: "has permission",
			permissions: []permissions.Permission{
				{
					Group: permissions.PermissionGroup{
						ID:   uuid.FromStringOrNil("a49305eb-bfcd-4958-b7ea-b7d7a7f80ddc"),
						Type: permissions.User,
					},
					Inherited: true,
					Types:     []permissions.PermissionType{permissions.READ},
				},
			},
			checkPermType:  []permissions.PermissionType{permissions.READ},
			responseGroups: []string{"a49305eb-bfcd-4958-b7ea-b7d7a7f80ddc"},

			hasAccess: true,
		},
		{
			name: "has few permissions",
			permissions: []permissions.Permission{
				{
					Group: permissions.PermissionGroup{
						ID:   uuid.FromStringOrNil("a49305eb-bfcd-4958-b7ea-b7d7a7f80ddc"),
						Type: permissions.User,
					},
					Inherited: true,
					Types:     []permissions.PermissionType{permissions.READ, permissions.UPDATE},
				},
			},
			checkPermType:  []permissions.PermissionType{permissions.READ, permissions.UPDATE},
			responseGroups: []string{"a49305eb-bfcd-4958-b7ea-b7d7a7f80ddc"},

			hasAccess: true,
		},
	}

	mctrl := gomock.NewController(t)
	defer mctrl.Finish()

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {

			sauth := mockpb.NewMockServAuthClient(mctrl)
			auth := &Auth{sauth}

			var filter []string
			if len(c.permissions) > 0 {
				filter = []string{c.permissions[0].Group.ID.String()}
			}
			resp := &pb.GetGroupsRq{
				UserId: userID.String(),
				Filter: filter,
			}

			call := sauth.EXPECT().GetGroups(gomock.Any(), gomock.Eq(resp)).AnyTimes()

			call.Return(&pb.GetGroupsRs{
				Groups: c.responseGroups,
			}, nil)

			res, err := auth.HasPermission(context.Background(), userID, c.permissions, c.checkPermType...)
			require.NoError(t, err)

			assert.Equal(t, c.hasAccess, res)

		})
	}
}

func TestAuth_GetUserPermissionTypes(t *testing.T) {
	userID := uuid.NewV4()

	cases := []struct {
		name string

		permissions    []permissions.Permission
		responseGroups []string

		types []permissions.PermissionType
	}{
		{
			name: "empty source permissions",

			permissions:    []permissions.Permission{},
			responseGroups: []string{},

			types: []permissions.PermissionType{},
		},
		{
			name: "one group",

			permissions: []permissions.Permission{
				{
					Group: permissions.PermissionGroup{
						ID: uuid.FromStringOrNil("a49305eb-bfcd-4958-b7ea-b7d7a7f80ddc"),
					},
					Types: []permissions.PermissionType{permissions.READ},
				},
			},
			responseGroups: []string{"a49305eb-bfcd-4958-b7ea-b7d7a7f80ddc"},

			types: []permissions.PermissionType{permissions.READ},
		},
		{
			name: "two groups",

			permissions: []permissions.Permission{
				{
					Group: permissions.PermissionGroup{
						ID: uuid.FromStringOrNil("a49305eb-bfcd-4958-b7ea-b7d7a7f80ddc"),
					},
					Types: []permissions.PermissionType{permissions.READ},
				},
				{
					Group: permissions.PermissionGroup{
						ID: uuid.FromStringOrNil("a49305eb-bfcd-4958-b7ea-b7d7a7f80dd2"),
					},
					Types: []permissions.PermissionType{permissions.UPDATE},
				},
			},
			responseGroups: []string{"a49305eb-bfcd-4958-b7ea-b7d7a7f80ddc", "a49305eb-bfcd-4958-b7ea-b7d7a7f80dd2"},

			types: []permissions.PermissionType{permissions.READ, permissions.UPDATE},
		},
		{
			name: "two groups but one permitted",

			permissions: []permissions.Permission{
				{
					Group: permissions.PermissionGroup{
						ID: uuid.FromStringOrNil("a49305eb-bfcd-4958-b7ea-b7d7a7f80ddc"),
					},
					Types: []permissions.PermissionType{permissions.READ, permissions.CREATE},
				},
				{
					Group: permissions.PermissionGroup{
						ID: uuid.FromStringOrNil("a49305eb-bfcd-4958-b7ea-b7d7a7f80dd2"),
					},
					Types: []permissions.PermissionType{permissions.UPDATE, permissions.DELETE, permissions.ASSIGN},
				},
			},
			responseGroups: []string{"a49305eb-bfcd-4958-b7ea-b7d7a7f80ddc"},

			types: []permissions.PermissionType{permissions.READ, permissions.CREATE},
		},
	}

	mctrl := gomock.NewController(t)
	defer mctrl.Finish()

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			sauth := mockpb.NewMockServAuthClient(mctrl)
			auth := &Auth{sauth}

			call := sauth.EXPECT().GetGroups(gomock.Any(), gomock.Any()).AnyTimes()
			call.Return(&pb.GetGroupsRs{
				Groups: c.responseGroups,
			}, nil)

			types, err := auth.GetUserPermissionTypes(context.Background(), userID, c.permissions)
			require.NoError(t, err)
			assert.Equal(t, c.types, types)
		})
	}
}

func TestAuth_IsUserParentForEmployee(t *testing.T) {

	userID := uuid.NewV4()
	employeeID := uuid.FromStringOrNil("00000000-0000-0000-0001-000000000000")

	cases := []struct {
		name           string
		employeeID     uuid.UUID
		responseGroups []string
		hasAccess      bool
	}{
		{
			name:           "empty response groups",
			employeeID:     employeeID,
			responseGroups: []string{},
			hasAccess:      false,
		},
		{
			name:           "employee is NOT under user",
			employeeID:     employeeID,
			responseGroups: []string{userID.String()},
			hasAccess:      false,
		},
		{
			name:           "employee is under user",
			employeeID:     employeeID,
			responseGroups: []string{userID.String(), employeeID.String()},
			hasAccess:      true,
		},
	}

	mctrl := gomock.NewController(t)
	defer mctrl.Finish()

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {

			sauth := mockpb.NewMockServAuthClient(mctrl)
			auth := &Auth{sauth}

			resp := &pb.GetGroupsRq{
				UserId:           userID.String(),
				ExcludeGroups:    true,
				ExcludeOrgstruct: true,
			}

			call := sauth.EXPECT().GetGroups(gomock.Any(), gomock.Eq(resp)).AnyTimes()
			call.Return(&pb.GetGroupsRs{
				Groups: c.responseGroups,
			}, nil)

			res, err := auth.IsUserParentForEmployee(context.Background(), userID, c.employeeID)
			require.NoError(t, err)

			assert.Equal(t, c.hasAccess, res)
		})
	}
}
