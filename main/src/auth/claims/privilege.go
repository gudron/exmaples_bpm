package claims

import (
	"encoding/json"

	"git.elewise.com/elma365/main/src/auth/pb"

	"github.com/pkg/errors"
)

// Privileges is a bit-mask container of privileges
type PrivilegeMask int32

// PrivilegeToMask convert single privilege to bitmask
func PrivilegeToMask(p pb.Privilege) PrivilegeMask {

	return 1 << uint32(p)
}

// PrivilegesToMask convert multiple privileges to bitmask
func PrivilegesToMask(pp []pb.Privilege) PrivilegeMask {
	var pm PrivilegeMask

	for _, p := range pp {
		pm |= PrivilegeToMask(p)
	}

	return pm
}

var (
	// Owner is a primary user
	Owner PrivilegeMask = PrivilegeToMask(pb.Privilege_administration)
)

// Has privilege in list
func (pm PrivilegeMask) Has(p pb.Privilege) bool {
	return PrivilegeToMask(p)&pm > 0
}

// Contain all given privileges
func (pm PrivilegeMask) Contain(pp PrivilegeMask) bool {
	return (pm&pp)^pp == 0
}

// Extract privileges as slice
func (pm PrivilegeMask) Extract() []pb.Privilege {
	res := []pb.Privilege{}
	for i := range pb.Privilege_name {
		p := pb.Privilege(i)
		if pm.Has(p) {
			res = append(res, p)
		}
	}

	return res
}

// MarshalJSON implements json.Marshaler interface
func (pm PrivilegeMask) MarshalJSON() ([]byte, error) {
	privileges := pm.Extract()
	names := make([]string, len(privileges))
	for i := range privileges {
		names[i] = privileges[i].String()
	}

	return json.Marshal(names)
}

// UnmarshalJSON implements json.Unmarshaler interface
func (pm *PrivilegeMask) UnmarshalJSON(data []byte) error {
	privileges := []string{}
	if err := json.Unmarshal(data, &privileges); err != nil {

		return err
	}
	for _, name := range privileges {
		p, ok := pb.Privilege_value[name]
		if !ok {

			return errors.Errorf("unknown privilege %s", name)
		}
		*pm |= PrivilegeToMask(pb.Privilege(p))
	}

	return nil
}
