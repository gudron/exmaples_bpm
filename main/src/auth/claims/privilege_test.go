package claims

import (
	"encoding/json"
	"testing"

	"git.elewise.com/elma365/main/src/auth/pb"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestPrivilegeToMask(t *testing.T) {
	t.Run("Valid", func(t *testing.T) {
		pm := PrivilegeToMask(pb.Privilege_administration)
		for i := range pb.Privilege_name {
			p := pb.Privilege(i)
			if p == pb.Privilege_administration {
				assert.True(t, pm.Has(p))
			} else {
				assert.False(t, pm.Has(p))
			}
		}
	})
	t.Run("Invalid", func(t *testing.T) {
		pm := PrivilegeToMask(32)
		for i := range pb.Privilege_name {
			p := pb.Privilege(i)
			assert.False(t, pm.Has(p))
		}
	})
}

func TestPrivilegeMaskJSON(t *testing.T) {
	testPrivileges := PrivilegeToMask(pb.Privilege_administration) | PrivilegeToMask(pb.Privilege_system)

	t.Run("Marshal", func(t *testing.T) {
		data, err := json.Marshal(testPrivileges)
		require.NoError(t, err)
		list := []string{}
		require.NoError(t, json.Unmarshal(data, &list), string(data))
		assert.Len(t, list, 2)
		assert.Contains(t, list, "system")
		assert.Contains(t, list, "administration")
	})

	t.Run("Unmarshal", func(t *testing.T) {
		var pp PrivilegeMask

		err := json.Unmarshal([]byte(`["system", "administration"]`), &pp)
		require.NoError(t, err)
		assert.True(t, pp.Contain(testPrivileges))
	})
}

func TestPrivilegesContain(t *testing.T) {
	pp := PrivilegeToMask(pb.Privilege_administration) | PrivilegeToMask(pb.Privilege_system)

	require.False(t, PrivilegeToMask(pb.Privilege_administration).Contain(pp))
	require.True(t, pp.Contain(PrivilegeToMask(pb.Privilege_administration)))
}
