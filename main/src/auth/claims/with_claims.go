package claims

//go:generate easyjson with_claims.go

import (
	"context"

	"git.elewise.com/elma365/main/src/auth/pb"

	"github.com/satori/go.uuid"
)

type key struct{}

// Claims of authorization
//
//easyjson:json
type Claims struct {
	UserID     uuid.UUID     `json:"userId"`
	SessionID  uuid.UUID     `json:"sessionId"`
	Privileges PrivilegeMask `json:"privileges"`
}

// ProtoToClaims convert protobuf message to json frendly claims
func ProtoToClaims(src *pb.Claims) *Claims {

	return &Claims{
		UserID:     uuid.FromStringOrNil(src.UserId),
		SessionID:  uuid.FromStringOrNil(src.SessionId),
		Privileges: PrivilegesToMask(src.Privileges),
	}
}

// ContextWithClaims save
func ContextWithClaims(ctx context.Context, src *Claims) context.Context {

	return context.WithValue(ctx, key{}, src)
}

// UserIDFromContext extract
func UserIDFromContext(ctx context.Context) uuid.UUID {
	v, ok := ctx.Value(key{}).(*Claims)
	if !ok {

		return uuid.Nil
	}

	return v.UserID
}

// PrivilegesFromContext extract
func PrivilegesFromContext(ctx context.Context) []pb.Privilege {
	v, ok := ctx.Value(key{}).(*Claims)
	if !ok {

		return nil
	}

	return v.Privileges.Extract()
}

// SessionIDFromContext extract
func SessionIDFromContext(ctx context.Context) uuid.UUID {
	v, ok := ctx.Value(key{}).(*Claims)
	if !ok {

		return uuid.Nil
	}

	return v.SessionID
}

// ClaimsFromContext extract
func ClaimsFromContext(ctx context.Context) *Claims {

	return ctx.Value(key{}).(*Claims)
}

// HasPrivilege return specified privilege existance in claims
func HasPrivilege(ctx context.Context, p pb.Privilege) bool {
	clm := ctx.Value(key{}).(*Claims)
	return clm.Privileges.Has(p)
}
