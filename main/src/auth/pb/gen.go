package pb

//go:generate protoc -I. -I../../../vendor --gogofast_out=plugins=grpc:. serv_auth.proto
//go:generate /bin/sh -c "mkdir -p mock && mockgen -source=serv_auth.pb.go -destination=mock/serv_auth_mock.go -package=mockpb -imports .=git.elewise.com/elma365/main/src/auth/pb"
