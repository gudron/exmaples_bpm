package config

import (
	"fmt"
	"net"
	"os"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/tomazk/envcfg"
)

// Config of application
type Config struct {
	// Debug is a flag to log debug level, default false
	Debug bool `envcfg:"ELMA365_MAIN_DEBUG" json:"debug"`
	// Bind string, default ":3000"
	Bind string `envcfg:"ELMA365_MAIN_BIND" json:"bind"`
	// DBName is a postgres database name, default "elma365"
	DBName string `envcfg:"ELMA365_MAIN_DB_NAME" json:"dbName"`
	// ReadTimeout is a net timeout to read headers and request in seconds, default 10
	ReadTimeout int `envcfg:"ELMA365_MAIN_READ_TIMEOUT" json:"readTimeout"`
	// HandlerTimeout is a timeout to handler deadline, after that request is closing with 503 status in seconds, default 30
	HandlerTimeout int `envcfg:"ELMA365_MAIN_HANDLER_TIMEOUT" json:"handlerTimeout"`

	// ELASTICSEARCH
	ElasticUser     string `envcfg:"ELMA365_MAIN_ELASTIC_USER"`
	ElasticPassword string `envcfg:"ELMA365_MAIN_ELASTIC_PASSWORD"`

	// POSTGRESQL
	PostgresqlUser     string `envcfg:"ELMA365_MAIN_POSTGRESQL_USER"`
	PostgresqlPassword string `envcfg:"ELMA365_MAIN_POSTGRESQL_PASSWORD"`

	// MONGODB
	MongoUser     string `envcfg:"ELMA365_MAIN_MONGO_USER"`
	MongoPassword string `envcfg:"ELMA365_MAIN_MONGO_PASSWORD"`

	// DNSSuffix нужен для нормальной работы telepresene. Иначе у нее глюки с SRV records
	DNSSuffix string `envcfg:"KUBE_DNS_SUFFIX"`

	// Vahter
	VahterHost       string `envcfg:"ELMA365_MAIN_VAHTER_HOST" json:"vahterHost"`
	VahterLinkDomain string `envcfg:"ELMA365_MAIN_VAHTER_LINK_DOMAIN"`

	// Invite
	InviteExpiredInDays int `envcfg:"ELMA365_MAIN_INVITE_EXPIRED_IN_DAYS" json:"inviteExpiredInDays"`
}

// Default Config
func Default() *Config {
	c := &Config{
		Debug:          false,
		Bind:           ":3000",
		DBName:         "elma365",
		ReadTimeout:    10,
		HandlerTimeout: 30,

		InviteExpiredInDays: 3,
		VahterLinkDomain:    "t-elma365",
	}

	return c
}

// ReadEnv variables to static configuration
func (c *Config) ReadEnv() error {
	return errors.WithStack(envcfg.Unmarshal(&c))
}

// DNSResolve service ip:port from consul
func (c *Config) DNSResolve(service, proto, name string) (string, error) {
	addr, ok := c.addrFromEnv(name, service)
	if !ok {
		if len(c.DNSSuffix) > 0 {
			if c.DNSSuffix[0] == '.' {
				name = name + c.DNSSuffix
			} else {
				name = name + "." + c.DNSSuffix
			}
		}
		cname, addrs, err := net.LookupSRV(service, proto, name)
		if err != nil {

			return addr, errors.WithStack(err)
		}

		if len(addrs) == 0 {

			return addr, errors.Errorf("SRV Lookup for %q service not found", name)
		}
		addr = fmt.Sprintf("%s:%d", cname, addrs[0].Port)
	}

	return addr, nil
}

func (c *Config) addrFromEnv(name, service string) (string, bool) {
	name = strings.ToUpper(strings.Replace(name, "-", "_", -1))
	host, ok := os.LookupEnv(fmt.Sprintf("%s_SERVICE_HOST", name))
	if !ok {

		return "", false
	}
	portName := strings.ToUpper(strings.Replace(service, "_", "", -1))
	port, ok := os.LookupEnv(fmt.Sprintf("%s_SERVICE_PORT_%s", name, strings.ToUpper(portName)))
	if !ok {

		return "", false
	}

	return fmt.Sprintf("%s:%s", host, port), true
}

// Dialer return closured service dialer
func (c *Config) Dialer(service, proto, name string) func() (net.Conn, error) {
	return func() (net.Conn, error) {
		addr, err := c.DNSResolve(service, proto, name)
		if err != nil {

			return nil, err
		}
		cn, err := net.Dial(proto, addr)

		return cn, errors.WithStack(err)
	}
}

// GRPCDialer is a method to pass it in grpc.Dial options
func (c *Config) GRPCDialer(service string, timeout time.Duration) (net.Conn, error) {
	addr, err := c.DNSResolve("grpc", "tcp", service)
	if err != nil {

		return nil, err
	}
	cn, err := net.DialTimeout("tcp", addr, timeout)

	return cn, errors.WithStack(err)
}
