package config

import (
	"fmt"
	"io"
	"os"

	"github.com/pkg/errors"
	jConfig "github.com/uber/jaeger-client-go/config"
)

// Jaeger init global tracer
func Jaeger(cfg *Config) (io.Closer, error) {
	// 5775 - порт для UDP agent-zipkin-thrift по умолчанию
	jaddr := fmt.Sprintf("%s:%d", os.Getenv("HOST_IP"), 5775)
	jCfg := jConfig.Configuration{
		Reporter: &jConfig.ReporterConfig{
			LocalAgentHostPort: jaddr,
		},
		Sampler: &jConfig.SamplerConfig{
			Type:  "const",
			Param: 1.0, // sample all traces
		},
	}
	closer, err := jCfg.InitGlobalTracer("elma365.main")
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return closer, nil
}
