package config

import (
	"log"

	"go.uber.org/zap"
	"google.golang.org/grpc/grpclog"
)

// Logger init global zap logger
func Logger(cfg *Config) *zap.Logger {
	lCfg := zap.NewProductionConfig()
	lCfg.DisableStacktrace = true // We use errs.ZapStack to get stacktrace
	if cfg.Debug {
		lCfg.Level.SetLevel(zap.DebugLevel)
	}
	l, err := lCfg.Build()
	if err != nil {
		log.Fatal(err.Error())
	}
	l = l.Named("elma365.main")
	zap.ReplaceGlobals(l)
	grpclog.SetLogger(zap.NewStdLog(zap.L()))

	return l
}
