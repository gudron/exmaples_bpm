package common

import "context"

// Entity - инетрфейс для сущностей. Описывает набор методов, которые должны быть реализованы в сущностях
type Entity interface {
	// CopyWithUpdateMetadata - копирование объекта с измением метаданных (ID, CreatedAt, и т.д.)
	CopyWithUpdateMetadata(ctx context.Context) interface{}
}
