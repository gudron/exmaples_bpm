package errs

// ConstantError is a package level errors to abstracted services
// implementations
type ConstantError string

func (c ConstantError) Error() string {
	return string(c)
}

// WithData fills error with specified data object
func (c ConstantError) WithData(data interface{}) error {
	return &errorWithData{
		orig: c,
		data: data,
	}
}

const (
	// InternalServerError is a message for 500 status
	InternalServerError = "something went wrong"

	// ModelChanged since last query
	ModelChanged ConstantError = "model has been changed"
	// ModelNotFound in store
	ModelNotFound ConstantError = "model not found"
	// ModelCollision is an uniq index collision
	ModelCollision ConstantError = "model collision"
	// ModelDeleted error returns if model has been deleted
	ModelDeleted ConstantError = "model deleted"

	// NotEmpty to delete
	NotEmpty ConstantError = "not empty"
	// AdminLost error returned when last admin lost him priveleges
	AdminLost ConstantError = "admin lost"
	// Denied error returns if some prerequisite has been failed
	Denied ConstantError = "prerequisite failed"
	// PermissionDenied error return if user don`t have permissions to make an action`
	PermissionDenied ConstantError = "permission denied"
	// NotAuthorized error
	NotAuthorized ConstantError = "not authorized"
	// ValidationError is a validation error
	ValidationError ConstantError = "validation error"
	// BadRequest error
	BadRequest ConstantError = "bad request"
	// InternalError error
	InternalError ConstantError = InternalServerError
)
