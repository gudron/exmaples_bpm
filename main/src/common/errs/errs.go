package errs

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/pkg/errors"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const stacktraceName = "stacktrace"

type withCause interface {
	Cause() error
}

type withStackTrace interface {
	StackTrace() errors.StackTrace
}

// ZapStack try get stacktrace from error and convert it in zapcore.Field
func ZapStack(err error) zapcore.Field {
	var st errors.StackTrace
	for err != nil {
		if e, ok := err.(withStackTrace); ok {
			st = e.StackTrace()
		}
		cause, ok := err.(withCause)
		if !ok {
			break
		}
		err = cause.Cause()
	}

	if st == nil {

		return zap.Stack(stacktraceName)
	}

	return zap.String(
		stacktraceName,
		strings.TrimSpace(
			fmt.Sprintf("%+v", st),
		),
	)
}

// Internal is a shortcut to log error and return 500 to client
func Internal(l *zap.Logger, w http.ResponseWriter, err error) {
	l.Error(err.Error(), ZapStack(err))
	http.Error(w, InternalServerError, http.StatusInternalServerError)
}

// Handle error according to it's type
func Handle(l *zap.Logger, w http.ResponseWriter, err error) {
	switch errors.Cause(err) {
	case ModelChanged:
		w.WriteHeader(http.StatusConflict)
	case ModelNotFound:
		w.WriteHeader(http.StatusNotFound)
	case NotAuthorized:
		w.WriteHeader(http.StatusUnauthorized)
	case PermissionDenied:
		w.WriteHeader(http.StatusForbidden)
	case ValidationError:
		w.WriteHeader(http.StatusBadRequest)
	case ModelCollision:
		w.WriteHeader(http.StatusConflict)
	case ModelDeleted:
		w.WriteHeader(http.StatusGone)
	default:
		Internal(l, w, err)
		return
	}

	if dataErr, ok := err.(ErrorWithData); ok {
		dataErr.WriteData(w)
	} else {
		w.Write([]byte(err.Error()))
	}
}

func GRPCHandle(l *zap.Logger, w http.ResponseWriter, err error) {
	c := status.Code(err)
	switch c {
	case codes.InvalidArgument:
		w.WriteHeader(http.StatusBadRequest)

	case codes.NotFound:
		w.WriteHeader(http.StatusNotFound)

	case codes.PermissionDenied:
		w.WriteHeader(http.StatusUnauthorized)

	case codes.Unimplemented:
		w.WriteHeader(http.StatusNotImplemented)

	case codes.FailedPrecondition:
		w.WriteHeader(http.StatusPreconditionFailed)

	case codes.AlreadyExists:
		w.WriteHeader(http.StatusConflict)

	default:
		Internal(l, w, err)
		return
	}
}
