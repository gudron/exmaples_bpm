package errs

import "github.com/pkg/errors"

// RecoverError - функция для приведения ошибки, полученной из recover в тип error
// Если восстановить не получается, создается ошибка восстановления ошибки
// Пример использования:
//
// У функции должен быть возвращаемое именнованое  (в примере имя err) значение типа error
//
// func Foo() (err error){
//   defer func(){
//     err = errs.RecoverError(recover())
//   }()
//   doSomething...
// }
//
// @Warning не переносите вызов recover в тело этой функции, recover должен вызываться в контексте defer паникующией функции
func RecoverError(recoverVal interface{}) error {
	if recoverVal != nil {
		err, ok := recoverVal.(error)
		if !ok {
			return errors.Errorf("Unexpected recovered type %T: %v", recoverVal, recoverVal)
		}
		return err
	}
	return nil
}
