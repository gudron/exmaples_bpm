package errs

import (
	"net/http"

	"github.com/pkg/errors"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type errorWithCode struct {
	code int
	orig error
}

// WithCode wrap error with code
func WithCode(orig error, code int) error {
	return &errorWithCode{
		code: code,
		orig: orig,
	}
}

var grpcToHttpCode = map[codes.Code]int{
	codes.InvalidArgument:  http.StatusBadRequest,
	codes.DeadlineExceeded: http.StatusGatewayTimeout,
}

func WithGRPCCode(err error, rules map[codes.Code]int) error {
	grpcCode := codes.Unknown
	msg := err.Error()
	if s, ok := status.FromError(err); ok {
		grpcCode = s.Code()
		msg = s.Message()
	}
	httpCode, ok := rules[grpcCode]
	if !ok {
		httpCode, ok = grpcToHttpCode[grpcCode]
		if !ok {
			httpCode = http.StatusInternalServerError
		}
	}

	return WithCode(errors.New(msg), httpCode)
}

func (ewc *errorWithCode) Error() string {
	return ewc.orig.Error()
}

func (ewc *errorWithCode) Cause() error {
	return ewc.orig
}

func (ewc *errorWithCode) Code() int {
	return ewc.code
}

type withCode interface {
	Code() int
}

// CodeFromError extract if any
func CodeFromError(err error) (int, bool) {
	for {
		switch terr := err.(type) {
		case withCode:

			return terr.Code(), true

		case withCause:

			err = terr.Cause()

		default:

			return 0, false
		}
	}
}
