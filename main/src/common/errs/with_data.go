package errs

import (
	"encoding/json"
	"io"

	"github.com/asaskevich/govalidator"
	"github.com/pkg/errors"
)

type errorWithData struct {
	orig error
	data interface{}
}

func (ewd *errorWithData) Error() string {
	return ewd.orig.Error()
}

func (ewd *errorWithData) Cause() error {
	return ewd.orig
}

func (ewd *errorWithData) GetData() interface{} {
	return ewd.data
}

func (ewd *errorWithData) WriteData(w io.Writer) error {
	je := json.NewEncoder(w)
	if err := je.Encode(ewd.data); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

type ErrorWithData interface {
	GetData() interface{}
	WriteData(w io.Writer) error
}

// WriteDataFromError to writer
//
// We walking up by errors chain until find data or chain is ended.
// If chain ended but data is not found, then error will be writed
// as string or as is if it is valid json. If data has been found it will be writed.
func WriteDataFromError(orig error, w io.Writer) error {
	err := orig
	for {
		switch terr := err.(type) {
		case ErrorWithData:

			return terr.WriteData(w)

		case withCause:

			err = terr.Cause()

		default:
			data := []byte(orig.Error())
			if !json.Valid([]byte(data)) {
				data = append(data, '"', '"')
				copy(data[1:], data)
				data[0] = '"'
			}
			_, err = w.Write(data)

			return errors.WithStack(err)
		}
	}
}

func ValidationErrorAsData(src error) error {
	validationErrors, ok := src.(govalidator.Errors)
	if !ok {

		return src
	}
	res := make(map[string]string, len(validationErrors.Errors()))
	for _, item := range validationErrors.Errors() {
		validationError, ok := item.(govalidator.Error)
		if !ok {
			return src
		}
		res[validationError.Name] = validationError.Err.Error()
	}

	return ValidationError.WithData(res)
}
