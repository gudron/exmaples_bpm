package common

import (
	processor "git.elewise.com/elma365/main/src/bpm/pb"
	"git.elewise.com/elma365/main/src/common/config"
	"git.elewise.com/elma365/main/src/common/interceptor"
	mailerpb "git.elewise.com/elma365/main/src/common/mailer/pb"
	filepb "git.elewise.com/elma365/main/src/disk/pb"
	feedpb "git.elewise.com/elma365/main/src/feed/pb"
	settingspb "git.elewise.com/elma365/main/src/settings/pb"

	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/tracing/opentracing"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

// GRPCClients summary grpc clients
type GRPCClients struct {
	filepb.SFileClient
	feedpb.FeederClient
	processor.ProcessorClient
	settingspb.SettingsProviderClient
	mailerpb.MailerProviderClient
}

// NewGRPCClients return interface for all common grpc clients
func NewGRPCClients(cfg *config.Config) (*GRPCClients, error) {
	sfile, err := newSFileClient(cfg)
	if err != nil {
		return nil, err
	}

	feeder, err := newFeederClient(cfg)
	if err != nil {
		return nil, err
	}

	processorClient, err := newProcessorClient(cfg)
	if err != nil {
		return nil, err
	}

	settingsClient, err := newSettingsClient(cfg)
	if err != nil {
		return nil, err
	}

	mailerClient, err := newMailerClient(cfg)
	if err != nil {
		return nil, err
	}

	return &GRPCClients{
		sfile,
		feeder,
		processorClient,
		settingsClient,
		mailerClient,
	}, nil
}

func newProcessorClient(cfg *config.Config) (processor.ProcessorClient, error) {
	conn, err := newGRPCConn(cfg, "processor")
	if err != nil {
		return nil, err
	}
	return processor.NewProcessorClient(conn), nil
}

func newSFileClient(cfg *config.Config) (filepb.SFileClient, error) {
	conn, err := newGRPCConn(cfg, "serv-file")
	if err != nil {
		return nil, err
	}

	return filepb.NewSFileClient(conn), nil
}

func newFeederClient(cfg *config.Config) (feedpb.FeederClient, error) {
	conn, err := newGRPCConn(cfg, "feeder")
	if err != nil {
		return nil, err
	}

	return feedpb.NewFeederClient(conn), nil
}

func newSettingsClient(cfg *config.Config) (settingspb.SettingsProviderClient, error) {
	conn, err := newGRPCConn(cfg, "settings")
	if err != nil {
		return nil, err
	}

	return settingspb.NewSettingsProviderClient(conn), nil
}

func newMailerClient(cfg *config.Config) (mailerpb.MailerProviderClient, error) {
	conn, err := newGRPCConn(cfg, "mailer")
	if err != nil {
		return nil, err
	}

	return mailerpb.NewMailerProviderClient(conn), nil
}

func newGRPCConn(cfg *config.Config, name string) (*grpc.ClientConn, error) {
	conn, err := grpc.Dial(name, grpc.WithInsecure(), grpc.WithDialer(cfg.GRPCDialer),
		grpc.WithUnaryInterceptor(grpc_middleware.ChainUnaryClient(
			grpc_opentracing.UnaryClientInterceptor(),
			interceptor.MDClientInterceptor(),
		)),
	)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return conn, nil
}
