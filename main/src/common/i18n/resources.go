package i18n

import (
	"path/filepath"

	"git.elewise.com/elma365/main/src/assets"

	"github.com/leonelquinteros/gotext"
	"github.com/pkg/errors"
)

// DataI18nDirPath определяет путь до папки с локализационными файлами
const DataI18nDirPath = "i18n"

// DefaultLocale определяет локаль по умолчанию
const DefaultLocale = "en"

var localeToZoneMapping = map[string]string{
	"en":    "com",
	"ru-RU": "ru",
}

// Resources определяет мапу названий локалей и их данных
type Resources map[string]*gotext.Po

// GetZoneByLocale получает зону по заданной локали
func GetZoneByLocale(locale string) (string, error) {
	zone, ok := localeToZoneMapping[locale]
	if !ok {
		defaultZone, ok := localeToZoneMapping[DefaultLocale]
		if !ok {
			return "", errors.New("Default zone not found")
		}
		return defaultZone, nil
	}

	return zone, nil
}

// GetPoByLocale получает локализационные данные для заданной локали
func (rs Resources) GetPoByLocale(locale string) (*gotext.Po, error) {
	po, ok := rs[locale]
	if !ok {
		defaultPo, ok := rs[DefaultLocale]
		if !ok {
			return nil, errors.New("Default locale not found")
		}
		return defaultPo, nil
	}

	return po, nil
}

// Load загружает локализационные файлы
func Load() (*Resources, error) {
	fileNames, err := assets.AssetDir(DataI18nDirPath)
	if err != nil {
		return nil, err
	}

	ll := make(Resources)
	for _, fileName := range fileNames {
		extension := filepath.Ext(fileName)
		name := fileName[0 : len(fileName)-len(extension)]

		path := filepath.Join(DataI18nDirPath, fileName)
		poData, err := assets.Asset(path)
		if err != nil {
			return nil, err
		}

		po := new(gotext.Po)
		po.Parse(string(poData))

		ll[name] = po
	}

	return &ll, nil
}
