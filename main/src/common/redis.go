package common

import (
	"context"
	"time"

	"git.elewise.com/elma365/main/src/common/config"
	"git.elewise.com/elma365/main/src/common/errs"
	"git.elewise.com/elma365/main/src/common/tracing"

	"github.com/go-redis/redis"
	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
	"go.uber.org/zap"
)

const (
	defaultTTL = 20 * time.Second
)

// Redis is a wrapper of redis connection
type Redis struct {
	redis.Client
}

// NewRedis create redis client
func NewRedis(cfg *config.Config) (*Redis, error) {
	redis.SetLogger(zap.NewStdLog(zap.L()))

	return &Redis{*redis.NewClient(&redis.Options{
		Dialer: cfg.Dialer("redis", "tcp", "redis"),
	})}, nil
}

// GetLock return lock hash by key in redis
func (red *Redis) GetLock(ctx context.Context, key string) (string, error) {
	var result string
	err := tracing.WithRedisSpan(ctx, "GetLock", func(ctx context.Context) error {
		var err error
		result, err = red.Get("lock:" + key).Result()
		if err != nil {
			if err == redis.Nil {
				return nil
			}
			return err
		}
		return nil
	})

	return result, err
}

// CreateLock in redis
func (red *Redis) CreateLock(ctx context.Context, key, hash string, ttl time.Duration) (string, error) {
	if len(hash) == 0 {
		hash = uuid.NewV4().String()
	}
	if ttl == 0 {
		ttl = defaultTTL
	}

	err := tracing.WithRedisSpan(ctx, "CreateLock", func(ctx context.Context) error {
		ok, err := red.SetNX("lock:"+key, hash, ttl).Result()
		if err != nil {

			return errors.WithStack(err)
		}
		if !ok {

			return errs.ModelCollision
		}

		return nil
	})

	return hash, err
}

// UpdateLock in redis
func (red *Redis) UpdateLock(ctx context.Context, key string, hash string, ttl time.Duration) error {
	return tracing.WithRedisSpan(ctx, "UpdateLock", func(ctx context.Context) error {
		if ttl == 0 {
			ttl = defaultTTL
		}
		updateScript := redis.NewScript(`
			local val = redis.call("GET", KEYS[1])
			if val == ARGV[1] then
				return redis.call("EXPIRE", KEYS[1], ARGV[2])
			elseif not val then
				return 0
			else
				return -1
			end
		`)
		n, err := updateScript.Run(red, []string{"lock:" + key}, hash, int(ttl/time.Second)).Result()
		if err != nil {

			return errors.WithStack(err)
		}
		switch n.(int64) {
		case 1:

			return nil

		case 0:

			return errs.ModelNotFound

		default:

			return errs.ModelCollision
		}
	})
}

// DeleteLock from redis
func (red *Redis) DeleteLock(ctx context.Context, key string, hash string) error {
	return tracing.WithRedisSpan(ctx, "DeleteLock", func(ctx context.Context) error {
		deleteScript := redis.NewScript(`
			local val = redis.call("GET", KEYS[1])
			if val == ARGV[1] then
				return redis.call("DEL", KEYS[1])
			elseif not val then
				return 0
			else
				return -1
			end
		`)
		n, err := deleteScript.Run(red, []string{"lock:" + key}, hash).Result()
		if err != nil {

			return errors.WithStack(err)
		}
		switch n.(int64) {
		case 1:

			return nil

		case 0:

			return errs.ModelNotFound

		default:

			return errs.ModelCollision
		}
	})
}

// Save any value in redis
func (red *Redis) Save(key string, value interface{}, ttl time.Duration) error {
	return red.Set(key, value, ttl).Err()
}

// Get value by key from redis
func (red *Redis) GetValue(key string) (string, error) {
	value, err := red.Get(key).Result()
	if err != nil {
		if err == redis.Nil {
			return "", nil
		}
		return "", err
	}

	return value, nil
}
