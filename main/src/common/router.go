package common

import (
	"time"

	"git.elewise.com/elma365/main/src/admin"
	"git.elewise.com/elma365/main/src/app"
	"git.elewise.com/elma365/main/src/auth"
	"git.elewise.com/elma365/main/src/bpm"
	"git.elewise.com/elma365/main/src/chat"
	"git.elewise.com/elma365/main/src/common/config"
	"git.elewise.com/elma365/main/src/common/i18n"
	"git.elewise.com/elma365/main/src/common/mw"
	"git.elewise.com/elma365/main/src/common/tracing"
	"git.elewise.com/elma365/main/src/disk"
	"git.elewise.com/elma365/main/src/disk/wopi"
	"git.elewise.com/elma365/main/src/exchange"
	"git.elewise.com/elma365/main/src/feed"
	"git.elewise.com/elma365/main/src/group"
	"git.elewise.com/elma365/main/src/invite"
	"git.elewise.com/elma365/main/src/namespace"
	"git.elewise.com/elma365/main/src/orgstruct"
	"git.elewise.com/elma365/main/src/page"
	"git.elewise.com/elma365/main/src/query"
	"git.elewise.com/elma365/main/src/registry"
	"git.elewise.com/elma365/main/src/scheduler"
	"git.elewise.com/elma365/main/src/search"
	"git.elewise.com/elma365/main/src/settings"
	"git.elewise.com/elma365/main/src/user"

	// "git.elewise.com/elma365/main/src/widget"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func router(cfg *config.Config, auth *auth.Auth, str *Store, grpcClients *GRPCClients, red *Redis,
	i18res *i18n.Resources) chi.Router {

	r := chi.NewRouter()

	r.Use(mw.WithMetrics)
	r.Use(tracing.WithSpan)
	r.Use(mw.WithCompany)
	r.Use(middleware.Timeout(time.Duration(cfg.HandlerTimeout) * time.Second))

	r.Route("/{query_from:(external|internal)}", func(r chi.Router) {
		r.Mount("/auth", auth.Router())
		r.Mount("/invites", invite.NewRouter(cfg, str, auth))
		r.Mount("/settings", settings.NewRouter(cfg, auth, grpcClients))

		// External services area, don`t need claims auth
		r.Group(func(r chi.Router) {
			r.Mount("/disk/wopi", wopi.NewRouter(grpcClients, str, red))
		})

		// User area
		r.Group(func(r chi.Router) {
			r.Use(auth.WithClaims)

			r.Mount("/orgstruct", orgstruct.NewRouter(str, auth))
			r.Mount("/apps", app.NewRouter(cfg, auth, str))
			r.Mount("/namespace", namespace.NewRouter(cfg, auth, str))
			r.Mount("/query", query.NewRouter(str))
			r.Mount("/disk", disk.NewRouter(grpcClients, auth, str))
			r.Mount("/users", user.NewRouter(cfg, str, auth, i18res, grpcClients, grpcClients))
			// r.Mount("/widgets", widget.NewRouter(cfg, str, auth))
			r.Mount("/groups", group.NewRouter(cfg, str, auth))
			r.Mount("/bpm", bpm.NewRouter(cfg, str, red, auth, grpcClients))
			r.Mount("/feed", feed.NewRouter(cfg, auth, str, grpcClients))
			r.Mount("/chats", chat.NewRouter(cfg, str))
			r.Mount("/search", search.NewRouter(str, grpcClients))
			r.Mount("/pages", page.NewRouter(auth, str))
			r.Mount("/scheduler", scheduler.NewRouter(cfg))
			r.Mount("/registry", registry.NewRouter(str))
			r.Mount("/exchange", exchange.NewRouter(str, grpcClients, auth))
		})

	})

	// System api
	r.With(auth.WithClaimsSystem).Mount("/system", admin.NewRouter(str))

	return r
}

// NewRouter api router
func NewRouter(cfg *config.Config, auth *auth.Auth, str *Store, grpcClients *GRPCClients, red *Redis,
	i18res *i18n.Resources) chi.Router {

	api := router(cfg, auth, str, grpcClients, red, i18res)

	r := chi.NewRouter()

	r.Use(middleware.RealIP)

	r.Mount("/debug", middleware.Profiler())
	r.Mount("/metrics", promhttp.Handler())

	r.Mount("/", api)

	return r
}
