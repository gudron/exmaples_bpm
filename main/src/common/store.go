package common

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"git.elewise.com/elma365/main/src/app/appview"
	"git.elewise.com/elma365/main/src/app/item"
	"git.elewise.com/elma365/main/src/bpm"
	"git.elewise.com/elma365/main/src/chat"
	"git.elewise.com/elma365/main/src/collection"
	"git.elewise.com/elma365/main/src/common/config"
	"git.elewise.com/elma365/main/src/common/mw"
	"git.elewise.com/elma365/main/src/common/queries"
	"git.elewise.com/elma365/main/src/common/responsewriter"
	"git.elewise.com/elma365/main/src/common/tracing"
	"git.elewise.com/elma365/main/src/disk"
	"git.elewise.com/elma365/main/src/group"
	"git.elewise.com/elma365/main/src/namespace/serial"
	"git.elewise.com/elma365/main/src/orgstruct"
	"git.elewise.com/elma365/main/src/page"
	"git.elewise.com/elma365/main/src/query"
	"git.elewise.com/elma365/main/src/registry"
	"git.elewise.com/elma365/main/src/search"
	"git.elewise.com/elma365/main/src/user"
	"git.elewise.com/elma365/main/src/vahter"
	"git.elewise.com/elma365/main/src/widget"
	"github.com/Masterminds/squirrel"
	"github.com/bfg-dev/crypto-core/_data/pkg/dep/sources/https---gopkg.in-mgo.v2"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"gopkg.in/mgo.v2"
)

// Store is a collection of method to access to store
type Store struct {
	dbx *sqlx.DB
	*appview.AppStore
	*item.ItemStore
	*bpm.BPTStore
	*collection.CollectionStore
	*disk.DiskStore
	*group.GroupStore
	*orgstruct.OSStore
	*page.PageStore
	*user.UserStore
	*widget.WidgetStore
	*query.QueryStore
	*search.SearchStore
	*chat.ChatStore
	*serial.SerialStore
	*registry.RegistryStore
	*vahter.VahterStore
}

// NewStore on db
func NewStore(ctx context.Context, cfg *config.Config, qc *queries.Client) (*Store, error) {
	sbt := squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar)

	dbx, err := connectoToPostgres(ctx, cfg)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	mongo, err := connectToMongo(ctx, cfg)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return &Store{
		dbx,
		appview.NewStore(dbx, sbt, qc),
		item.NewStore(dbx, sbt, qc),
		bpm.NewStore(dbx, sbt, qc),
		collection.NewStore(dbx, sbt, qc),
		disk.NewStore(dbx, sbt, qc),
		group.NewStore(dbx, sbt, qc),
		orgstruct.NewStore(dbx, sbt, qc),
		page.NewStore(dbx, sbt, qc),
		user.NewStore(dbx, sbt, qc),
		widget.NewStore(dbx, sbt),
		query.NewStore(qc),
		search.NewStore(qc),
		chat.NewStore(dbx, qc, mongo),
		serial.NewStore(dbx, sbt, qc),
		registry.NewStore(dbx, sbt, qc),
		vahter.NewStore(),
	}, nil
}

func connectoToPostgres(ctx context.Context, cfg *config.Config) (*sqlx.DB, error) {
	addr, err := cfg.DNSResolve("postgres", "tcp", "postgres")
	if err != nil {
		return nil, err
	}

	return sqlx.Connect("postgres", fmt.Sprintf(
		"postgres://%s:%s@%s/%s?sslmode=disable",
		cfg.PostgresqlUser, cfg.PostgresqlPassword, addr, cfg.DBName))
}

func connectToMongo(ctx context.Context, cfg *config.Config) (*mgo.Session, error) {
	mongoAddr, err := cfg.DNSResolve("mongo", "tcp", "mongo")
	if err != nil {
		return nil, err
	}

	di := &mgo.DialInfo{
		Addrs:    []string{mongoAddr},
		Timeout:  10 * time.Second,
		Password: cfg.MongoPassword,
		Username: cfg.MongoUser,
		Database: cfg.DBName,
	}
	return mgo.DialWithInfo(di)
}

func (s *Store) WithTx(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		l := tracing.LoggerFromContext(ctx)
		tx, err := s.dbx.BeginTxx(ctx, nil)
		if err != nil {
			l.Debug("start transaction", zap.Error(err))
			http.Error(w, "start transaction", http.StatusInternalServerError)
			return
		}
		ctx = mw.ContextWithTx(ctx, tx)
		ww := responsewriter.NewWrapResponseWriter(w)
		next.ServeHTTP(ww, r.WithContext(ctx))
		if ww.HasError() {
			tx.Rollback()
		} else if err = tx.Commit(); err != nil {
			l.Debug("commit transaction", zap.Error(err))
			http.Error(ww, "commit transaction", http.StatusInternalServerError)
		}
		ww.End()
	}
	return http.HandlerFunc(fn)
}
