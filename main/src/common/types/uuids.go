package types

//go:generate easyjson uuids.go

import (
	"bytes"
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"sort"

	"github.com/satori/go.uuid"
)

//easyjson:json
type UUIDS []uuid.UUID

// Scan implements sql.Scanner interface
func (us *UUIDS) Scan(pSrc interface{}) error {
	switch src := pSrc.(type) {
	case []byte:
		if err := json.Unmarshal(src, &us); err != nil {
			return err
		}

	default:

		return fmt.Errorf("can`t scan type %T into UUIDS", pSrc)
	}

	return nil
}

// Value implements sql.Valuer interface
func (us UUIDS) Value() (driver.Value, error) {
	return json.Marshal(us)
}

// AsStrings returns slice of UUIDs as slice of strings
func (us UUIDS) AsStrings() []string {
	res := make([]string, 0, len(us))

	for _, u := range us {
		res = append(res, u.String())
	}

	return res
}

// RemoveDuplicates remove duplicate values from array
func (us *UUIDS) RemoveDuplicates() {
	tmp := make(map[uuid.UUID]struct{}, len(*us))

	for _, v := range *us {
		tmp[v] = struct{}{}
	}

	list := make([]uuid.UUID, 0, len(tmp))
	for v := range tmp {
		list = append(list, v)
	}

	*us = list
}

// Contains check array of UUIDS containts specified UUID
func (us UUIDS) Contains(u uuid.UUID) bool {
	for _, v := range us {
		if uuid.Equal(v, u) {
			return true
		}
	}

	return false
}

// Diff compare UUID array with new UUID array
// Return UUIDS that were added as first value
// and deleted UUIDS as second value
func (origin UUIDS) Diff(new UUIDS) (plus UUIDS, minus UUIDS) {
	sort.Sort(origin)
	sort.Sort(new)

	i, j := 0, 0

	for i < len(origin) || j < len(new) {
		if j >= len(new) {
			minus = append(minus, origin[i])
			i++
			continue
		}

		if i >= len(origin) {
			plus = append(plus, new[j])
			j++
			continue
		}

		if origin[i] == new[j] {
			i++
			j++
		} else if bytes.Compare(origin[i].Bytes(), new[j].Bytes()) > 0 {
			plus = append(plus, new[j])
			j++
		} else {
			minus = append(minus, origin[i])
			i++
		}
	}

	return
}

// methods for sort.Interface

// Len ...
func (us UUIDS) Len() int { return len(us) }

// Swap ...
func (us UUIDS) Swap(i, j int) { us[i], us[j] = us[j], us[i] }

// Less ...
func (us UUIDS) Less(i, j int) bool { return bytes.Compare(us[i].Bytes(), us[j].Bytes()) < 0 }
