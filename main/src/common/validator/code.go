package validator

import (
	"regexp"

	"github.com/asaskevich/govalidator"
)

var codeRE = regexp.MustCompile(`^[A-Za-z][A-Za-z0-9\-_]+$`)
var extendedCodeRE = regexp.MustCompile(`^[A-Za-z][A-Za-z0-9\-_]+([.][A-Za-z][A-Za-z0-9\-_]+)?$`)

func init() {
	govalidator.TagMap["code"] = govalidator.Validator(IsCode)
	govalidator.TagMap["extendedCode"] = govalidator.Validator(IsExtendedCode)
}

// IsCode - проверка того, что строка дозволимый код
func IsCode(v string) bool {

	return codeRE.MatchString(v)
}

func IsExtendedCode(v string) bool {
	return extendedCodeRE.MatchString(v)
}
