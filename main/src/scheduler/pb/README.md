

# pb
`import "./internal/service/grpc/pb"`

* [Overview](#pkg-overview)
* [Index](#pkg-index)
* [Subdirectories](#pkg-subdirectories)

## <a name="pkg-overview">Overview</a>
Package pb is a generated protocol buffer package.

### Scheduler

Сервис расписания. Хранит настройки графика рабочего времени, а также расчёт с учётом этого графика и планирование.

* Репозиторий проекта: <a href="https://git.elewise.com/elma365/schedule">https://git.elewise.com/elma365/schedule</a>
* Оригинальный proto-файл: <a href="https://git.elewise.com/elma365/scheduler/blob/develop/internal/service/grpc/pb/scheduler.proto">https://git.elewise.com/elma365/scheduler/blob/develop/internal/service/grpc/pb/scheduler.proto</a>

It is generated from these files:


	scheduler.proto

It has these top-level messages:


	DayInterval
	DaySchedule
	GeneralSettings
	SpecialDay
	SpecialDays
	GetWorkTimeRq
	GetWorkTimeRs




## <a name="pkg-index">Index</a>
* [Variables](#pkg-variables)
* [func RegisterSchedulerServer(s *grpc.Server, srv SchedulerServer)](#RegisterSchedulerServer)
* [type DayInterval](#DayInterval)
  * [func (*DayInterval) Descriptor() ([]byte, []int)](#DayInterval.Descriptor)
  * [func (m *DayInterval) GetFrom() uint32](#DayInterval.GetFrom)
  * [func (m *DayInterval) GetTo() uint32](#DayInterval.GetTo)
  * [func (m *DayInterval) Marshal() (dAtA []byte, err error)](#DayInterval.Marshal)
  * [func (m *DayInterval) MarshalTo(dAtA []byte) (int, error)](#DayInterval.MarshalTo)
  * [func (*DayInterval) ProtoMessage()](#DayInterval.ProtoMessage)
  * [func (m *DayInterval) Reset()](#DayInterval.Reset)
  * [func (m *DayInterval) Size() (n int)](#DayInterval.Size)
  * [func (m *DayInterval) String() string](#DayInterval.String)
  * [func (m *DayInterval) Unmarshal(dAtA []byte) error](#DayInterval.Unmarshal)
* [type DaySchedule](#DaySchedule)
  * [func (*DaySchedule) Descriptor() ([]byte, []int)](#DaySchedule.Descriptor)
  * [func (m *DaySchedule) GetLunchTime() DayInterval](#DaySchedule.GetLunchTime)
  * [func (m *DaySchedule) GetWorkingTime() DayInterval](#DaySchedule.GetWorkingTime)
  * [func (m *DaySchedule) Marshal() (dAtA []byte, err error)](#DaySchedule.Marshal)
  * [func (m *DaySchedule) MarshalTo(dAtA []byte) (int, error)](#DaySchedule.MarshalTo)
  * [func (*DaySchedule) ProtoMessage()](#DaySchedule.ProtoMessage)
  * [func (m *DaySchedule) Reset()](#DaySchedule.Reset)
  * [func (m *DaySchedule) Size() (n int)](#DaySchedule.Size)
  * [func (m *DaySchedule) String() string](#DaySchedule.String)
  * [func (m *DaySchedule) Unmarshal(dAtA []byte) error](#DaySchedule.Unmarshal)
* [type GeneralSettings](#GeneralSettings)
  * [func (*GeneralSettings) Descriptor() ([]byte, []int)](#GeneralSettings.Descriptor)
  * [func (m *GeneralSettings) GetDaySchedule() DaySchedule](#GeneralSettings.GetDaySchedule)
  * [func (m *GeneralSettings) GetWeekends() map[string]bool](#GeneralSettings.GetWeekends)
  * [func (m *GeneralSettings) Marshal() (dAtA []byte, err error)](#GeneralSettings.Marshal)
  * [func (m *GeneralSettings) MarshalTo(dAtA []byte) (int, error)](#GeneralSettings.MarshalTo)
  * [func (*GeneralSettings) ProtoMessage()](#GeneralSettings.ProtoMessage)
  * [func (m *GeneralSettings) Reset()](#GeneralSettings.Reset)
  * [func (m *GeneralSettings) Size() (n int)](#GeneralSettings.Size)
  * [func (m *GeneralSettings) String() string](#GeneralSettings.String)
  * [func (m *GeneralSettings) Unmarshal(dAtA []byte) error](#GeneralSettings.Unmarshal)
* [type GetWorkTimeRq](#GetWorkTimeRq)
  * [func (*GetWorkTimeRq) Descriptor() ([]byte, []int)](#GetWorkTimeRq.Descriptor)
  * [func (m *GetWorkTimeRq) GetStartedAt() google_protobuf2.Timestamp](#GetWorkTimeRq.GetStartedAt)
  * [func (m *GetWorkTimeRq) GetTimeForWork() google_protobuf3.Duration](#GetWorkTimeRq.GetTimeForWork)
  * [func (m *GetWorkTimeRq) Marshal() (dAtA []byte, err error)](#GetWorkTimeRq.Marshal)
  * [func (m *GetWorkTimeRq) MarshalTo(dAtA []byte) (int, error)](#GetWorkTimeRq.MarshalTo)
  * [func (*GetWorkTimeRq) ProtoMessage()](#GetWorkTimeRq.ProtoMessage)
  * [func (m *GetWorkTimeRq) Reset()](#GetWorkTimeRq.Reset)
  * [func (m *GetWorkTimeRq) Size() (n int)](#GetWorkTimeRq.Size)
  * [func (m *GetWorkTimeRq) String() string](#GetWorkTimeRq.String)
  * [func (m *GetWorkTimeRq) Unmarshal(dAtA []byte) error](#GetWorkTimeRq.Unmarshal)
* [type GetWorkTimeRs](#GetWorkTimeRs)
  * [func (*GetWorkTimeRs) Descriptor() ([]byte, []int)](#GetWorkTimeRs.Descriptor)
  * [func (m *GetWorkTimeRs) GetWillDoneAt() *google_protobuf2.Timestamp](#GetWorkTimeRs.GetWillDoneAt)
  * [func (m *GetWorkTimeRs) Marshal() (dAtA []byte, err error)](#GetWorkTimeRs.Marshal)
  * [func (m *GetWorkTimeRs) MarshalTo(dAtA []byte) (int, error)](#GetWorkTimeRs.MarshalTo)
  * [func (*GetWorkTimeRs) ProtoMessage()](#GetWorkTimeRs.ProtoMessage)
  * [func (m *GetWorkTimeRs) Reset()](#GetWorkTimeRs.Reset)
  * [func (m *GetWorkTimeRs) Size() (n int)](#GetWorkTimeRs.Size)
  * [func (m *GetWorkTimeRs) String() string](#GetWorkTimeRs.String)
  * [func (m *GetWorkTimeRs) Unmarshal(dAtA []byte) error](#GetWorkTimeRs.Unmarshal)
* [type SchedulerClient](#SchedulerClient)
  * [func NewSchedulerClient(cc *grpc.ClientConn) SchedulerClient](#NewSchedulerClient)
* [type SchedulerServer](#SchedulerServer)
* [type SpecialDay](#SpecialDay)
  * [func (*SpecialDay) Descriptor() ([]byte, []int)](#SpecialDay.Descriptor)
  * [func (m *SpecialDay) GetDate() int64](#SpecialDay.GetDate)
  * [func (m *SpecialDay) GetHoliday() bool](#SpecialDay.GetHoliday)
  * [func (m *SpecialDay) Marshal() (dAtA []byte, err error)](#SpecialDay.Marshal)
  * [func (m *SpecialDay) MarshalTo(dAtA []byte) (int, error)](#SpecialDay.MarshalTo)
  * [func (*SpecialDay) ProtoMessage()](#SpecialDay.ProtoMessage)
  * [func (m *SpecialDay) Reset()](#SpecialDay.Reset)
  * [func (m *SpecialDay) Size() (n int)](#SpecialDay.Size)
  * [func (m *SpecialDay) String() string](#SpecialDay.String)
  * [func (m *SpecialDay) Unmarshal(dAtA []byte) error](#SpecialDay.Unmarshal)
* [type SpecialDays](#SpecialDays)
  * [func (*SpecialDays) Descriptor() ([]byte, []int)](#SpecialDays.Descriptor)
  * [func (m *SpecialDays) GetSpecialDays() []SpecialDay](#SpecialDays.GetSpecialDays)
  * [func (m *SpecialDays) Marshal() (dAtA []byte, err error)](#SpecialDays.Marshal)
  * [func (m *SpecialDays) MarshalTo(dAtA []byte) (int, error)](#SpecialDays.MarshalTo)
  * [func (*SpecialDays) ProtoMessage()](#SpecialDays.ProtoMessage)
  * [func (m *SpecialDays) Reset()](#SpecialDays.Reset)
  * [func (m *SpecialDays) Size() (n int)](#SpecialDays.Size)
  * [func (m *SpecialDays) String() string](#SpecialDays.String)
  * [func (m *SpecialDays) Unmarshal(dAtA []byte) error](#SpecialDays.Unmarshal)


#### <a name="pkg-files">Package files</a>
[gen.go](/src/target/gen.go) [scheduler.pb.go](/src/target/scheduler.pb.go) 



## <a name="pkg-variables">Variables</a>
``` go
var (
    ErrInvalidLengthScheduler = fmt.Errorf("proto: negative length found during unmarshaling")
    ErrIntOverflowScheduler   = fmt.Errorf("proto: integer overflow")
)
```


## <a name="RegisterSchedulerServer">func</a> [RegisterSchedulerServer](/src/target/scheduler.pb.go?s=10899:10964#L305)
``` go
func RegisterSchedulerServer(s *grpc.Server, srv SchedulerServer)
```



## <a name="DayInterval">type</a> [DayInterval](/src/target/scheduler.pb.go?s=1761:1940#L55)
``` go
type DayInterval struct {
    From uint32 `protobuf:"varint,1,opt,name=from,proto3" json:"from,omitempty"`
    To   uint32 `protobuf:"varint,2,opt,name=to,proto3" json:"to,omitempty"`
}
```
DayInterval — интервал в течении рабочего дня в секундах от 00:00:00










### <a name="DayInterval.Descriptor">func</a> (\*DayInterval) [Descriptor](/src/target/scheduler.pb.go?s=2153:2201#L63)
``` go
func (*DayInterval) Descriptor() ([]byte, []int)
```



### <a name="DayInterval.GetFrom">func</a> (\*DayInterval) [GetFrom](/src/target/scheduler.pb.go?s=2248:2286#L65)
``` go
func (m *DayInterval) GetFrom() uint32
```



### <a name="DayInterval.GetTo">func</a> (\*DayInterval) [GetTo](/src/target/scheduler.pb.go?s=2336:2372#L72)
``` go
func (m *DayInterval) GetTo() uint32
```



### <a name="DayInterval.Marshal">func</a> (\*DayInterval) [Marshal](/src/target/scheduler.pb.go?s=14929:14985#L428)
``` go
func (m *DayInterval) Marshal() (dAtA []byte, err error)
```



### <a name="DayInterval.MarshalTo">func</a> (\*DayInterval) [MarshalTo](/src/target/scheduler.pb.go?s=15125:15182#L438)
``` go
func (m *DayInterval) MarshalTo(dAtA []byte) (int, error)
```



### <a name="DayInterval.ProtoMessage">func</a> (\*DayInterval) [ProtoMessage](/src/target/scheduler.pb.go?s=2101:2135#L62)
``` go
func (*DayInterval) ProtoMessage()
```



### <a name="DayInterval.Reset">func</a> (\*DayInterval) [Reset](/src/target/scheduler.pb.go?s=1942:1971#L60)
``` go
func (m *DayInterval) Reset()
```



### <a name="DayInterval.Size">func</a> (\*DayInterval) [Size](/src/target/scheduler.pb.go?s=20200:20236#L697)
``` go
func (m *DayInterval) Size() (n int)
```



### <a name="DayInterval.String">func</a> (\*DayInterval) [String](/src/target/scheduler.pb.go?s=2014:2051#L61)
``` go
func (m *DayInterval) String() string
```



### <a name="DayInterval.Unmarshal">func</a> (\*DayInterval) [Unmarshal](/src/target/scheduler.pb.go?s=21930:21980#L794)
``` go
func (m *DayInterval) Unmarshal(dAtA []byte) error
```



## <a name="DaySchedule">type</a> [DaySchedule](/src/target/scheduler.pb.go?s=2542:2737#L80)
``` go
type DaySchedule struct {
    LunchTime   DayInterval `protobuf:"bytes,1,opt,name=lunchTime" json:"lunchTime"`
    WorkingTime DayInterval `protobuf:"bytes,2,opt,name=workingTime" json:"workingTime"`
}
```
DaySchedule — расписание рабочего дня: время работы и обеденное время










### <a name="DaySchedule.Descriptor">func</a> (\*DaySchedule) [Descriptor](/src/target/scheduler.pb.go?s=2950:2998#L88)
``` go
func (*DaySchedule) Descriptor() ([]byte, []int)
```



### <a name="DaySchedule.GetLunchTime">func</a> (\*DaySchedule) [GetLunchTime](/src/target/scheduler.pb.go?s=3045:3093#L90)
``` go
func (m *DaySchedule) GetLunchTime() DayInterval
```



### <a name="DaySchedule.GetWorkingTime">func</a> (\*DaySchedule) [GetWorkingTime](/src/target/scheduler.pb.go?s=3160:3210#L97)
``` go
func (m *DaySchedule) GetWorkingTime() DayInterval
```



### <a name="DaySchedule.Marshal">func</a> (\*DaySchedule) [Marshal](/src/target/scheduler.pb.go?s=15428:15484#L456)
``` go
func (m *DaySchedule) Marshal() (dAtA []byte, err error)
```



### <a name="DaySchedule.MarshalTo">func</a> (\*DaySchedule) [MarshalTo](/src/target/scheduler.pb.go?s=15624:15681#L466)
``` go
func (m *DaySchedule) MarshalTo(dAtA []byte) (int, error)
```



### <a name="DaySchedule.ProtoMessage">func</a> (\*DaySchedule) [ProtoMessage](/src/target/scheduler.pb.go?s=2898:2932#L87)
``` go
func (*DaySchedule) ProtoMessage()
```



### <a name="DaySchedule.Reset">func</a> (\*DaySchedule) [Reset](/src/target/scheduler.pb.go?s=2739:2768#L85)
``` go
func (m *DaySchedule) Reset()
```



### <a name="DaySchedule.Size">func</a> (\*DaySchedule) [Size](/src/target/scheduler.pb.go?s=20388:20424#L709)
``` go
func (m *DaySchedule) Size() (n int)
```



### <a name="DaySchedule.String">func</a> (\*DaySchedule) [String](/src/target/scheduler.pb.go?s=2811:2848#L86)
``` go
func (m *DaySchedule) String() string
```



### <a name="DaySchedule.Unmarshal">func</a> (\*DaySchedule) [Unmarshal](/src/target/scheduler.pb.go?s=23757:23807#L882)
``` go
func (m *DaySchedule) Unmarshal(dAtA []byte) error
```



## <a name="GeneralSettings">type</a> [GeneralSettings](/src/target/scheduler.pb.go?s=3430:3724#L105)
``` go
type GeneralSettings struct {
    DaySchedule DaySchedule     `protobuf:"bytes,1,opt,name=daySchedule" json:"daySchedule"`
    Weekends    map[string]bool `protobuf:"bytes,2,rep,name=weekends" json:"weekends" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"varint,2,opt,name=value,proto3"`
}
```
GeneralSettings — общие настройки рабочего времени: расписание рабочего дня и выходные










### <a name="GeneralSettings.Descriptor">func</a> (\*GeneralSettings) [Descriptor](/src/target/scheduler.pb.go?s=3953:4005#L113)
``` go
func (*GeneralSettings) Descriptor() ([]byte, []int)
```



### <a name="GeneralSettings.GetDaySchedule">func</a> (\*GeneralSettings) [GetDaySchedule](/src/target/scheduler.pb.go?s=4052:4106#L115)
``` go
func (m *GeneralSettings) GetDaySchedule() DaySchedule
```



### <a name="GeneralSettings.GetWeekends">func</a> (\*GeneralSettings) [GetWeekends](/src/target/scheduler.pb.go?s=4175:4230#L122)
``` go
func (m *GeneralSettings) GetWeekends() map[string]bool
```



### <a name="GeneralSettings.Marshal">func</a> (\*GeneralSettings) [Marshal](/src/target/scheduler.pb.go?s=16089:16149#L490)
``` go
func (m *GeneralSettings) Marshal() (dAtA []byte, err error)
```



### <a name="GeneralSettings.MarshalTo">func</a> (\*GeneralSettings) [MarshalTo](/src/target/scheduler.pb.go?s=16289:16350#L500)
``` go
func (m *GeneralSettings) MarshalTo(dAtA []byte) (int, error)
```



### <a name="GeneralSettings.ProtoMessage">func</a> (\*GeneralSettings) [ProtoMessage](/src/target/scheduler.pb.go?s=3897:3935#L112)
``` go
func (*GeneralSettings) ProtoMessage()
```



### <a name="GeneralSettings.Reset">func</a> (\*GeneralSettings) [Reset](/src/target/scheduler.pb.go?s=3726:3759#L110)
``` go
func (m *GeneralSettings) Reset()
```



### <a name="GeneralSettings.Size">func</a> (\*GeneralSettings) [Size](/src/target/scheduler.pb.go?s=20584:20624#L719)
``` go
func (m *GeneralSettings) Size() (n int)
```



### <a name="GeneralSettings.String">func</a> (\*GeneralSettings) [String](/src/target/scheduler.pb.go?s=3806:3847#L111)
``` go
func (m *GeneralSettings) String() string
```



### <a name="GeneralSettings.Unmarshal">func</a> (\*GeneralSettings) [Unmarshal](/src/target/scheduler.pb.go?s=26132:26186#L992)
``` go
func (m *GeneralSettings) Unmarshal(dAtA []byte) error
```



## <a name="GetWorkTimeRq">type</a> [GetWorkTimeRq](/src/target/scheduler.pb.go?s=5935:6162#L173)
``` go
type GetWorkTimeRq struct {
    StartedAt   google_protobuf2.Timestamp `protobuf:"bytes,1,opt,name=startedAt" json:"startedAt"`
    TimeForWork google_protobuf3.Duration  `protobuf:"bytes,2,opt,name=timeForWork" json:"timeForWork"`
}
```
GetWorkTimeRq — запрос расчёта времени окночания с учётом рабочего времени










### <a name="GetWorkTimeRq.Descriptor">func</a> (\*GetWorkTimeRq) [Descriptor](/src/target/scheduler.pb.go?s=6383:6433#L181)
``` go
func (*GetWorkTimeRq) Descriptor() ([]byte, []int)
```



### <a name="GetWorkTimeRq.GetStartedAt">func</a> (\*GetWorkTimeRq) [GetStartedAt](/src/target/scheduler.pb.go?s=6480:6545#L183)
``` go
func (m *GetWorkTimeRq) GetStartedAt() google_protobuf2.Timestamp
```



### <a name="GetWorkTimeRq.GetTimeForWork">func</a> (\*GetWorkTimeRq) [GetTimeForWork](/src/target/scheduler.pb.go?s=6627:6693#L190)
``` go
func (m *GetWorkTimeRq) GetTimeForWork() google_protobuf3.Duration
```



### <a name="GetWorkTimeRq.Marshal">func</a> (\*GetWorkTimeRq) [Marshal](/src/target/scheduler.pb.go?s=18258:18316#L608)
``` go
func (m *GetWorkTimeRq) Marshal() (dAtA []byte, err error)
```



### <a name="GetWorkTimeRq.MarshalTo">func</a> (\*GetWorkTimeRq) [MarshalTo](/src/target/scheduler.pb.go?s=18456:18515#L618)
``` go
func (m *GetWorkTimeRq) MarshalTo(dAtA []byte) (int, error)
```



### <a name="GetWorkTimeRq.ProtoMessage">func</a> (\*GetWorkTimeRq) [ProtoMessage](/src/target/scheduler.pb.go?s=6329:6365#L180)
``` go
func (*GetWorkTimeRq) ProtoMessage()
```



### <a name="GetWorkTimeRq.Reset">func</a> (\*GetWorkTimeRq) [Reset](/src/target/scheduler.pb.go?s=6164:6195#L178)
``` go
func (m *GetWorkTimeRq) Reset()
```



### <a name="GetWorkTimeRq.Size">func</a> (\*GetWorkTimeRq) [Size](/src/target/scheduler.pb.go?s=21357:21395#L761)
``` go
func (m *GetWorkTimeRq) Size() (n int)
```



### <a name="GetWorkTimeRq.String">func</a> (\*GetWorkTimeRq) [String](/src/target/scheduler.pb.go?s=6240:6279#L179)
``` go
func (m *GetWorkTimeRq) String() string
```



### <a name="GetWorkTimeRq.Unmarshal">func</a> (\*GetWorkTimeRq) [Unmarshal](/src/target/scheduler.pb.go?s=34706:34758#L1381)
``` go
func (m *GetWorkTimeRq) Unmarshal(dAtA []byte) error
```



## <a name="GetWorkTimeRs">type</a> [GetWorkTimeRs](/src/target/scheduler.pb.go?s=6827:6965#L198)
``` go
type GetWorkTimeRs struct {
    WillDoneAt *google_protobuf2.Timestamp `protobuf:"bytes,1,opt,name=willDoneAt" json:"willDoneAt,omitempty"`
}
```
GetWorkTimeRs — время окончания










### <a name="GetWorkTimeRs.Descriptor">func</a> (\*GetWorkTimeRs) [Descriptor](/src/target/scheduler.pb.go?s=7186:7236#L205)
``` go
func (*GetWorkTimeRs) Descriptor() ([]byte, []int)
```



### <a name="GetWorkTimeRs.GetWillDoneAt">func</a> (\*GetWorkTimeRs) [GetWillDoneAt](/src/target/scheduler.pb.go?s=7283:7350#L207)
``` go
func (m *GetWorkTimeRs) GetWillDoneAt() *google_protobuf2.Timestamp
```



### <a name="GetWorkTimeRs.Marshal">func</a> (\*GetWorkTimeRs) [Marshal](/src/target/scheduler.pb.go?s=18923:18981#L642)
``` go
func (m *GetWorkTimeRs) Marshal() (dAtA []byte, err error)
```



### <a name="GetWorkTimeRs.MarshalTo">func</a> (\*GetWorkTimeRs) [MarshalTo](/src/target/scheduler.pb.go?s=19121:19180#L652)
``` go
func (m *GetWorkTimeRs) MarshalTo(dAtA []byte) (int, error)
```



### <a name="GetWorkTimeRs.ProtoMessage">func</a> (\*GetWorkTimeRs) [ProtoMessage](/src/target/scheduler.pb.go?s=7132:7168#L204)
``` go
func (*GetWorkTimeRs) ProtoMessage()
```



### <a name="GetWorkTimeRs.Reset">func</a> (\*GetWorkTimeRs) [Reset](/src/target/scheduler.pb.go?s=6967:6998#L202)
``` go
func (m *GetWorkTimeRs) Reset()
```



### <a name="GetWorkTimeRs.Size">func</a> (\*GetWorkTimeRs) [Size](/src/target/scheduler.pb.go?s=21555:21593#L771)
``` go
func (m *GetWorkTimeRs) Size() (n int)
```



### <a name="GetWorkTimeRs.String">func</a> (\*GetWorkTimeRs) [String](/src/target/scheduler.pb.go?s=7043:7082#L203)
``` go
func (m *GetWorkTimeRs) String() string
```



### <a name="GetWorkTimeRs.Unmarshal">func</a> (\*GetWorkTimeRs) [Unmarshal](/src/target/scheduler.pb.go?s=37087:37139#L1491)
``` go
func (m *GetWorkTimeRs) Unmarshal(dAtA []byte) error
```



## <a name="SchedulerClient">type</a> [SchedulerClient](/src/target/scheduler.pb.go?s=8187:8769#L234)
``` go
type SchedulerClient interface {
    GetGeneralSettings(ctx context.Context, in *google_protobuf1.Empty, opts ...grpc.CallOption) (*GeneralSettings, error)
    SetGeneralSettings(ctx context.Context, in *GeneralSettings, opts ...grpc.CallOption) (*GeneralSettings, error)
    GetSpecialDays(ctx context.Context, in *google_protobuf1.Empty, opts ...grpc.CallOption) (*SpecialDays, error)
    SetSpecialDays(ctx context.Context, in *SpecialDays, opts ...grpc.CallOption) (*SpecialDays, error)
    GetWorkTime(ctx context.Context, in *GetWorkTimeRq, opts ...grpc.CallOption) (*GetWorkTimeRs, error)
}
```






### <a name="NewSchedulerClient">func</a> [NewSchedulerClient](/src/target/scheduler.pb.go?s=8825:8885#L246)
``` go
func NewSchedulerClient(cc *grpc.ClientConn) SchedulerClient
```




## <a name="SchedulerServer">type</a> [SchedulerServer](/src/target/scheduler.pb.go?s=10475:10897#L297)
``` go
type SchedulerServer interface {
    GetGeneralSettings(context.Context, *google_protobuf1.Empty) (*GeneralSettings, error)
    SetGeneralSettings(context.Context, *GeneralSettings) (*GeneralSettings, error)
    GetSpecialDays(context.Context, *google_protobuf1.Empty) (*SpecialDays, error)
    SetSpecialDays(context.Context, *SpecialDays) (*SpecialDays, error)
    GetWorkTime(context.Context, *GetWorkTimeRq) (*GetWorkTimeRs, error)
}
```









## <a name="SpecialDay">type</a> [SpecialDay](/src/target/scheduler.pb.go?s=4423:4718#L130)
``` go
type SpecialDay struct {
    Date        int64 `protobuf:"varint,1,opt,name=date,proto3" json:"date,omitempty"`
    Holiday     bool  `protobuf:"varint,2,opt,name=holiday,proto3" json:"holiday,omitempty"`
    DayInterval `protobuf:"bytes,3,opt,name=workingTime,embedded=workingTime" json:"workingTime"`
}
```
SpecialDay — исключительный день: дата, признак выходного дня и рабочее время










### <a name="SpecialDay.Descriptor">func</a> (\*SpecialDay) [Descriptor](/src/target/scheduler.pb.go?s=4927:4974#L139)
``` go
func (*SpecialDay) Descriptor() ([]byte, []int)
```



### <a name="SpecialDay.GetDate">func</a> (\*SpecialDay) [GetDate](/src/target/scheduler.pb.go?s=5021:5057#L141)
``` go
func (m *SpecialDay) GetDate() int64
```



### <a name="SpecialDay.GetHoliday">func</a> (\*SpecialDay) [GetHoliday](/src/target/scheduler.pb.go?s=5107:5145#L148)
``` go
func (m *SpecialDay) GetHoliday() bool
```



### <a name="SpecialDay.Marshal">func</a> (\*SpecialDay) [Marshal](/src/target/scheduler.pb.go?s=17011:17066#L537)
``` go
func (m *SpecialDay) Marshal() (dAtA []byte, err error)
```



### <a name="SpecialDay.MarshalTo">func</a> (\*SpecialDay) [MarshalTo](/src/target/scheduler.pb.go?s=17206:17262#L547)
``` go
func (m *SpecialDay) MarshalTo(dAtA []byte) (int, error)
```



### <a name="SpecialDay.ProtoMessage">func</a> (\*SpecialDay) [ProtoMessage](/src/target/scheduler.pb.go?s=4876:4909#L138)
``` go
func (*SpecialDay) ProtoMessage()
```



### <a name="SpecialDay.Reset">func</a> (\*SpecialDay) [Reset](/src/target/scheduler.pb.go?s=4720:4748#L136)
``` go
func (m *SpecialDay) Reset()
```



### <a name="SpecialDay.Size">func</a> (\*SpecialDay) [Size](/src/target/scheduler.pb.go?s=20937:20972#L735)
``` go
func (m *SpecialDay) Size() (n int)
```



### <a name="SpecialDay.String">func</a> (\*SpecialDay) [String](/src/target/scheduler.pb.go?s=4790:4826#L137)
``` go
func (m *SpecialDay) String() string
```



### <a name="SpecialDay.Unmarshal">func</a> (\*SpecialDay) [Unmarshal](/src/target/scheduler.pb.go?s=30399:30448#L1181)
``` go
func (m *SpecialDay) Unmarshal(dAtA []byte) error
```



## <a name="SpecialDays">type</a> [SpecialDays](/src/target/scheduler.pb.go?s=5272:5386#L156)
``` go
type SpecialDays struct {
    SpecialDays []SpecialDay `protobuf:"bytes,1,rep,name=specialDays" json:"specialDays"`
}
```
SpecialDays — список исключительных дней










### <a name="SpecialDays.Descriptor">func</a> (\*SpecialDays) [Descriptor](/src/target/scheduler.pb.go?s=5599:5647#L163)
``` go
func (*SpecialDays) Descriptor() ([]byte, []int)
```



### <a name="SpecialDays.GetSpecialDays">func</a> (\*SpecialDays) [GetSpecialDays](/src/target/scheduler.pb.go?s=5694:5745#L165)
``` go
func (m *SpecialDays) GetSpecialDays() []SpecialDay
```



### <a name="SpecialDays.Marshal">func</a> (\*SpecialDays) [Marshal](/src/target/scheduler.pb.go?s=17703:17759#L578)
``` go
func (m *SpecialDays) Marshal() (dAtA []byte, err error)
```



### <a name="SpecialDays.MarshalTo">func</a> (\*SpecialDays) [MarshalTo](/src/target/scheduler.pb.go?s=17899:17956#L588)
``` go
func (m *SpecialDays) MarshalTo(dAtA []byte) (int, error)
```



### <a name="SpecialDays.ProtoMessage">func</a> (\*SpecialDays) [ProtoMessage](/src/target/scheduler.pb.go?s=5547:5581#L162)
``` go
func (*SpecialDays) ProtoMessage()
```



### <a name="SpecialDays.Reset">func</a> (\*SpecialDays) [Reset](/src/target/scheduler.pb.go?s=5388:5417#L160)
``` go
func (m *SpecialDays) Reset()
```



### <a name="SpecialDays.Size">func</a> (\*SpecialDays) [Size](/src/target/scheduler.pb.go?s=21159:21195#L749)
``` go
func (m *SpecialDays) Size() (n int)
```



### <a name="SpecialDays.String">func</a> (\*SpecialDays) [String](/src/target/scheduler.pb.go?s=5460:5497#L161)
``` go
func (m *SpecialDays) String() string
```



### <a name="SpecialDays.Unmarshal">func</a> (\*SpecialDays) [Unmarshal](/src/target/scheduler.pb.go?s=32927:32977#L1300)
``` go
func (m *SpecialDays) Unmarshal(dAtA []byte) error
```







- - -
Generated by [godoc2md](http://godoc.org/github.com/davecheney/godoc2md)
