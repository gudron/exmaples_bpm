

# mockpb
`import "./internal/service/grpc/pb/mock"`

* [Overview](#pkg-overview)
* [Index](#pkg-index)

## <a name="pkg-overview">Overview</a>
Package mockpb is a generated GoMock package.




## <a name="pkg-index">Index</a>
* [type MockSchedulerClient](#MockSchedulerClient)
  * [func NewMockSchedulerClient(ctrl *gomock.Controller) *MockSchedulerClient](#NewMockSchedulerClient)
  * [func (m *MockSchedulerClient) EXPECT() *MockSchedulerClientMockRecorder](#MockSchedulerClient.EXPECT)
  * [func (m *MockSchedulerClient) GetGeneralSettings(ctx context.Context, in *types.Empty, opts ...grpc.CallOption) (*GeneralSettings, error)](#MockSchedulerClient.GetGeneralSettings)
  * [func (m *MockSchedulerClient) GetSpecialDays(ctx context.Context, in *types.Empty, opts ...grpc.CallOption) (*SpecialDays, error)](#MockSchedulerClient.GetSpecialDays)
  * [func (m *MockSchedulerClient) GetWorkTime(ctx context.Context, in *GetWorkTimeRq, opts ...grpc.CallOption) (*GetWorkTimeRs, error)](#MockSchedulerClient.GetWorkTime)
  * [func (m *MockSchedulerClient) SetGeneralSettings(ctx context.Context, in *GeneralSettings, opts ...grpc.CallOption) (*GeneralSettings, error)](#MockSchedulerClient.SetGeneralSettings)
  * [func (m *MockSchedulerClient) SetSpecialDays(ctx context.Context, in *SpecialDays, opts ...grpc.CallOption) (*SpecialDays, error)](#MockSchedulerClient.SetSpecialDays)
* [type MockSchedulerClientMockRecorder](#MockSchedulerClientMockRecorder)
  * [func (mr *MockSchedulerClientMockRecorder) GetGeneralSettings(ctx, in interface{}, opts ...interface{}) *gomock.Call](#MockSchedulerClientMockRecorder.GetGeneralSettings)
  * [func (mr *MockSchedulerClientMockRecorder) GetSpecialDays(ctx, in interface{}, opts ...interface{}) *gomock.Call](#MockSchedulerClientMockRecorder.GetSpecialDays)
  * [func (mr *MockSchedulerClientMockRecorder) GetWorkTime(ctx, in interface{}, opts ...interface{}) *gomock.Call](#MockSchedulerClientMockRecorder.GetWorkTime)
  * [func (mr *MockSchedulerClientMockRecorder) SetGeneralSettings(ctx, in interface{}, opts ...interface{}) *gomock.Call](#MockSchedulerClientMockRecorder.SetGeneralSettings)
  * [func (mr *MockSchedulerClientMockRecorder) SetSpecialDays(ctx, in interface{}, opts ...interface{}) *gomock.Call](#MockSchedulerClientMockRecorder.SetSpecialDays)
* [type MockSchedulerServer](#MockSchedulerServer)
  * [func NewMockSchedulerServer(ctrl *gomock.Controller) *MockSchedulerServer](#NewMockSchedulerServer)
  * [func (m *MockSchedulerServer) EXPECT() *MockSchedulerServerMockRecorder](#MockSchedulerServer.EXPECT)
  * [func (m *MockSchedulerServer) GetGeneralSettings(arg0 context.Context, arg1 *types.Empty) (*GeneralSettings, error)](#MockSchedulerServer.GetGeneralSettings)
  * [func (m *MockSchedulerServer) GetSpecialDays(arg0 context.Context, arg1 *types.Empty) (*SpecialDays, error)](#MockSchedulerServer.GetSpecialDays)
  * [func (m *MockSchedulerServer) GetWorkTime(arg0 context.Context, arg1 *GetWorkTimeRq) (*GetWorkTimeRs, error)](#MockSchedulerServer.GetWorkTime)
  * [func (m *MockSchedulerServer) SetGeneralSettings(arg0 context.Context, arg1 *GeneralSettings) (*GeneralSettings, error)](#MockSchedulerServer.SetGeneralSettings)
  * [func (m *MockSchedulerServer) SetSpecialDays(arg0 context.Context, arg1 *SpecialDays) (*SpecialDays, error)](#MockSchedulerServer.SetSpecialDays)
* [type MockSchedulerServerMockRecorder](#MockSchedulerServerMockRecorder)
  * [func (mr *MockSchedulerServerMockRecorder) GetGeneralSettings(arg0, arg1 interface{}) *gomock.Call](#MockSchedulerServerMockRecorder.GetGeneralSettings)
  * [func (mr *MockSchedulerServerMockRecorder) GetSpecialDays(arg0, arg1 interface{}) *gomock.Call](#MockSchedulerServerMockRecorder.GetSpecialDays)
  * [func (mr *MockSchedulerServerMockRecorder) GetWorkTime(arg0, arg1 interface{}) *gomock.Call](#MockSchedulerServerMockRecorder.GetWorkTime)
  * [func (mr *MockSchedulerServerMockRecorder) SetGeneralSettings(arg0, arg1 interface{}) *gomock.Call](#MockSchedulerServerMockRecorder.SetGeneralSettings)
  * [func (mr *MockSchedulerServerMockRecorder) SetSpecialDays(arg0, arg1 interface{}) *gomock.Call](#MockSchedulerServerMockRecorder.SetSpecialDays)


#### <a name="pkg-files">Package files</a>
[scheduler_mock.go](/src/target/scheduler_mock.go) 






## <a name="MockSchedulerClient">type</a> [MockSchedulerClient](/src/target/scheduler_mock.go?s=440:547#L17)
``` go
type MockSchedulerClient struct {
    // contains filtered or unexported fields
}
```
MockSchedulerClient is a mock of SchedulerClient interface







### <a name="NewMockSchedulerClient">func</a> [NewMockSchedulerClient](/src/target/scheduler_mock.go?s=759:832#L28)
``` go
func NewMockSchedulerClient(ctrl *gomock.Controller) *MockSchedulerClient
```
NewMockSchedulerClient creates a new mock instance





### <a name="MockSchedulerClient.EXPECT">func</a> (\*MockSchedulerClient) [EXPECT](/src/target/scheduler_mock.go?s=1025:1096#L35)
``` go
func (m *MockSchedulerClient) EXPECT() *MockSchedulerClientMockRecorder
```
EXPECT returns an object that allows the caller to indicate expected use




### <a name="MockSchedulerClient.GetGeneralSettings">func</a> (\*MockSchedulerClient) [GetGeneralSettings](/src/target/scheduler_mock.go?s=1161:1298#L40)
``` go
func (m *MockSchedulerClient) GetGeneralSettings(ctx context.Context, in *types.Empty, opts ...grpc.CallOption) (*GeneralSettings, error)
```
GetGeneralSettings mocks base method




### <a name="MockSchedulerClient.GetSpecialDays">func</a> (\*MockSchedulerClient) [GetSpecialDays](/src/target/scheduler_mock.go?s=2795:2924#L76)
``` go
func (m *MockSchedulerClient) GetSpecialDays(ctx context.Context, in *types.Empty, opts ...grpc.CallOption) (*SpecialDays, error)
```
GetSpecialDays mocks base method




### <a name="MockSchedulerClient.GetWorkTime">func</a> (\*MockSchedulerClient) [GetWorkTime](/src/target/scheduler_mock.go?s=4346:4476#L112)
``` go
func (m *MockSchedulerClient) GetWorkTime(ctx context.Context, in *GetWorkTimeRq, opts ...grpc.CallOption) (*GetWorkTimeRs, error)
```
GetWorkTime mocks base method




### <a name="MockSchedulerClient.SetGeneralSettings">func</a> (\*MockSchedulerClient) [SetGeneralSettings](/src/target/scheduler_mock.go?s=1978:2119#L58)
``` go
func (m *MockSchedulerClient) SetGeneralSettings(ctx context.Context, in *GeneralSettings, opts ...grpc.CallOption) (*GeneralSettings, error)
```
SetGeneralSettings mocks base method




### <a name="MockSchedulerClient.SetSpecialDays">func</a> (\*MockSchedulerClient) [SetSpecialDays](/src/target/scheduler_mock.go?s=3572:3701#L94)
``` go
func (m *MockSchedulerClient) SetSpecialDays(ctx context.Context, in *SpecialDays, opts ...grpc.CallOption) (*SpecialDays, error)
```
SetSpecialDays mocks base method




## <a name="MockSchedulerClientMockRecorder">type</a> [MockSchedulerClientMockRecorder](/src/target/scheduler_mock.go?s=629:703#L23)
``` go
type MockSchedulerClientMockRecorder struct {
    // contains filtered or unexported fields
}
```
MockSchedulerClientMockRecorder is the mock recorder for MockSchedulerClient










### <a name="MockSchedulerClientMockRecorder.GetGeneralSettings">func</a> (\*MockSchedulerClientMockRecorder) [GetGeneralSettings](/src/target/scheduler_mock.go?s=1611:1727#L52)
``` go
func (mr *MockSchedulerClientMockRecorder) GetGeneralSettings(ctx, in interface{}, opts ...interface{}) *gomock.Call
```
GetGeneralSettings indicates an expected call of GetGeneralSettings




### <a name="MockSchedulerClientMockRecorder.GetSpecialDays">func</a> (\*MockSchedulerClientMockRecorder) [GetSpecialDays](/src/target/scheduler_mock.go?s=3221:3333#L88)
``` go
func (mr *MockSchedulerClientMockRecorder) GetSpecialDays(ctx, in interface{}, opts ...interface{}) *gomock.Call
```
GetSpecialDays indicates an expected call of GetSpecialDays




### <a name="MockSchedulerClientMockRecorder.GetWorkTime">func</a> (\*MockSchedulerClientMockRecorder) [GetWorkTime](/src/target/scheduler_mock.go?s=4766:4875#L124)
``` go
func (mr *MockSchedulerClientMockRecorder) GetWorkTime(ctx, in interface{}, opts ...interface{}) *gomock.Call
```
GetWorkTime indicates an expected call of GetWorkTime




### <a name="MockSchedulerClientMockRecorder.SetGeneralSettings">func</a> (\*MockSchedulerClientMockRecorder) [SetGeneralSettings](/src/target/scheduler_mock.go?s=2432:2548#L70)
``` go
func (mr *MockSchedulerClientMockRecorder) SetGeneralSettings(ctx, in interface{}, opts ...interface{}) *gomock.Call
```
SetGeneralSettings indicates an expected call of SetGeneralSettings




### <a name="MockSchedulerClientMockRecorder.SetSpecialDays">func</a> (\*MockSchedulerClientMockRecorder) [SetSpecialDays](/src/target/scheduler_mock.go?s=3998:4110#L106)
``` go
func (mr *MockSchedulerClientMockRecorder) SetSpecialDays(ctx, in interface{}, opts ...interface{}) *gomock.Call
```
SetSpecialDays indicates an expected call of SetSpecialDays




## <a name="MockSchedulerServer">type</a> [MockSchedulerServer](/src/target/scheduler_mock.go?s=5134:5241#L130)
``` go
type MockSchedulerServer struct {
    // contains filtered or unexported fields
}
```
MockSchedulerServer is a mock of SchedulerServer interface







### <a name="NewMockSchedulerServer">func</a> [NewMockSchedulerServer](/src/target/scheduler_mock.go?s=5453:5526#L141)
``` go
func NewMockSchedulerServer(ctrl *gomock.Controller) *MockSchedulerServer
```
NewMockSchedulerServer creates a new mock instance





### <a name="MockSchedulerServer.EXPECT">func</a> (\*MockSchedulerServer) [EXPECT](/src/target/scheduler_mock.go?s=5719:5790#L148)
``` go
func (m *MockSchedulerServer) EXPECT() *MockSchedulerServerMockRecorder
```
EXPECT returns an object that allows the caller to indicate expected use




### <a name="MockSchedulerServer.GetGeneralSettings">func</a> (\*MockSchedulerServer) [GetGeneralSettings](/src/target/scheduler_mock.go?s=5855:5970#L153)
``` go
func (m *MockSchedulerServer) GetGeneralSettings(arg0 context.Context, arg1 *types.Empty) (*GeneralSettings, error)
```
GetGeneralSettings mocks base method




### <a name="MockSchedulerServer.GetSpecialDays">func</a> (\*MockSchedulerServer) [GetSpecialDays](/src/target/scheduler_mock.go?s=7115:7222#L179)
``` go
func (m *MockSchedulerServer) GetSpecialDays(arg0 context.Context, arg1 *types.Empty) (*SpecialDays, error)
```
GetSpecialDays mocks base method




### <a name="MockSchedulerServer.GetWorkTime">func</a> (\*MockSchedulerServer) [GetWorkTime](/src/target/scheduler_mock.go?s=8292:8400#L205)
``` go
func (m *MockSchedulerServer) GetWorkTime(arg0 context.Context, arg1 *GetWorkTimeRq) (*GetWorkTimeRs, error)
```
GetWorkTime mocks base method




### <a name="MockSchedulerServer.SetGeneralSettings">func</a> (\*MockSchedulerServer) [SetGeneralSettings](/src/target/scheduler_mock.go?s=6485:6604#L166)
``` go
func (m *MockSchedulerServer) SetGeneralSettings(arg0 context.Context, arg1 *GeneralSettings) (*GeneralSettings, error)
```
SetGeneralSettings mocks base method




### <a name="MockSchedulerServer.SetSpecialDays">func</a> (\*MockSchedulerServer) [SetSpecialDays](/src/target/scheduler_mock.go?s=7705:7812#L192)
``` go
func (m *MockSchedulerServer) SetSpecialDays(arg0 context.Context, arg1 *SpecialDays) (*SpecialDays, error)
```
SetSpecialDays mocks base method




## <a name="MockSchedulerServerMockRecorder">type</a> [MockSchedulerServerMockRecorder](/src/target/scheduler_mock.go?s=5323:5397#L136)
``` go
type MockSchedulerServerMockRecorder struct {
    // contains filtered or unexported fields
}
```
MockSchedulerServerMockRecorder is the mock recorder for MockSchedulerServer










### <a name="MockSchedulerServerMockRecorder.GetGeneralSettings">func</a> (\*MockSchedulerServerMockRecorder) [GetGeneralSettings](/src/target/scheduler_mock.go?s=6188:6286#L161)
``` go
func (mr *MockSchedulerServerMockRecorder) GetGeneralSettings(arg0, arg1 interface{}) *gomock.Call
```
GetGeneralSettings indicates an expected call of GetGeneralSettings




### <a name="MockSchedulerServerMockRecorder.GetSpecialDays">func</a> (\*MockSchedulerServerMockRecorder) [GetSpecialDays](/src/target/scheduler_mock.go?s=7424:7518#L187)
``` go
func (mr *MockSchedulerServerMockRecorder) GetSpecialDays(arg0, arg1 interface{}) *gomock.Call
```
GetSpecialDays indicates an expected call of GetSpecialDays




### <a name="MockSchedulerServerMockRecorder.GetWorkTime">func</a> (\*MockSchedulerServerMockRecorder) [GetWorkTime](/src/target/scheduler_mock.go?s=8595:8686#L213)
``` go
func (mr *MockSchedulerServerMockRecorder) GetWorkTime(arg0, arg1 interface{}) *gomock.Call
```
GetWorkTime indicates an expected call of GetWorkTime




### <a name="MockSchedulerServerMockRecorder.SetGeneralSettings">func</a> (\*MockSchedulerServerMockRecorder) [SetGeneralSettings](/src/target/scheduler_mock.go?s=6822:6920#L174)
``` go
func (mr *MockSchedulerServerMockRecorder) SetGeneralSettings(arg0, arg1 interface{}) *gomock.Call
```
SetGeneralSettings indicates an expected call of SetGeneralSettings




### <a name="MockSchedulerServerMockRecorder.SetSpecialDays">func</a> (\*MockSchedulerServerMockRecorder) [SetSpecialDays](/src/target/scheduler_mock.go?s=8014:8108#L200)
``` go
func (mr *MockSchedulerServerMockRecorder) SetSpecialDays(arg0, arg1 interface{}) *gomock.Call
```
SetSpecialDays indicates an expected call of SetSpecialDays








- - -
Generated by [godoc2md](http://godoc.org/github.com/davecheney/godoc2md)
