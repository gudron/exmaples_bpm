package scheduler

import (
	"github.com/go-chi/chi"

	"git.elewise.com/elma365/main/src/common/config"
)

// Router is an scheduler endpoint
func NewRouter(cfg *config.Config) chi.Router {
	r := chi.NewRouter()
	sc := New(cfg)

	r.Route("/general", func(r chi.Router) {
		r.Get("/", sc.getGeneral)
		r.Put("/", sc.setGeneral)
	})

	r.Route("/specialDays", func(r chi.Router) {
		r.Get("/", sc.getSpecialDays)
		r.Put("/", sc.setSpecialDays)
	})

	r.Route("/getWorkTime", func(r chi.Router) {
		r.Put("/", sc.getWorkTime)
	})

	return r
}
