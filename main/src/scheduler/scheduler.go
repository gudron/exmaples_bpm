package scheduler

import (
	"encoding/json"
	"net/http"

	"git.elewise.com/elma365/main/src/common/config"
	"git.elewise.com/elma365/main/src/common/errs"
	"git.elewise.com/elma365/main/src/common/interceptor"
	"git.elewise.com/elma365/main/src/common/tracing"
	"git.elewise.com/elma365/main/src/scheduler/pb"
	"github.com/gogo/protobuf/types"

	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/tracing/opentracing"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Scheduler is a scheduler client wrapper
type Scheduler struct {
	svc pb.SchedulerClient
}

// New scheduler DI container
func New(cfg *config.Config) *Scheduler {
	conn, err := grpc.Dial(
		"scheduler",
		grpc.WithInsecure(),
		grpc.WithDialer(cfg.GRPCDialer),
		grpc.WithUnaryInterceptor(grpc_middleware.ChainUnaryClient(
			grpc_opentracing.UnaryClientInterceptor(),
			interceptor.MDClientInterceptor(),
		)),
	)
	if err != nil {

		zap.L().Fatal(err.Error())
	}

	return &Scheduler{pb.NewSchedulerClient(conn)}
}

// getGeneral scheduler settings
func (sc *Scheduler) getGeneral(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	l := tracing.LoggerFromContext(ctx)

	res, err := sc.svc.GetGeneralSettings(ctx, &types.Empty{})

	if err != nil {
		st, ok := status.FromError(err)

		if !ok {
			errs.Internal(l, w, err)
		} else {
			if st.Code() == codes.NotFound {
				http.Error(w, errors.Wrap(err, "scheduler:empty_settings").Error(), http.StatusNotFound)
			}
		}

		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	je := json.NewEncoder(w)
	if err = je.Encode(res); err != nil {
		l.Error(err.Error())
	}
}

// setGeneral scheduler settings
func (sc *Scheduler) setGeneral(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	l := tracing.LoggerFromContext(ctx)

	jd := json.NewDecoder(r.Body)
	d := &pb.GeneralSettings{}

	if err := errors.Wrap(jd.Decode(d), "unmarshal"); err != nil {
		l.Debug(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	res, err := sc.svc.SetGeneralSettings(ctx, d)

	if err != nil {
		l.Debug(err.Error(), errs.ZapStack(err))
		code := codes.Unknown
		stat, ok := status.FromError(err)
		if ok {
			code = stat.Code()
		}
		switch code {
		case codes.Aborted:
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(stat.Message()))
		default:
			errs.Internal(l, w, err)
		}

		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	je := json.NewEncoder(w)
	if err = je.Encode(res); err != nil {
		l.Error(err.Error())
	}
}

// getGeneral scheduler settings
func (sc *Scheduler) getSpecialDays(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	l := tracing.LoggerFromContext(ctx)

	res, err := sc.svc.GetSpecialDays(ctx, &types.Empty{})

	if err != nil {
		st, ok := status.FromError(err)

		if !ok {
			errs.Internal(l, w, err)
		} else {
			if st.Code() == codes.NotFound {
				http.Error(w, errors.Wrap(err, "scheduler:empty_specialdays_settings").Error(), http.StatusNotFound)
			}
		}

		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	je := json.NewEncoder(w)
	if err = je.Encode(res); err != nil {
		l.Error(err.Error())
	}
}

// setGeneral scheduler settings
func (sc *Scheduler) setSpecialDays(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	l := tracing.LoggerFromContext(ctx)

	jd := json.NewDecoder(r.Body)
	d := &pb.SpecialDays{}

	if err := errors.Wrap(jd.Decode(d), "unmarshal"); err != nil {
		l.Debug(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	res, err := sc.svc.SetSpecialDays(ctx, d)

	if err != nil {

		l.Debug(err.Error(), errs.ZapStack(err))
		code := codes.Unknown
		stat, ok := status.FromError(err)
		if ok {
			code = stat.Code()
		}
		switch code {
		case codes.Aborted:
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(stat.Message()))
		default:
			errs.Internal(l, w, err)
		}

		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	je := json.NewEncoder(w)
	if err = je.Encode(res); err != nil {
		l.Error(err.Error())
	}
}

// setGeneral scheduler settings
func (sc *Scheduler) getWorkTime(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	l := tracing.LoggerFromContext(ctx)

	jd := json.NewDecoder(r.Body)
	d := &pb.GetWorkTimeRq{}

	if err := errors.Wrap(jd.Decode(d), "unmarshal"); err != nil {
		l.Debug(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	res, err := sc.svc.GetWorkTime(ctx, d)

	if err != nil {
		l.Debug(err.Error(), errs.ZapStack(err))
		code := codes.Unknown
		stat, ok := status.FromError(err)
		if ok {
			code = stat.Code()
		}
		switch code {
		case codes.Aborted:
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(stat.Message()))
		default:
			errs.Internal(l, w, err)
		}

		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	je := json.NewEncoder(w)
	if err = je.Encode(res); err != nil {
		l.Error(err.Error())
	}
}
