CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- Schema

CREATE SCHEMA IF NOT EXISTS {{ . }};

-- Events

CREATE TABLE IF NOT EXISTS {{ . }}.event (
    id      SERIAL    PRIMARY KEY,
    ts      TIMESTAMP,
    initiator UUID,
    hdr     JSONB,
    items   JSONB
);

-- User

DROP TABLE IF EXISTS {{ . }}.users CASCADE;
CREATE TABLE {{ . }}.users (
    id         UUID         PRIMARY KEY,
    email      VARCHAR(255) UNIQUE,
    passhash   CHAR(60),
    firstname  VARCHAR(255),
    lastname   VARCHAR(255),
    owner      BOOLEAN      DEFAULT FALSE,
    created_at TIMESTAMP    DEFAULT NOW(),
    updated_at TIMESTAMP    DEFAULT NOW(),
    deleted_at TIMESTAMP    DEFAULT NULL
);

-- Orgstruct

DROP TABLE IF EXISTS {{ . }}.orgstruct CASCADE;
CREATE TABLE {{ . }}.orgstruct (
    id         UUID         PRIMARY KEY,
    type       VARCHAR(255),
    name       VARCHAR(255),
    sort       INT,
    page       BOOLEAN,
    parent     UUID,
    created_at TIMESTAMP     DEFAULT NOW(),
    updated_at TIMESTAMP     DEFAULT NOW(),
    deleted_at TIMESTAMP     DEFAULT NULL
);

-- Groups

DROP TABLE IF EXISTS {{ . }}.groups CASCADE;
CREATE TABLE {{ . }}.groups (
    id         UUID      PRIMARY KEY,
    namespace  TEXT,
    code       TEXT,
    name       TEXT,
    privileges INT,
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP DEFAULT NOW(),
    deleted_at TIMESTAMP DEFAULT NULL,

    UNIQUE (namespace, code)
);

DROP TABLE IF EXISTS {{ . }}.group_link CASCADE;
CREATE TABLE {{ . }}.group_link (
    object_id   UUID        NOT NULL,
    object_type VARCHAR(10) NOT NULL,
    super_id    UUID        NOT NULL,
    super_type  VARCHAR(10) NOT NULL,

    UNIQUE (object_id, super_id)
);

-- BP templates

DROP TABLE IF EXISTS {{ . }}.bp_template_categories CASCADE;
CREATE TABLE {{ . }}.bp_template_categories (
    id         UUID         PRIMARY KEY,
    name       VARCHAR(255),
    parent     UUID,
    created_at TIMESTAMP    DEFAULT NOW(),
    updated_at TIMESTAMP    DEFAULT NOW(),
    deleted_at TIMESTAMP    DEFAULT NULL
);
CREATE UNIQUE INDEX bp_template_categories_parent_name ON {{ . }}.bp_template_categories (parent, name);


DROP TABLE IF EXISTS {{ . }}.bp_templates CASCADE;
DROP TABLE IF EXISTS {{ . }}.bp_template_versions CASCADE;
DROP TYPE IF EXISTS {{ . }}.bp_type;
CREATE TYPE {{ . }}.bp_type AS ENUM ('bpmn', 'simple');
CREATE TABLE {{ . }}.bp_templates (
    id         UUID            PRIMARY KEY,
    namespace  VARCHAR(255),
    code       UUID,
    type       {{ . }}.bp_type,
    name       VARCHAR(255),
    category   UUID,
    version    INT,
    draft      BOOLEAN,
    process    JSONB,
    context    JSONB,
    scripts    TEXT,
    manual_run BOOLEAN,
    api_run    BOOLEAN,
    created_at TIMESTAMP       DEFAULT NOW(),
    updated_at TIMESTAMP       DEFAULT NOW(),
    deleted_at TIMESTAMP       DEFAULT NULL
);
CREATE UNIQUE INDEX bp_templates_category_name ON {{ . }}.bp_templates (category, name);
CREATE UNIQUE INDEX bp_templates_namespace_code ON {{ . }}.bp_templates (namespace, code);

CREATE TABLE {{ . }}.bp_template_versions (
    id          UUID      PRIMARY KEY,
    template_id UUID,
    version     INT,
    process     JSONB,
    context     JSONB,
    scripts     TEXT,
    created_at  TIMESTAMP DEFAULT NOW()
);
CREATE UNIQUE INDEX bp_template_versions_version ON {{ . }}.bp_template_versions (template_id, version);

-- Disk


CREATE TABLE {{ . }}.disk_files (
    id            UUID         PRIMARY KEY,
    name          VARCHAR(255),
    original_name  VARCHAR(255),
    directory     UUID DEFAULT NULL,
    size          BIGINT,
    hash          UUID,
    thumbnails    JSONB,
    permissions   JSONB,
    created_at    TIMESTAMP    DEFAULT NOW(),
    updated_at    TIMESTAMP    DEFAULT NOW(),
    deleted_at    TIMESTAMP    DEFAULT NULL
);
CREATE UNIQUE INDEX disk_files_name_directory_uindex ON {{ . }}.disk_files (name, directory);

CREATE TABLE {{ . }}.disk_directories (
    id            UUID         PRIMARY KEY,
    name          VARCHAR(255),
    parent        UUID,
    permissions   JSONB,
    created_at    TIMESTAMP    DEFAULT NOW(),
    updated_at    TIMESTAMP    DEFAULT NOW(),
    deleted_at    TIMESTAMP    DEFAULT NULL
);
CREATE UNIQUE INDEX disk_directories_name_parent_uindex ON {{ . }}.disk_directories (name, parent);

-- Create role

DO
$body$
BEGIN
   IF NOT EXISTS (
      SELECT *
      FROM   pg_catalog.pg_user
      WHERE  usename = 'main') THEN

      CREATE ROLE main LOGIN PASSWORD 'test_main_password';
   END IF;
END
$body$;

-- Grant access (it must be last statement to grant priveleges on all created items)

GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA {{ . }} TO main;
GRANT UPDATE ON ALL SEQUENCES IN SCHEMA {{ . }} TO main;
GRANT USAGE ON SCHEMA {{ . }} TO main;
