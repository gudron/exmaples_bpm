package testhelpers

// Check error to be nil
func Check(err error) {
	if err != nil {
		panic(err)
	}
}
