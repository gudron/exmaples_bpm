package testhelpers

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"git.elewise.com/elma365/main/src/common/config"
)

var (
	cfg *config.Config
)

func init() {
	cfg = config.Default()
	if err := cfg.ReadEnv(); err != nil {
		log.Fatal(err)
	}
}

// OpenDB connection
func OpenDB(schemaName string) (*sql.DB, error) {

	var postgresAddr = "localhost:5432"

	if len(os.Getenv("CI")) > 0 {
		postgresAddr = "postgres:5432"
	}

	url := fmt.Sprintf("postgres://postgres:postgres@%s/%s?sslmode=disable", postgresAddr, cfg.DBName)
	if len(schemaName) > 0 {
		url = fmt.Sprintf("%s&search_path=%s,public", url, schemaName)
	}

	return sql.Open("postgres", url)
}

// CreateTemporarySchema with gived name
//
// folderDepth must be equal package depth related from root.
func CreateTemporarySchema(db *sql.DB, name string, folderDepth int) error {
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	padding := make([]string, folderDepth)
	for i := range padding {
		padding[i] = ".."
	}
	padPath := filepath.Join(padding...)

	tmpl, err := template.ParseFiles(filepath.Join(padPath, "test_data", "schema.sql"))
	if err != nil {
		return err
	}

	buf := &bytes.Buffer{}
	err = tmpl.ExecuteTemplate(buf, "schema.sql", name)
	if err != nil {
		return err
	}

	_, err = db.Exec(buf.String())
	if err != nil {
		return err
	}

	return tx.Commit()
}

// RemoveTemporarySchema with gived name
func RemoveTemporarySchema(db *sql.DB, name string) error {
	_, err := db.Exec(fmt.Sprintf("DROP SCHEMA %q CASCADE", name))

	return err
}

func getLastEventItems(t *testing.T, db *sql.DB, schemaName string) json.RawMessage {
	rows, err := db.Query(fmt.Sprintf("SELECT items FROM %q.event", schemaName))
	require.NoError(t, err)
	defer rows.Close()

	var res json.RawMessage
	count := 0
	for rows.Next() {
		require.Equal(t, 0, count, "event should be single")
		require.NoError(t, rows.Scan(&res))
	}

	return res
}

// QueryTable return whole table as slice of maps
func QueryTable(t *testing.T, db *sql.DB, schemaName, tableName string) []map[string]interface{} {
	rows, err := db.Query(fmt.Sprintf("SELECT * FROM %q.%s ORDER BY ctid", schemaName, tableName))
	require.NoError(t, err)
	defer rows.Close()

	columns, err := rows.Columns()
	require.NoError(t, err)
	count := len(columns)
	tableData := make([]map[string]interface{}, 0)
	values := make([]interface{}, count)
	valuePtrs := make([]interface{}, count)
	for rows.Next() {
		for i := 0; i < count; i++ {
			valuePtrs[i] = &values[i]
		}
		rows.Scan(valuePtrs...)
		entry := make(map[string]interface{})
		for i, col := range columns {
			var v interface{}

			val := values[i]
			switch b := val.(type) {
			default:
				v = val

			case []byte:
				if json.Valid(b) {
					v = json.RawMessage(b)
				} else {
					v = string(b)
				}

			case time.Time:
				v = time.Unix(0, 0).UTC()
			}
			entry[col] = v
		}
		tableData = append(tableData, entry)
	}

	return tableData
}
