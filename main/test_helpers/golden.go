package testhelpers

import (
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

var (
	goldenPath = filepath.Join("test_data", "golden")
)

// CreateOrCompareEvents expect that there is only one event and create/compare items with golden
func CreateOrCompareEvents(db *sql.DB, schemaName string, golden bool) func(*testing.T) {
	return func(t *testing.T) {
		events := QueryTable(t, db, schemaName, "event")
		CreateOrCompareGoldenJSON(t, golden, events, true)
	}
}

// CreateOrCompareGoldenJSON and gived object
func CreateOrCompareGoldenJSON(t *testing.T, golden bool, v interface{}, removeTime bool) {
	if golden || !goldenExists(t) {
		CreateGoldenJSON(t, v)
	} else {
		CompareGoldenJSON(t, v, removeTime)
	}
}

func goldenExists(t *testing.T) bool {
	path := filepath.Join(goldenPath, filename(t))
	_, err := os.Stat(path)

	return !os.IsNotExist(err)
}

// CreateGoldenJSON with name of test
func CreateGoldenJSON(t *testing.T, v interface{}) {
	data, err := json.MarshalIndent(v, "", "    ")
	require.NoError(t, err)

	require.NoError(t, os.MkdirAll(goldenPath, 0755))
	path := filepath.Join(goldenPath, filename(t))

	require.NoError(t, ioutil.WriteFile(path, data, 0644))
}

// CompareGoldenJSON with name of test with object
func CompareGoldenJSON(t *testing.T, v interface{}, removeTime bool) {
	data, err := json.Marshal(v)
	require.NoError(t, err)

	path := filepath.Join(goldenPath, filename(t))
	gdata, err := ioutil.ReadFile(path)
	require.NoError(t, err)

	require.JSONEq(t, RemoveTime(string(gdata)), RemoveTime(string(data)))
}

func filename(t *testing.T) string {
	return strings.Replace(t.Name(), "/", "_", -1) + ".json"
}
