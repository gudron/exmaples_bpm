package testhelpers

import (
	"math/rand"
	"regexp"
	"time"
)

const (
	letters = "abcdefghijklmnopqrstuvwxyz"
)

var (
	rnd = rand.New(rand.NewSource(time.Now().UnixNano()))

	timeRE = regexp.MustCompile(`\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d+)`)
)

// RandomString return pseudo random string w/ gived length from a-z letters
func RandomString(length int) string {
	res := make([]byte, length)
	for i := range res {
		res[i] = letters[rnd.Intn(len(letters))]
	}

	return string(res)
}

// RemoveTime from src to json compare
func RemoveTime(src string) string {
	return timeRE.ReplaceAllString(src, "1970-01-01T00:00:00")
}
