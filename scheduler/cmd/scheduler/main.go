package main

import (
	"fmt"
	"net"
	"os"
	"os/signal"
	"syscall"

	"git.elewise.com/elma365/scheduler/internal/adaptor/config"
	"git.elewise.com/elma365/scheduler/internal/adaptor/store"
	"git.elewise.com/elma365/scheduler/internal/common/appctx"
	"git.elewise.com/elma365/scheduler/internal/common/errs"
	"git.elewise.com/elma365/scheduler/internal/service"
	"git.elewise.com/elma365/scheduler/internal/service/pb"

	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	"github.com/grpc-ecosystem/go-grpc-middleware/tracing/opentracing"
	"github.com/grpc-ecosystem/go-grpc-prometheus"
	"google.golang.org/grpc"

	_ "github.com/lib/pq"
)

func main() {
	cfg := config.Default()

	if err := cfg.ReadEnv(); err != nil {
		fmt.Println(err.Error())

		os.Exit(1)
	}

	l := config.Logger(cfg)

	defer l.Sync()

	grpc_zap.ReplaceGrpcLogger(l)

	jCloser, err := config.Jaeger(cfg)
	if err != nil {
		l.Warn(err.Error(), errs.ZapStack(err))
	} else {
		defer jCloser.Close()
	}

	if err = store.Setup(cfg); err != nil {
		l.Fatal(err.Error(), errs.ZapStack(err))
	}

	srv := service.New()

	grpcServer := grpc.NewServer(
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpc_opentracing.UnaryServerInterceptor(),
			grpc_prometheus.UnaryServerInterceptor,
			appctx.UnaryMetadataInterceptor,
			appctx.UnaryErrorInterceptor,
			appctx.UnaryValidateInterceptor,
		)),
	)

	pb.RegisterSchedulerServer(grpcServer, srv)

	l.Info("starting http service")
	conn, err := net.Listen("tcp", cfg.Bind)
	if err != nil {
		panic(err)
	}
	go grpcServer.Serve(conn)

	c := make(chan os.Signal)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
	<-c
	l.Warn("stopping http service")
	grpcServer.GracefulStop()
	l.Warn("stopped")
}
