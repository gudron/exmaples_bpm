package config

import (
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/tomazk/envcfg"
)

// Config of application
type Config struct {
	// Debug is a flag to log debug level, default false
	Debug bool `envcfg:"ELMA365_DEBUG" json:"debug"`
	// Bind string, default ":8010"
	Bind string `envcfg:"ELMA365_BIND" json:"bind"`
	// DBName is a postgres database name, default "elma365"
	DBName string `envcfg:"ELMA365_DB_NAME" json:"dbName"`
	// VHName is a rabbitmq virtual host name, default "elma365"
	VHName string `envcfg:"ELMA365_VH_NAME" json:"vhName"`

	PostgresUser     string `envcfg:"ELMA365_POSTGRESQL_USER"`
	PostgresPassword string `envcfg:"ELMA365_POSTGRESQL_PASSWORD"`

	RabbitmqUser     string `envcfg:"ELMA365_RABBITMQ_USER"`
	RabbitmqPassword string `envcfg:"ELMA365_RABBITMQ_PASSWORD"`

	// DNSSuffix нужен для нормальной работы telepresene. Иначе у нее глюки с SRV records
	DNSSuffix string `envcfg:"KUBE_DNS_SUFFIX"`
}

// Default Config
func Default() *Config {
	c := &Config{
		Debug:  false,
		Bind:   ":8010",
		DBName: "elma365",
		VHName: "elma365",
	}

	return c
}

// ReadEnv variables to static configuration
func (c *Config) ReadEnv() error {
	return errors.WithStack(envcfg.Unmarshal(&c))
}

// DNSResolve service ip:port
func (c *Config) DNSResolve(service, proto, name string) (string, error) {
	var addr string
	envAddr := os.Getenv(strings.Replace(name, "-", "_", -1))
	if len(envAddr) > 0 {
		splittedAddr := strings.Split(envAddr, ":")
		if len(splittedAddr) == 2 {
			port, err := strconv.Atoi(splittedAddr[1])
			if err != nil {
				return addr, errors.WithStack(err)
			}
			addr = fmt.Sprintf("%s:%d", splittedAddr[0], port)
		} else {
			return addr, errors.WithStack(errors.Errorf(
				"invalid %q address forced with env: %q must be in format \"address:port\"",
				name, envAddr,
			))
		}
	} else {
		if len(c.DNSSuffix) > 0 {
			if c.DNSSuffix[0] == '.' {
				name = name + c.DNSSuffix
			} else {
				name = name + "." + c.DNSSuffix
			}
		}
		cname, addrs, err := net.LookupSRV(service, proto, name)
		if err != nil {
			return addr, errors.WithStack(err)
		}

		if len(addrs) == 0 {
			return addr, errors.Errorf("SRV Lookup for %q service not found", name)
		}
		addr = fmt.Sprintf("%s:%d", cname, addrs[0].Port)
	}
	return addr, nil
}

// GRPCDialer is a method to pass it in grpc.Dial options
func (c *Config) GRPCDialer(service string, timeout time.Duration) (net.Conn, error) {
	addr, err := c.DNSResolve("grpc", "tcp", service)
	if err != nil {

		return nil, err
	}
	cn, err := net.DialTimeout("tcp", addr, timeout)

	return cn, errors.WithStack(err)
}
