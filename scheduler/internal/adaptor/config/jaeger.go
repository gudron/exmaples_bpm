package config

import (
	"io"

	"github.com/pkg/errors"
	jConfig "github.com/uber/jaeger-client-go/config"
)

// Jaeger init global tracer
func Jaeger(cfg *Config) (io.Closer, error) {
	jaddr, err := cfg.DNSResolve("jaeger", "udp", "jaeger")
	if err != nil {
		return nil, errors.WithStack(err)
	}
	jCfg := jConfig.Configuration{
		Reporter: &jConfig.ReporterConfig{
			LocalAgentHostPort: jaddr,
		},
		Sampler: &jConfig.SamplerConfig{
			Type:  "const",
			Param: 1.0, // sample all traces
		},
	}
	closer, err := jCfg.InitGlobalTracer("elma365.processor")
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return closer, nil
}
