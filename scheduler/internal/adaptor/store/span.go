package store

import (
	"context"
	"github.com/bfg-dev/crypto-core/_data/pkg/dep/sources/https---github.com-opentracing-opentracing--go"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/opentracing/opentracing-go/log"
)

func withDB(ctx context.Context, name string, fn func(context.Context) error) error {
	sp, ctx := opentracing.StartSpanFromContext(ctx, "db:"+name)
	ext.DBType.Set(sp, "postges")

	err := fn(ctx)
	saveError(sp, err)

	sp.Finish()

	return err
}

func saveError(sp opentracing.Span, err error) {
	if err != nil {
		ext.Error.Set(sp, true)
		sp.LogFields(log.Error(err))
	}
}
