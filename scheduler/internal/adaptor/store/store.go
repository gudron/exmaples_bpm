package store

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"git.elewise.com/elma365/scheduler/internal/adaptor/config"
	"git.elewise.com/elma365/scheduler/internal/common/appctx"
	"git.elewise.com/elma365/scheduler/internal/common/errs"
	"git.elewise.com/elma365/scheduler/internal/model/schedule"

	"github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

const (
	scheduleSettingsTable    TableName = "schedule_settings"
	scheduleSpecialDaysTable TableName = "schedule_settings_specialdays"
	userTable                TableName = "users"
	companySettingsTable     TableName = "company_settings"
)

type transactionCtxKey struct{}

// Store is a wrapper on database
type Store interface {
	GetSettings(ctx context.Context) (*schedule.GeneralSettings, error)
	SetSettings(ctx context.Context, settings *schedule.GeneralSettings) error
	GetSpecialDays(ctx context.Context) (schedule.SpecialDays, error)
	SetSpecialDays(ctx context.Context, newSpecialDays schedule.SpecialDays) error
	GetSpecialDaysAfterDate(ctx context.Context, dateFrom int64) (schedule.SpecialDays, error)
	GetTimezoneSettings(ctx context.Context) (string, error)
}

// Default store
var Default Store

type store struct {
	dbx *sqlx.DB
	sbt squirrel.StatementBuilderType
}

// Setup store instance
func Setup(cfg *config.Config) error {
	dbx, err := connectToPG(cfg)
	if err != nil {

		return err
	}
	sbt := squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar)

	Default = &store{
		dbx: dbx,
		sbt: sbt,
	}

	return nil
}

func connectToPG(cfg *config.Config) (*sqlx.DB, error) {

	addr, err := cfg.DNSResolve("postgres", "tcp", "postgres")
	if err != nil {

		return nil, err
	}
	url := fmt.Sprintf(
		"postgres://%s:%s@%s/%s?sslmode=disable",
		cfg.PostgresUser, cfg.PostgresPassword, addr, cfg.DBName)

	dbx, err := sqlx.Connect("postgres", url)

	return dbx, errors.WithStack(err)
}

// GetSettings Get general settings
//
func GetSettings(ctx context.Context) (*schedule.GeneralSettings, error) {

	return Default.GetSettings(ctx)
}

// SetSettings Set general settings
//
func SetSettings(ctx context.Context, settings *schedule.GeneralSettings) error {

	return Default.SetSettings(ctx, settings)
}

// GetSpecialDays Get special days settings
//
func GetSpecialDays(ctx context.Context) (schedule.SpecialDays, error) {
	return Default.GetSpecialDays(ctx)
}

// SetSpecialDays Set special days settings
//
func SetSpecialDays(ctx context.Context, newSpecialDays schedule.SpecialDays) error {
	return Default.SetSpecialDays(ctx, newSpecialDays)
}

// GetSpecialDaysAfterDate method for get SpecialDays after date
//
func GetSpecialDaysAfterDate(ctx context.Context, dateFrom int64) (schedule.SpecialDays, error) {
	return Default.GetSpecialDaysAfterDate(ctx, dateFrom)
}

// GetTimezoneSettings method for get Timezone from user and company
//
func GetTimezoneSettings(ctx context.Context) (string, error) {
	return Default.GetTimezoneSettings(ctx)
}

func (str *store) GetSettings(ctx context.Context) (*schedule.GeneralSettings, error) {
	settings := &schedule.GeneralSettings{}

	err := withDB(ctx, "Load", func(ctx context.Context) error {
		query, args, err := str.sbt.
			Select("*").
			From(scheduleSettingsTable.WithCompany(ctx)).
			ToSql()

		if err != nil {

			return errors.WithStack(err)
		}

		err = str.dbx.GetContext(ctx, settings, query, args...)
		if err == sql.ErrNoRows {
			return errs.ModelNotFound
		}

		return errors.WithStack(err)
	})

	return settings, err
}

func (str *store) SetSettings(ctx context.Context, settings *schedule.GeneralSettings) error {
	tx, err := str.dbx.BeginTxx(ctx, nil)

	if err != nil {

		return errors.WithStack(err)
	}
	defer tx.Rollback()

	err = withDB(ctx, "Set", func(ctx context.Context) error {

		data := map[string]interface{}{
			"lunch_time_from":   settings.LunchTimeFrom,
			"lunch_time_to":     settings.LunchTimeTo,
			"working_time_from": settings.WorkingTimeFrom,
			"working_time_to":   settings.WorkingTimeTo,
			"weekdays_working":  settings.WeekdaysWorking,
		}

		_, err := str.GetSettings(ctx)

		if errors.Cause(err) == errs.ModelNotFound {
			_, err = str.sbt.
				Insert(scheduleSettingsTable.WithCompany(ctx)).
				SetMap(data).
				RunWith(tx.Tx).
				ExecContext(ctx)

		} else {
			_, err = str.sbt.
				Update(scheduleSettingsTable.WithCompany(ctx)).
				SetMap(data).
				RunWith(tx.Tx).
				ExecContext(ctx)
		}

		return errors.WithStack(err)
	})

	if err != nil {

		return errors.WithStack(err)
	}

	return errors.WithStack(tx.Commit())
}

// ContextWithUserID store userID in context
func contextWithTransaction(ctx context.Context, tx *sqlx.Tx) context.Context {

	return context.WithValue(ctx, transactionCtxKey{}, tx)
}

// UserIDFromContext extract userID from context
func transactionFromContext(ctx context.Context) *sqlx.Tx {
	return ctx.Value(transactionCtxKey{}).(*sqlx.Tx)
}

// UserIDFromContext extract userID from context
func tryTransactionFromContext(ctx context.Context) (*sqlx.Tx, bool) {

	tx, ok := ctx.Value(transactionCtxKey{}).(*sqlx.Tx)
	return tx, ok
}

func (str *store) WithTX(ctx context.Context, fn func(context.Context) error) error {

	var err error

	tx, ok := tryTransactionFromContext(ctx)
	if !ok {
		tx, err = str.dbx.BeginTxx(ctx, nil)
		ctx = contextWithTransaction(ctx, tx)
	}

	if err != nil {

		return errors.WithStack(err)
	}

	defer tx.Rollback()

	err = fn(ctx)

	return errors.WithStack(tx.Commit())
}

func (str *store) GetSpecialDays(ctx context.Context) (schedule.SpecialDays, error) {

	specDays := make(schedule.SpecialDays, 0)

	currentDate := time.Now()
	firstDayOfCurrentYear := time.Date(currentDate.Year(), 1, 1, 0, 0, 0,
		0, time.Local)

	lastDayOfNextYear := time.Date(currentDate.Year()+1, 12, 31, 23, 59, 1,
		1, time.Local)

	err := withDB(ctx, "GetSpecialDays", func(ctx context.Context) error {
		query, args, dbErr := str.sbt.
			Select("date", "isdayoff", "working_time_from", " working_time_to").
			From(scheduleSpecialDaysTable.WithCompany(ctx)).
			Where("date BETWEEN ? and ?", firstDayOfCurrentYear.Unix(), lastDayOfNextYear.Unix()).
			OrderBy("date").
			ToSql()

		if dbErr != nil {

			return errors.WithStack(dbErr)
		}

		dbErr = str.dbx.SelectContext(ctx, &specDays, query, args...)

		return errors.WithStack(dbErr)
	})

	return specDays, err
}

func (str *store) GetSpecialDaysAfterDate(ctx context.Context, dateFrom int64) (schedule.SpecialDays, error) {
	specDays := make(schedule.SpecialDays, 0)

	err := withDB(ctx, "GetSpecialDaysAfterDate", func(ctx context.Context) error {
		query, args, dbErr := str.sbt.
			Select("date", "isdayoff", "working_time_from", " working_time_to").
			From(scheduleSpecialDaysTable.WithCompany(ctx)).
			Where("date > ? ", dateFrom).
			OrderBy("date").
			ToSql()

		if dbErr != nil {

			return errors.WithStack(dbErr)
		}

		dbErr = str.dbx.SelectContext(ctx, &specDays, query, args...)

		return errors.WithStack(dbErr)
	})

	return specDays, err
}

func (str *store) SetSpecialDays(ctx context.Context, newSpecialDays schedule.SpecialDays) error {

	oldSpecialDays, dbErr := str.GetSpecialDays(ctx)

	err := str.WithTX(ctx, func(ctx context.Context) error {
		var err error

		if errors.Cause(dbErr) == errs.ModelNotFound {
			err = str.addSpecialDays(ctx, newSpecialDays)
		} else {

			toInsert, toUpdate, toDelete := schedule.
				SpecialDaysDiff(oldSpecialDays, newSpecialDays)

			if len(toInsert) > 0 {
				err = str.addSpecialDays(ctx, toInsert)
			}

			if len(toUpdate) > 0 {
				err = str.updateSpecialDays(ctx, toUpdate)
			}

			if len(toDelete) > 0 {
				err = str.deleteSpecialDays(ctx, toDelete)
			}

			if err != nil {
				return errors.WithStack(err)
			}

		}

		return errors.WithStack(err)
	})

	return errors.WithStack(err)
}

func (str *store) addSpecialDays(ctx context.Context, newSpecialDays schedule.SpecialDays) error {
	err := withDB(ctx, "AddSpDays", func(ctx context.Context) error {

		tx := transactionFromContext(ctx)

		insertBuilder := str.sbt.
			Insert(scheduleSpecialDaysTable.WithCompany(ctx)).
			Columns("date", "isdayoff", "working_time_from", " working_time_to")

		for _, sd := range newSpecialDays {
			insertBuilder = insertBuilder.Values(sd.Date, sd.IsDayOff, sd.WorkingTimeFrom, sd.WorkingTimeTo)
		}

		query, args, err := insertBuilder.
			ToSql()

		_, err = tx.ExecContext(ctx, query, args...)

		return errors.WithStack(err)
	})

	return errors.WithStack(err)
}

func (str *store) updateSpecialDays(ctx context.Context, specialDays schedule.SpecialDays) error {

	err := withDB(ctx, "AddSpDays", func(ctx context.Context) error {

		var err error

		tx := transactionFromContext(ctx)

		for _, sd := range specialDays {

			query, args, err := str.sbt.
				Update(scheduleSpecialDaysTable.WithCompany(ctx)).
				Set("date", sd.Date).
				Set("isdayoff", sd.IsDayOff).
				Set("working_time_from", sd.WorkingTimeFrom).
				Set("working_time_to", sd.WorkingTimeTo).
				Where("date = ?", sd.Date).
				ToSql()

			if err != nil {
				break
			}

			_, err = tx.ExecContext(ctx, query, args...)

			if err != nil {
				break
			}
		}

		return errors.WithStack(err)
	})

	return errors.WithStack(err)
}

func (str *store) deleteSpecialDays(ctx context.Context, specialDays schedule.SpecialDays) error {

	err := withDB(ctx, "DelSpDays", func(ctx context.Context) error {

		tx := transactionFromContext(ctx)

		deleteBuilder := str.sbt.
			Delete(scheduleSpecialDaysTable.WithCompany(ctx))

		for _, sd := range specialDays {
			deleteBuilder = deleteBuilder.
				Where("date = ?", sd.Date)
		}

		query, args, err := deleteBuilder.
			ToSql()

		_, err = tx.ExecContext(ctx, query, args...)

		return errors.WithStack(err)
	})

	return errors.WithStack(err)
}

func (str *store) GetTimezoneSettings(ctx context.Context) (string, error) {
	userID := appctx.UserIDFromContext(ctx)

	var timezones struct {
		CompanyTimezone string `db:"ctz"`
		UserTimezone    string `db:"utz"`
	}

	var timezone string

	err := withDB(ctx, "GetTimezoneSettings", func(ctx context.Context) error {

		query, args, err := str.sbt.
			Select("company_settings.timezone as ctz", "users.timezone as utz").
			From(companySettingsTable.WithCompany(ctx)).
			Join(userTable.WithCompany(ctx)+" ON users.id = ?", userID).
			ToSql()

		if err != nil {

			return errors.WithStack(err)
		}

		err = str.dbx.GetContext(ctx, &timezones, query, args...)

		timezone = timezones.UserTimezone
		if timezone == "" {
			timezone = timezones.CompanyTimezone
		}

		if err == sql.ErrNoRows || len(timezone) == 0 {
			return errs.ModelNotFound
		}

		return errors.WithStack(err)
	})

	return timezone, errors.WithStack(err)
}
