package store

import (
	"fmt"
	"context"

	"git.elewise.com/elma365/scheduler/internal/common/appctx"
)

// TableName is a name of postgres table
type TableName string

// WithCompany prefix
func (tn TableName) WithCompany(ctx context.Context) string {
	company := appctx.CompanyFromContext(ctx)

	return fmt.Sprintf("%q.%s", company, tn)
}

// IndexName is a name of elastic index
type IndexName string

// WithCompany prefix
func (in IndexName) WithCompany(ctx context.Context) string {
	company := appctx.CompanyFromContext(ctx)

	return fmt.Sprintf("%s@%s", company, in)
}