package appctx

import "context"

type companyCtxKey struct{}

// ContextWithCompany store company in context
func ContextWithCompany(ctx context.Context, company string) context.Context {

	return context.WithValue(ctx, companyCtxKey{}, company)
}

// CompanyFromContext extract company from context
func CompanyFromContext(ctx context.Context) string {

	return ctx.Value(companyCtxKey{}).(string)
}
