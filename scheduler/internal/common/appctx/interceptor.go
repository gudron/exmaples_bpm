package appctx

import (
	"encoding/json"

	"git.elewise.com/elma365/scheduler/internal/common/errs"

	"github.com/asaskevich/govalidator"
	"github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
	"github.com/uber/jaeger-client-go"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

// UnaryMetadataInterceptor extract company from metadata and put it in context
func UnaryMetadataInterceptor(ctx context.Context, req interface{}, _ *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {

		return nil, status.Error(codes.InvalidArgument, "metadata required")
	}

	// extract company
	company, ok := md["company"]
	if !ok || len(company) == 0 {

		return nil, status.Error(codes.InvalidArgument, "company required")
	}
	ctx = ContextWithCompany(ctx, company[0])

	// extract user id
	userIDStr, ok := md["user_id"]
	if !ok || len(userIDStr) == 0 {

		return nil, status.Error(codes.InvalidArgument, "user_id required")
	}
	userID, err := uuid.FromString(userIDStr[0])
	if err != nil {

		return nil, status.Errorf(codes.InvalidArgument, "user_id: %s", err.Error())
	}
	ctx = ContextWithUserID(ctx, userID)

	// extract tracing span
	sp := opentracing.SpanFromContext(ctx)
	var id string
	if sc, ok := sp.Context().(jaeger.SpanContext); ok {
		id = sc.TraceID().String()
	}
	ctx = ContextWithTraceID(ctx, id)

	return handler(ctx, req)
}

// UnaryErrorInterceptor check not nil error to contain response code, log original error, then build and return correct grpc error
func UnaryErrorInterceptor(ctx context.Context, req interface{}, _ *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	res, err := handler(ctx, req)

	if err != nil {
		l := LoggerFromContext(ctx)
		code, alreadyGRPCError := errs.GRPCCodeFromError(err)
		if code == codes.Internal {
			l.Error(err.Error(), errs.ZapStack(err))
		} else {
			l.Debug(err.Error(), errs.ZapStack(err))
		}
		if !alreadyGRPCError {
			err = status.Error(code, err.Error())
		}
	}

	return res, err
}

// UnaryValidateInterceptor validate request data with govalidator and return InvalidArgument error if it is invalid
func UnaryValidateInterceptor(ctx context.Context, req interface{}, _ *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	ok, validationErr := govalidator.ValidateStruct(req)
	if !ok {
		marshaledErr, err := convertValidatorErrorsToJSON(validationErr)
		if err != nil {
			l := LoggerFromContext(ctx)
			l.Error(err.Error())
		}

		return nil, status.Error(codes.InvalidArgument, marshaledErr)
	}

	return handler(ctx, req)
}

func convertValidatorErrorsToJSON(orig error) (string, error) {
	validationErrs, ok := orig.(govalidator.Errors)
	if !ok {

		return orig.Error(), errors.New("original error is not an validator error slice")
	}
	errorMap := make(map[string]string)
	for _, err := range validationErrs {
		namedErr, ok := err.(govalidator.Error)
		if !ok {

			return orig.Error(), errors.Wrap(err, "original error contain not named error")
		}
		errorMap[namedErr.Name] = namedErr.Err.Error()
	}
	res, err := json.Marshal(errorMap)
	if err != nil {

		return orig.Error(), errors.Wrap(err, "marshal")
	}

	return string(res), nil
}
