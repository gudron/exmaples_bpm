package appctx

import (
	"context"

	"go.uber.org/zap"
)

// LoggerFromContext extract company and span from context and add fields to logger
func LoggerFromContext(ctx context.Context) *zap.Logger {

	return zap.L().
		With(zap.String("company", CompanyFromContext(ctx))).
		With(zap.Stringer("user", UserIDFromContext(ctx))).
		With(zap.String("traceId", TraceIDFromContext(ctx)))
}
