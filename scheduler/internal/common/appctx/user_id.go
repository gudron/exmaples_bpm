package appctx

import (
	"context"

	"github.com/satori/go.uuid"
)

type userIDCtxKey struct{}

// ContextWithUserID store userID in context
func ContextWithUserID(ctx context.Context, userID uuid.UUID) context.Context {

	return context.WithValue(ctx, userIDCtxKey{}, userID)
}

// UserIDFromContext extract userID from context
func UserIDFromContext(ctx context.Context) uuid.UUID {

	return ctx.Value(userIDCtxKey{}).(uuid.UUID)
}
