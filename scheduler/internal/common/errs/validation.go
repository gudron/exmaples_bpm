package errs

//go:generate easyjson -lower_camel_case -omit_empty validation.go

import (
	"encoding/json"

	"github.com/pkg/errors"
	"google.golang.org/grpc/codes"
)

// ValidationError is an error with path
//
// easyjson:json
type ValidationError struct {
	Path string `json:"path"`
	Desc string `json:"desc"`
}

// NewValidationError constructor
func NewValidationError(path, desc string) ValidationError {

	return ValidationError{path, desc}
}

// Error return wrapped error string
//
// Implements: error
func (ve ValidationError) Error() string {

	return errors.Wrap(errors.New(ve.Desc), ve.Path).Error()
}

// ValidationErrors is a collection of errors of the some struct
type ValidationErrors struct {
	records  []ValidationError
	critical error
}

// NewValidationErrors constructor
func NewValidationErrors() *ValidationErrors {

	return &ValidationErrors{}
}

// Error return marshaled error
//
// Implements: error
func (ve *ValidationErrors) Error() string {
	res, _ := json.Marshal(ve.records)

	return string(res)
}

// Return nil if empty else self
func (ve *ValidationErrors) Return() error {
	if ve.IsEmpty() {

		return nil
	}

	return ve
}

// IsEmpty error
func (ve *ValidationErrors) IsEmpty() bool {

	return ve.critical == nil && len(ve.records) == 0
}

// IsCritical error
func (ve *ValidationErrors) IsCritical() bool {

	return ve.critical != nil
}

// Add non-critical error
func (ve *ValidationErrors) Add(path, desc string) {
	ve.records = append(ve.records, ValidationError{
		Path: path,
		Desc: desc,
	})
}

// AddError merge with another ValidationErrors and set critical otherwise
func (ve *ValidationErrors) AddError(err error) {
	switch verr := err.(type) {
	default:
		ve.AddCritical(err)

	case nil:
		// pass

	case ValidationError:
		ve.records = append(ve.records, verr)

	case *ValidationErrors:
		if ve.critical == nil {
			ve.critical = verr.critical
		}
		ve.records = append(ve.records, verr.records...)
	}
}

// AddCritical error
func (ve *ValidationErrors) AddCritical(err error) {
	ve.Add("*", err.Error())
	ve.critical = err
}

// Cause return critical causer
//
// Implements: errors.Causer
func (ve *ValidationErrors) Cause() error {
	if ve.critical == nil {

		return InvalidArgument.New("validation error")
	}

	return ve.critical
}

// GRPCCode of the validation error
//
// Implements: withGRPCCode
func (ve *ValidationErrors) GRPCCode() codes.Code {
	if ve.critical == nil {

		return InvalidArgument.GRPCCode()
	}
	code, _ := GRPCCodeFromError(ve.critical)

	return code
}
