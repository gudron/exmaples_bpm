// Code generated by easyjson for marshaling/unmarshaling. DO NOT EDIT.

package errs

import (
	json "encoding/json"
	easyjson "github.com/mailru/easyjson"
	jlexer "github.com/mailru/easyjson/jlexer"
	jwriter "github.com/mailru/easyjson/jwriter"
)

// suppress unused package warning
var (
	_ *json.RawMessage
	_ *jlexer.Lexer
	_ *jwriter.Writer
	_ easyjson.Marshaler
)

func easyjsonFe6ae441DecodeGitElewiseComElma365ProcessorInternalCommonErrs(in *jlexer.Lexer, out *ValidationError) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "path":
			out.Path = string(in.String())
		case "desc":
			out.Desc = string(in.String())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjsonFe6ae441EncodeGitElewiseComElma365ProcessorInternalCommonErrs(out *jwriter.Writer, in ValidationError) {
	out.RawByte('{')
	first := true
	_ = first
	if in.Path != "" {
		if !first {
			out.RawByte(',')
		}
		first = false
		out.RawString("\"path\":")
		out.String(string(in.Path))
	}
	if in.Desc != "" {
		if !first {
			out.RawByte(',')
		}
		first = false
		out.RawString("\"desc\":")
		out.String(string(in.Desc))
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v ValidationError) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjsonFe6ae441EncodeGitElewiseComElma365ProcessorInternalCommonErrs(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v ValidationError) MarshalEasyJSON(w *jwriter.Writer) {
	easyjsonFe6ae441EncodeGitElewiseComElma365ProcessorInternalCommonErrs(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *ValidationError) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjsonFe6ae441DecodeGitElewiseComElma365ProcessorInternalCommonErrs(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *ValidationError) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjsonFe6ae441DecodeGitElewiseComElma365ProcessorInternalCommonErrs(l, v)
}
