package errs

import (
	"github.com/pkg/errors"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// List of global error codes
const (
	ModelNotFound   errorCode = "model not found"
	Internal        errorCode = "internal error"
	InvalidArgument errorCode = "invalid argument"
	// Iteration broken
	Iteration errorCode = "iteration broken"
	// NoWorker queue available
	NoWorker errorCode = "no worker available"
	// Timeout of item execution
	Timeout errorCode = "timeout exceeded"
)

type errorCode string

// Error return string description of code
func (ec errorCode) Error() string {

	return string(ec)
}

// New error from string
func (ec errorCode) New(s string) error {

	return ec.Wrap(errors.New(s))
}

// Wrap error with code
func (ec errorCode) Wrap(err error) error {

	return ErrorWithCode(err, ec)
}

func (ec errorCode) GRPCCode() codes.Code {
	switch ec {
	case ModelNotFound:

		return codes.NotFound

	case Internal:

		return codes.Internal

	case InvalidArgument:

		return codes.InvalidArgument

	case NoWorker:

		return codes.Unavailable

	case Timeout:

		return codes.DeadlineExceeded

	case Iteration:

		return codes.OutOfRange
	}

	return codes.Unknown
}

type errorWithCode struct {
	orig error
	code errorCode
}

func (ewc errorWithCode) Cause() error {

	return ewc.orig
}

func (ewc errorWithCode) Code() errorCode {

	return ewc.code
}

func (ewc errorWithCode) GRPCCode() codes.Code {

	return ewc.code.GRPCCode()
}

func (ewc errorWithCode) Error() string {

	return ewc.orig.Error()
}

// ErrorWithCode to explain grpc error code
func ErrorWithCode(err error, code errorCode) error {
	if err == nil {

		return nil
	}
	err = errors.WithStack(err)

	return errorWithCode{orig: err, code: code}
}

type withGRPCCode interface {
	GRPCCode() codes.Code
}

// GRPCCodeFromError extract code from errors chain
//
// On first error with code or grpc error return it code.
// If no error with code found, return codes.Unknown.
func GRPCCodeFromError(err error) (code codes.Code, alreadyGRPCError bool) {
	for {
		if s, ok := status.FromError(err); ok {

			return s.Code(), true
		}
		if ewc, ok := err.(withGRPCCode); ok {

			return ewc.GRPCCode(), false
		}
		if ewc, ok := err.(withCause); ok {
			err = ewc.Cause()
			continue
		}

		return codes.Unknown, false
	}
}
