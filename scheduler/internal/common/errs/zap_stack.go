package errs

import (
	"fmt"
	"strings"

	"github.com/pkg/errors"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const stacktraceName = "stacktrace"

type withCause interface {
	Cause() error
}

type withStackTrace interface {
	StackTrace() errors.StackTrace
}

// ZapStack try get stacktrace from error and convert it in zapcore.Field
func ZapStack(err error) zapcore.Field {
	var st errors.StackTrace
	for err != nil {
		if e, ok := err.(withStackTrace); ok {
			st = e.StackTrace()
		}
		cause, ok := err.(withCause)
		if !ok {
			break
		}
		err = cause.Cause()
	}

	if st == nil {

		return zap.Stack(stacktraceName)
	}

	return zap.String(
		stacktraceName,
		strings.TrimSpace(
			fmt.Sprintf("%+v", st),
		),
	)
}
