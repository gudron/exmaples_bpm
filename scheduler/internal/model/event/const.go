package event

const (
	// NSSystem is a system level items
	NSSystem = "system"
	// NSGlobal is a global user items
	NSGlobal = "global"
	// NSVirtual is a virtual system items not linked to any namespace
	NSVirtual = "virtual"

	// CollectionItemType is a type code for collection item
	CollectionItemType = "__item"
)
