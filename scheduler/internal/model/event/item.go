package event

import "github.com/bfg-dev/crypto-core/_data/pkg/dep/sources/https---github.com-satori-go.uuid"

type scopeType string

const (
	// Object scope
	Object scopeType = "object"
	// Permissions scope
	Permissions scopeType = "permissions"
)

// Item is an element of event
//
//easyjson:json
type Item struct {
	NS    string      `json:"ns"`
	Code  string      `json:"code"`
	ID    uuid.UUID   `json:"id"`
	Type  string      `json:"type"` // здесь "__item" или название категории (ex. directory)
	Scope scopeType   `json:"scope"`
	Data  interface{} `json:"data"`
}
