package schedule

import (
	"context"
	"time"

	"git.elewise.com/elma365/scheduler/internal/service/pb"
)

// Deadline struct for store and calculate when task will done
type Deadline struct {
	startedAt  time.Time
	willDoneAt time.Time
	gs         *GeneralSettings
	sd         *SpecialDays
}

// NewDeadline function for create Deadline instance
func NewDeadline(ctx context.Context,
	startedAt int64,
	timezone string,
	gs *GeneralSettings,
	sp *SpecialDays) *Deadline {

	loc, _ := time.LoadLocation(timezone)

	return &Deadline{
		startedAt:  time.Unix(startedAt, 0).In(loc),
		willDoneAt: time.Unix(startedAt, 0).In(loc),
		gs:         gs,
		sd:         sp,
	}
}

// CalculateFinishDateFor method for calculate Date when task will be done
func (dl *Deadline) CalculateFinishDateFor(ctx context.Context, timeToWork int64) time.Time {
	availableTimeWorWork := time.Second * time.Duration(timeToWork)

	for availableTimeWorWork > 0 {
		isDayOff := true

		wdSchedule := &WorkingDaySchedule{
			LunchTimeFrom: dl.gs.LunchTimeFrom,
			LunchTimeTo:   dl.gs.LunchTimeTo,
		}

		isSpecialDay, specialDay := dl.sd.isSpecialDay(ctx, dl.willDoneAt)

		if isSpecialDay {
			isDayOff = specialDay.IsDayOff

			wdSchedule.WorkingTimeFrom = specialDay.WorkingTimeFrom
			wdSchedule.WorkingTimeTo = specialDay.WorkingTimeTo
		} else {
			isDayOff = dl.gs.isDayOff(ctx, dl.willDoneAt)

			wdSchedule.WorkingTimeFrom = dl.gs.WorkingTimeFrom
			wdSchedule.WorkingTimeTo = dl.gs.WorkingTimeTo
		}

		if isDayOff {
			dl.willDoneAt = time.Date(dl.willDoneAt.Year(), dl.willDoneAt.Month(), dl.willDoneAt.Day()+1,
				0, 0, 0, 0, dl.willDoneAt.Location())
		} else {
			workingDay := NewWorkingDay(ctx, dl.willDoneAt, *wdSchedule)

			willBeDoneToday, debt := workingDay.IsItWillBeDoneToday(ctx, availableTimeWorWork)

			if willBeDoneToday {
				dl.willDoneAt = workingDay.TakeTimeFor(ctx, availableTimeWorWork)
				availableTimeWorWork = 0
			} else {
				availableTimeWorWork = debt
				dl.willDoneAt = time.Date(workingDay.StartTime.Year(),
					workingDay.StartTime.Month(),
					workingDay.StartTime.Day()+1,
					0, 0, 0, 0, dl.willDoneAt.Location())
			}
		}
	}

	return dl.willDoneAt
}

// ToProto make protobuf message from DeadlineDate
func (dl *Deadline) ToProto(ctx context.Context) *pb.GetWorkTimeRs {
	proto := pb.GetWorkTimeRs{
		WillDoneAt: dl.willDoneAt.Unix(),
	}

	return &proto
}
