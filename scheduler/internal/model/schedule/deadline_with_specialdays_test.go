package schedule

import (
	"context"
	"testing"

	"fmt"

	"time"

	"github.com/stretchr/testify/assert"
)

func TestDeadlineWithSpecialDays(t *testing.T) {
	var ctx context.Context

	euTimezoneName := "Europe/Samara"
	euSamaraTimezone, _ := time.LoadLocation("Europe/Samara")
	var workingTimeDuration int64 = 46800 // 13h
	specDays := make(SpecialDays, 0)

	specDays = append(specDays, SpecialDay{
		IsDayOff:        true,
		Date:            time.Date(2018, 05, 1, 0, 0, 0, 0, euSamaraTimezone).Unix(),
		WorkingTimeFrom: uint32(0),
		WorkingTimeTo:   uint32(0),
	}, SpecialDay{
		IsDayOff:        true,
		Date:            time.Date(2018, 05, 8, 0, 0, 0, 0, euSamaraTimezone).Unix(),
		WorkingTimeFrom: uint32(0),
		WorkingTimeTo:   uint32(0),
	}, SpecialDay{
		IsDayOff:        true,
		Date:            time.Date(2018, 05, 9, 0, 0, 0, 0, euSamaraTimezone).Unix(),
		WorkingTimeFrom: uint32(0),
		WorkingTimeTo:   uint32(0),
	}, SpecialDay{
		IsDayOff:        false,
		Date:            time.Date(2018, 05, 18, 0, 0, 0, 0, euSamaraTimezone).Unix(),
		WorkingTimeFrom: uint32((time.Duration(10) * time.Hour).Seconds()),
		WorkingTimeTo:   uint32((time.Duration(16) * time.Hour).Seconds()),
	}, SpecialDay{
		IsDayOff:        false,
		Date:            time.Date(2018, 05, 19, 0, 0, 0, 0, euSamaraTimezone).Unix(),
		WorkingTimeFrom: uint32((time.Duration(10) * time.Hour).Seconds() + (time.Duration(30) * time.Minute).Seconds()),
		WorkingTimeTo:   uint32((time.Duration(16) * time.Hour).Seconds() + (time.Duration(30) * time.Minute).Seconds()),
	}, SpecialDay{
		IsDayOff:        true,
		Date:            time.Date(2018, 05, 20, 0, 0, 0, 0, euSamaraTimezone).Unix(),
		WorkingTimeFrom: uint32(0),
		WorkingTimeTo:   uint32(0),
	}, SpecialDay{
		IsDayOff:        false,
		Date:            time.Date(2018, 05, 28, 0, 0, 0, 0, euSamaraTimezone).Unix(),
		WorkingTimeFrom: uint32((time.Duration(7) * time.Hour).Seconds() + (time.Duration(30) * time.Minute).Seconds()),
		WorkingTimeTo:   uint32((time.Duration(19) * time.Hour).Seconds()),
	}, SpecialDay{
		IsDayOff:        false,
		Date:            time.Date(2018, 06, 1, 0, 0, 0, 0, euSamaraTimezone).Unix(),
		WorkingTimeFrom: uint32((time.Duration(7) * time.Hour).Seconds()),
		WorkingTimeTo:   uint32((time.Duration(20) * time.Hour).Seconds()),
	})

	settings := &GeneralSettings{
		WorkingTimeFrom: uint32((time.Duration(9) * time.Hour).Seconds()),  //09:00
		WorkingTimeTo:   uint32((time.Duration(18) * time.Hour).Seconds()), // 18:00

		LunchTimeFrom: uint32((time.Duration(12) * time.Hour).Seconds()), // 12:00
		LunchTimeTo:   uint32((time.Duration(13) * time.Hour).Seconds()), //13:00

		WeekdaysWorking: 96,
	}

	tables := []struct {
		name      string
		startedAt time.Time
		duration  int64
		timezone  string
		result    time.Time
	}{
		{
			name: "noon of the day off",
			startedAt: time.Date(2018, 5, 5, 12, 0,
				0, 0, euSamaraTimezone),
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 5, 10, 15, 0,
				0, 0, euSamaraTimezone),
		}, {
			name: "working day after lunch before special days",
			startedAt: time.Date(2018, 5, 7, 15, 0,
				0, 0, euSamaraTimezone),
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 5, 11, 11, 0,
				0, 0, euSamaraTimezone),
		}, {
			name: "working day after working time start but before lunch @ special short day",
			startedAt: time.Date(2018, 5, 18, 10, 30,
				0, 0, euSamaraTimezone),
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 5, 21, 13, 30,
				0, 0, euSamaraTimezone),
		}, {
			name: "at the lunch in special dayoff day",
			startedAt: time.Date(2018, 5, 8, 12, 30,
				0, 0, euSamaraTimezone),
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 5, 11, 15, 0,
				0, 0, euSamaraTimezone),
		}, {
			name: "after lunch in special short day",
			startedAt: time.Date(2018, 5, 18, 13, 30,
				0, 0, euSamaraTimezone),
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 5, 21, 15, 30,
				0, 0, euSamaraTimezone),
		}, {
			name: "@ 22:00 of working days before special day",
			startedAt: time.Date(2018, 5, 7, 22, 0,
				0, 0, euSamaraTimezone),
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 5, 11, 15, 0,
				0, 0, euSamaraTimezone), // 2018-05-17 15:00:00
		}, {
			name: "@ 22:00 of special day",
			startedAt: time.Date(2018, 5, 8, 22, 0,
				0, 0, euSamaraTimezone),
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 5, 11, 15, 0,
				0, 0, euSamaraTimezone),
		}, {
			name: "after lunch in working day but before weekends shorten special days and dayoff",
			startedAt: time.Date(2018, 5, 17, 16, 0,
				0, 0, euSamaraTimezone),
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 5, 21, 10, 0,
				0, 0, euSamaraTimezone),
		}, {
			name: "@ start of special long working day",
			startedAt: time.Date(2018, 5, 28, 7, 30,
				0, 0, euSamaraTimezone),
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 5, 29, 11, 30,
				0, 0, euSamaraTimezone),
		}, {
			name: "after lunch of special long working day",
			startedAt: time.Date(2018, 5, 28, 12, 30,
				0, 0, euSamaraTimezone),
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 5, 29, 17, 0,
				0, 0, euSamaraTimezone),
		}, {
			name: "before lunch of special long working day before dayoff",
			startedAt: time.Date(2018, 6, 1, 10, 30,
				0, 0, euSamaraTimezone),
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 6, 4, 14, 30,
				0, 0, euSamaraTimezone),
		}, {
			name: "after lunch of special long working day before dayoff",
			startedAt: time.Date(2018, 6, 1, 14, 30,
				0, 0, euSamaraTimezone),
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 6, 4, 17, 30,
				0, 0, euSamaraTimezone),
		},
	}

	for _, table := range tables {
		t.Run(fmt.Sprintf("test %s", table.name), func(t *testing.T) {

			deadline := NewDeadline(ctx, table.startedAt.Unix(), table.timezone,
				settings,
				&specDays)

			res := deadline.CalculateFinishDateFor(ctx, table.duration)

			assert.Equal(t, table.result.Unix(), res.Unix(),
				"they should be %s ,but %s \n Test name: %s", table.result, res, table.name)
		})
	}
}
