package schedule

import (
	"context"
	"testing"

	"fmt"

	"time"

	"github.com/stretchr/testify/assert"
)

func TestDeadlineWithoutSpecialDays(t *testing.T) {
	var ctx context.Context

	euTimezoneName := "Europe/Samara"
	euSamaraTimezone, _ := time.LoadLocation("Europe/Samara")
	var workingTimeDuration int64 = 46800 // 13h
	specDays := make(SpecialDays, 0)
	settings := &GeneralSettings{
		WorkingTimeFrom: uint32((time.Duration(9) * time.Hour).Seconds()),  //09:00
		WorkingTimeTo:   uint32((time.Duration(18) * time.Hour).Seconds()), // 18:00

		LunchTimeFrom: uint32((time.Duration(12) * time.Hour).Seconds()), // 12:00
		LunchTimeTo:   uint32((time.Duration(13) * time.Hour).Seconds()), //13:00

		WeekdaysWorking: 96,
	}

	tables := []struct {
		name      string
		startedAt time.Time
		duration  int64
		timezone  string
		result    time.Time
	}{
		{
			name: "noon of the day off",
			startedAt: time.Date(2018, 5, 5, 12, 0,
				0, 0, euSamaraTimezone),
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 5, 8, 15, 0,
				0, 0, euSamaraTimezone),
		}, {
			name: "working day before working time",
			startedAt: time.Date(2018, 5, 15, 6, 0,
				0, 0, euSamaraTimezone),
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 5, 16, 15, 0,
				0, 0, euSamaraTimezone),
		}, {
			name: "working day after working time start but before lunch",
			startedAt: time.Date(2018, 5, 15, 11, 0,
				0, 0, euSamaraTimezone),
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 5, 16, 17, 0,
				0, 0, euSamaraTimezone),
		}, { // at the lunch
			name: "at the lunch",
			startedAt: time.Date(2018, 5, 15, 12, 30,
				0, 0, euSamaraTimezone),
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 5, 16, 18, 0,
				0, 0, euSamaraTimezone),
		}, { // after lunch
			name: "after lunch",
			startedAt: time.Date(2018, 5, 15, 16, 0,
				0, 0, euSamaraTimezone), // 2018-05-15 16:00:00
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 5, 17, 12, 0,
				0, 0, euSamaraTimezone), // 2018-05-17 10:00:00
		}, { // @ 22:00
			name: "@ 22:00",
			startedAt: time.Date(2018, 5, 15, 22, 0,
				0, 0, euSamaraTimezone), // 2018-05-15 22:00:00
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 5, 17, 15, 0,
				0, 0, euSamaraTimezone), // 2018-05-17 15:00:00
		}, { // after lunch, but before weekends (friday after lunch)
			name: "after lunch but before weekends (friday after lunch)",
			startedAt: time.Date(2018, 5, 18, 16, 0,
				0, 0, euSamaraTimezone), // 2018-05-18 16:00:00
			duration: workingTimeDuration,
			timezone: euTimezoneName,
			result: time.Date(2018, 5, 22, 12, 0,
				0, 0, euSamaraTimezone), // 2018-05-22 10:00:00
		},
	}

	for _, table := range tables {
		t.Run(fmt.Sprintf("test %s", table.name), func(t *testing.T) {

			deadline := NewDeadline(ctx, table.startedAt.Unix(), table.timezone,
				settings,
				&specDays)

			res := deadline.CalculateFinishDateFor(ctx, table.duration)

			assert.Equal(t, table.result.Unix(), res.Unix(),
				"they should be %s ,but %s \n Test name: %s", table.result, res, table.name)
		})
	}
}
