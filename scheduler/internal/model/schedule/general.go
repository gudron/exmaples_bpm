package schedule

import (
	"context"
	"time"

	"git.elewise.com/elma365/scheduler/internal/service/pb"
)

// GeneralSettings is an struct for store Scheduler general settings
// easyjson:json
type GeneralSettings struct {
	LunchTimeFrom uint32 `db:"lunch_time_from"`
	LunchTimeTo   uint32 `db:"lunch_time_to"`

	WorkingTimeFrom uint32              `db:"working_time_from"`
	WorkingTimeTo   uint32              `db:"working_time_to"`
	WeekdaysWorking WeekDaysWorkingMask `db:"weekdays_working"`
}

// ToProto marshaler to protobuf
func (gs *GeneralSettings) ToProto(ctx context.Context) *pb.GeneralRs {
	proto := pb.GeneralRs{
		WorkingDays: gs.WeekdaysWorking.ToProto(ctx),
		DaySchedule: &pb.DaySchedule{
			LunchTime: &pb.TimeRange{
				From: gs.LunchTimeFrom,
				To:   gs.LunchTimeTo,
			},
			WorkingTime: &pb.TimeRange{
				From: gs.WorkingTimeFrom,
				To:   gs.WorkingTimeTo,
			},
		},
	}

	return &proto
}

// FromProto marshaler from protobuf
func (gs *GeneralSettings) FromProto(ctx context.Context, protoMessage *pb.SetGeneralRq) {
	gs.LunchTimeFrom = protoMessage.DaySchedule.LunchTime.From
	gs.LunchTimeTo = protoMessage.DaySchedule.LunchTime.To

	gs.WorkingTimeFrom = protoMessage.DaySchedule.WorkingTime.From
	gs.WorkingTimeTo = protoMessage.DaySchedule.WorkingTime.To

	gs.WeekdaysWorking.FromProto(ctx, protoMessage.WorkingDays)
}

func (gs *GeneralSettings) isDayOff(ctx context.Context, day time.Time) bool {
	dayNum := int(day.Weekday())
	return gs.WeekdaysWorking.isDayOff(ctx, dayNum)
}
