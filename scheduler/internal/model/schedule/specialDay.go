package schedule

// SpecialDay is an struct for store Special Days settings
type SpecialDay struct {
	Date     int64 `db:"date"`
	IsDayOff bool  `db:"isdayoff"`

	WorkingTimeFrom uint32 `db:"working_time_from"`
	WorkingTimeTo   uint32 `db:"working_time_to"`
}

// IsWorkOffDay check is day off
func (sd *SpecialDay) IsWorkOffDay() bool {
	return sd.IsDayOff
}
