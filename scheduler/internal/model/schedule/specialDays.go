package schedule

import (
	"context"
	"time"

	"git.elewise.com/elma365/scheduler/internal/service/pb"
)

// SpecialDays is list of SpecialDay
type SpecialDays []SpecialDay

// SpecialDaysDiff Diff between two SpecialDay lists
// It returns three slice new elements, updated elements, deleted elements
func SpecialDaysDiff(first SpecialDays, second SpecialDays) (SpecialDays, SpecialDays, SpecialDays) {

	oldDates := make(map[int64]SpecialDay)
	newDates := make(map[int64]SpecialDay)

	newElements := make(SpecialDays, 0)
	updatedElements := make(SpecialDays, 0)
	deletedElements := make(SpecialDays, 0)

	for _, sd := range first {
		oldDates[sd.Date] = sd
	}

	for _, sd := range second {
		newDates[sd.Date] = sd

		specDay, exist := oldDates[sd.Date]
		if exist {
			if isChanged(specDay, sd) {
				updatedElements = append(updatedElements, sd)
			}
		} else {
			newElements = append(newElements, sd)
		}
	}

	for _, sd := range first {
		_, exist := newDates[sd.Date]
		if !exist {
			deletedElements = append(deletedElements, sd)
		}
	}

	return newElements, updatedElements, deletedElements
}

// isChanged compare two SpecialDay objects
// It returns false if objects is equal, and true if not
func isChanged(first SpecialDay, second SpecialDay) bool {
	isChanged := false

	if first.WorkingTimeFrom != second.WorkingTimeFrom {
		isChanged = true
	}

	if first.WorkingTimeTo != second.WorkingTimeTo {
		isChanged = true
	}

	if first.IsDayOff != second.IsDayOff {
		isChanged = true
	}

	return isChanged
}

// ToProto marshaler to protobuf
func (sd *SpecialDays) ToProto() *pb.SpecialDaysRs {
	proto := pb.SpecialDaysRs{}

	for _, sDay := range *sd {
		proto.SpecialDays = append(proto.SpecialDays, &pb.SpecialDay{
			Date:     sDay.Date,
			IsDayOff: sDay.IsDayOff,
			TimeRange: &pb.TimeRange{
				From: sDay.WorkingTimeFrom,
				To:   sDay.WorkingTimeTo,
			},
		})
	}

	return &proto
}

// FromProto marshaler from protobuf
func (sd *SpecialDays) FromProto(ctx context.Context, protoMessage *pb.SetSpecialDaysRq) {

	for _, pMsg := range protoMessage.SpecialDays {

		specDay := time.Unix(pMsg.Date, 0)
		startOfDay := time.Date(specDay.Year(), specDay.Month(), specDay.Day(),
			0, 0, 0, 0, time.Local)

		newSd := SpecialDay{
			Date:            startOfDay.Unix(),
			IsDayOff:        pMsg.IsDayOff,
			WorkingTimeFrom: pMsg.From,
			WorkingTimeTo:   pMsg.To,
		}

		*sd = append(*sd, newSd)
	}
}

func (sd *SpecialDays) isDayOff(ctx context.Context, day time.Time) bool {
	dayToFindUnix := time.Date(day.Year(), day.Month(), day.Day(),
		0, 0, 0, 0, day.Location()).Unix()

	for _, sday := range *sd {
		if sday.Date == dayToFindUnix {
			return sday.IsDayOff
		}
	}

	return false
}

func (sd *SpecialDays) isSpecialDay(ctx context.Context, day time.Time) (bool, SpecialDay) {

	dayToFindUnix := time.Date(day.Year(), day.Month(), day.Day(),
		0, 0, 0, 0, day.Location()).Unix()

	var specialDay SpecialDay

	for _, sday := range *sd {
		if sday.Date == dayToFindUnix {

			return true, sday
		}
	}

	return false, specialDay
}
