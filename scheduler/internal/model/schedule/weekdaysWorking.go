package schedule

import (
	"context"
	"time"

	"git.elewise.com/elma365/scheduler/internal/service/pb"
)

// WeekDaysWorkingMask type for store workingdays in week schedule
type WeekDaysWorkingMask uint8

// WeekDaysWorkingToMask get workingdays mask
func WeekDaysWorkingToMask(p pb.Weekdays) WeekDaysWorkingMask {
	return 1 << uint8(p)
}

// Has day is working
func (mask WeekDaysWorkingMask) Has(p pb.Weekdays) bool {
	return WeekDaysWorkingToMask(p)&mask > 0
}

// WriteDay set WorkingDay in mask
func WriteDay(WeekDaysMask *WeekDaysWorkingMask, IsWorking bool) {
	if IsWorking {
		*WeekDaysMask++
	}

	*WeekDaysMask <<= uint8(1)
}

// FromProto set workingDays mask from protobuf message
func (mask *WeekDaysWorkingMask) FromProto(ctx context.Context, days *pb.WorkingDays) {
	WeekDaysMask := WeekDaysWorkingMask(0)

	WriteDay(&WeekDaysMask, days.GetSunday())
	WriteDay(&WeekDaysMask, days.GetSaturday())
	WriteDay(&WeekDaysMask, days.GetFriday())
	WriteDay(&WeekDaysMask, days.GetThursday())
	WriteDay(&WeekDaysMask, days.GetWednesday())
	WriteDay(&WeekDaysMask, days.GetTuesday())
	WriteDay(&WeekDaysMask, days.GetMonday())

	WeekDaysMask >>= uint8(1)

	*mask = WeekDaysMask
}

// ToProto make protobuf message from WeekDaysWorkingMask mask
func (mask WeekDaysWorkingMask) ToProto(ctx context.Context) *pb.WorkingDays {
	res := pb.WorkingDays{}

	res.Monday = mask.Has(pb.Weekdays_monday)
	res.Tuesday = mask.Has(pb.Weekdays_tuesday)
	res.Wednesday = mask.Has(pb.Weekdays_wednesday)
	res.Thursday = mask.Has(pb.Weekdays_thursday)
	res.Friday = mask.Has(pb.Weekdays_friday)
	res.Saturday = mask.Has(pb.Weekdays_saturday)
	res.Sunday = mask.Has(pb.Weekdays_sunday)

	return &res
}

func (mask WeekDaysWorkingMask) isDayOff(ctx context.Context, dayNum int) bool {
	weekdaysMask := WeekDaysWorkingMask(0)

	WriteDay(&weekdaysMask, dayNum == int(time.Sunday))
	WriteDay(&weekdaysMask, dayNum == int(time.Saturday))
	WriteDay(&weekdaysMask, dayNum == int(time.Friday))
	WriteDay(&weekdaysMask, dayNum == int(time.Thursday))
	WriteDay(&weekdaysMask, dayNum == int(time.Wednesday))
	WriteDay(&weekdaysMask, dayNum == int(time.Monday))

	return weekdaysMask&mask > 0
}
