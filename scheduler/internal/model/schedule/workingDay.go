package schedule

import (
	"context"
	"time"
)

// WorkingDaySchedule struct
type WorkingDaySchedule struct {
	LunchTimeFrom uint32 `db:"lunch_time_from"`
	LunchTimeTo   uint32 `db:"lunch_time_to"`

	WorkingTimeFrom uint32 `db:"working_time_from"`
	WorkingTimeTo   uint32 `db:"working_time_to"`
}

// WorkingDay struct
type WorkingDay struct {
	StartTime             time.Time
	WorkingDayMustStartAt time.Time
	WorkingDayMustEndAt   time.Time
	LunchMustStartAt      time.Time
	LunchMustEndAt        time.Time
	LunchTimeDuration     time.Duration
	CanWorkBeforeLunch    bool
	CanWorkAfterLunch     bool
}

// NewWorkingDay function for create WorkingDay instance
func NewWorkingDay(ctx context.Context,
	StartTime time.Time,
	Schedule WorkingDaySchedule) *WorkingDay {

	workingDayMustStartAt := time.Date(StartTime.Year(), StartTime.Month(), StartTime.Day(),
		0, 0, 0, 0, StartTime.Location()).
		Add(time.Second * time.Duration(Schedule.WorkingTimeFrom))

	workingDayMustEndAt := time.Date(StartTime.Year(), StartTime.Month(), StartTime.Day(),
		0, 0, 0, 0, StartTime.Location()).
		Add(time.Second * time.Duration(Schedule.WorkingTimeTo))

	lunchMustStartAt := time.Date(StartTime.Year(), StartTime.Month(), StartTime.Day(),
		0, 0, 0, 0, StartTime.Location()).
		Add(time.Second * time.Duration(Schedule.LunchTimeFrom))

	lunchMustEndAt := time.Date(StartTime.Year(), StartTime.Month(), StartTime.Day(),
		0, 0, 0, 0, StartTime.Location()).
		Add(time.Second * time.Duration(Schedule.LunchTimeTo))

	wd := &WorkingDay{
		StartTime:             StartTime,
		WorkingDayMustStartAt: workingDayMustStartAt,
		WorkingDayMustEndAt:   workingDayMustEndAt,
		LunchMustStartAt:      lunchMustStartAt,
		LunchMustEndAt:        lunchMustEndAt,
		LunchTimeDuration:     lunchMustEndAt.Sub(lunchMustStartAt),
	}

	wd.prepareData(ctx)

	return wd
}

func (wd *WorkingDay) prepareData(ctx context.Context) {

	// check is started time not after start of working day
	if !wd.StartTime.After(wd.WorkingDayMustStartAt) {
		wd.StartTime = wd.WorkingDayMustStartAt
	}

	if wd.CheckTimeIsLunchTime(wd.StartTime) {
		wd.StartTime = wd.LunchMustEndAt
	}

	// check is started  after working day end
	if wd.StartTime.After(wd.WorkingDayMustEndAt) {
		wd.StartTime = wd.WorkingDayMustEndAt
	}

	wd.CanWorkBeforeLunch = wd.CheckCanWorkBeforeLunchTime(wd.StartTime)
	wd.CanWorkAfterLunch = wd.CanWorkBeforeLunch

	if !wd.CanWorkBeforeLunch {
		wd.CanWorkAfterLunch = wd.CheckCanWorkAfterLunchTime(wd.StartTime)
	}
}

// CheckTimeIsLunchTime check passed Time is LunchTime
func (wd *WorkingDay) CheckTimeIsLunchTime(timeToCheck time.Time) bool {
	// check is started time in lunch time
	return (timeToCheck.After(wd.LunchMustStartAt) || timeToCheck.Equal(wd.LunchMustStartAt)) &&
		timeToCheck.Before(wd.LunchMustEndAt)
}

// CheckCanWorkBeforeLunchTime check can work before lunch if start work @ passed time
func (wd *WorkingDay) CheckCanWorkBeforeLunchTime(timeToCheck time.Time) bool {
	canWorkBeforeLunch := timeToCheck.After(wd.WorkingDayMustStartAt) ||
		timeToCheck.Equal(wd.WorkingDayMustStartAt)

	return canWorkBeforeLunch && timeToCheck.Before(wd.LunchMustStartAt)
}

// CheckCanWorkAfterLunchTime check can work after lunch if start work @ passed time
func (wd *WorkingDay) CheckCanWorkAfterLunchTime(timeToCheck time.Time) bool {
	canWorkAfterLunch := timeToCheck.After(wd.LunchMustEndAt) || timeToCheck.Equal(wd.LunchMustEndAt)

	return canWorkAfterLunch && timeToCheck.Before(wd.WorkingDayMustEndAt)
}

// TakeAvailableWorkingTime for current working day
func (wd *WorkingDay) TakeAvailableWorkingTime(ctx context.Context) time.Duration {
	workingTimeDuration := time.Duration(0)
	tmp := wd.StartTime

	if wd.CanWorkBeforeLunch {
		workingTimeDuration += wd.LunchMustStartAt.Sub(tmp)
		tmp = wd.LunchMustEndAt
	}

	if wd.CanWorkAfterLunch {
		workingTimeDuration += wd.WorkingDayMustEndAt.Sub(tmp)
	}

	return workingTimeDuration
}

// IsItWillBeDoneToday returns true/false of possibility end work today
func (wd *WorkingDay) IsItWillBeDoneToday(ctx context.Context, durationOfWork time.Duration) (bool, time.Duration) {
	debt := durationOfWork - wd.TakeAvailableWorkingTime(ctx)

	if debt <= 0 {
		return true, debt
	}

	return false, debt
}

// TakeTimeFor returns Time when work is done. Works only with current day
func (wd *WorkingDay) TakeTimeFor(ctx context.Context, durationOfWork time.Duration) time.Time {
	willBeDoneAt := wd.StartTime
	timeMustBeWorked := durationOfWork

	if wd.CanWorkBeforeLunch {
		workedTime := wd.LunchMustStartAt.Sub(wd.WorkingDayMustStartAt)

		timeMustBeWorked -= workedTime
	}

	if timeMustBeWorked > 0 && wd.CanWorkAfterLunch {
		willBeDoneAt = wd.LunchMustEndAt.
			Add(timeMustBeWorked)
	}

	if timeMustBeWorked <= 0 {
		willBeDoneAt = willBeDoneAt.Add(durationOfWork)
	}

	return willBeDoneAt
}
