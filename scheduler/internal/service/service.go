package service

import (
	"context"

	"git.elewise.com/elma365/scheduler/internal/adaptor/store"
	"git.elewise.com/elma365/scheduler/internal/model/schedule"
	"git.elewise.com/elma365/scheduler/internal/service/pb"
)

// Service extend pb.ProcessorServer with worker methods
type Service interface {
	pb.SchedulerServer
}

type service struct{}

// New instance of service
func New() Service {

	return &service{}
}

// General get general scheduler settings
func (srv *service) General(ctx context.Context, in *pb.GeneralRq) (*pb.GeneralRs, error) {

	settings, err := store.GetSettings(ctx)

	if err != nil {
		return nil, err
	}

	return settings.ToProto(ctx), nil
}

// SetGeneral general scheduler settings
func (srv *service) SetGeneral(ctx context.Context, in *pb.SetGeneralRq) (*pb.GeneralRs, error) {

	settings := &schedule.GeneralSettings{}
	settings.FromProto(ctx, in)

	err := store.SetSettings(ctx, settings)

	if err != nil {
		return nil, err
	}

	return settings.ToProto(ctx), nil
}

// GetSpecialDays scheduler settings
func (srv *service) GetSpecialDays(ctx context.Context, in *pb.SpecialDaysRq) (*pb.SpecialDaysRs, error) {

	specialDays, err := store.GetSpecialDays(ctx)

	if err != nil {
		return nil, err
	}

	return specialDays.ToProto(), nil
}

// GetSpecialDays scheduler settings
func (srv *service) SetSpecialDays(ctx context.Context, in *pb.SetSpecialDaysRq) (*pb.SpecialDaysRs, error) {

	specialDays := make(schedule.SpecialDays, 0)
	specialDays.FromProto(ctx, in)

	err := store.SetSpecialDays(ctx, specialDays)

	if err != nil {
		return nil, err
	}

	allSpecialDays, err := store.GetSpecialDays(ctx)

	if err != nil {
		return nil, err
	}

	return allSpecialDays.ToProto(), nil
}

// General get general scheduler settings
func (srv *service) GetWorkTime(ctx context.Context, in *pb.GetWorkTimeRq) (*pb.GetWorkTimeRs, error) {

	timezone, err := store.GetTimezoneSettings(ctx)

	if err != nil {
		return nil, err
	}

	generalSettings, err := store.GetSettings(ctx)

	if err != nil {
		return nil, err
	}

	specialDays, err := store.GetSpecialDaysAfterDate(ctx, in.StartedAt)

	if err != nil {
		return nil, err
	}

	deadline := schedule.NewDeadline(ctx, in.StartedAt, timezone,
		generalSettings,
		&specialDays)

	deadline.CalculateFinishDateFor(ctx, in.TimeForWork)

	return deadline.ToProto(ctx), nil
}
