import requests, os, sys
import logging

logging.basicConfig(format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
    datefmt='%d-%m-%Y:%H:%M:%S',
    level=logging.ERROR)
logger = logging.getLogger('view-init')


rabbitmq_user = os.getenv('RABBITMQ_USER')
rabbitmq_password = os.getenv('RABBITMQ_PASSWORD')

rabbitmq_processor_user = os.getenv('RABBITMQ_PROCESSOR_USER')
rabbitmq_processor_password = os.getenv('RABBITMQ_PROCESSOR_PASSWORD')


# создать exchange
payload = {'type': 'topic', 'auto_delete': False, 'durable': True }
try:
    response = requests.put('http://rabbitmq:15672/api/exchanges/elma365/scripts', json=payload, auth=(rabbitmq_user, rabbitmq_password))
    if response.status_code not in [201,204]:
        raise ValueError('Unexpected status code: ', response.status_code)
except Exception as e:
    logger.error(e)
    sys.exit(1)


# создать очередь
payload = {'auto_delete': False, 'durable': True}
try:
    response = requests.put('http://rabbitmq:15672/api/queues/elma365/scripts.results', json=payload, auth=(rabbitmq_user, rabbitmq_password))
    if response.status_code not in [201,204]:
        raise ValueError('Unexpected status code: ', response.status_code)
except Exception as e:
    logger.error(e)
    sys.exit(1)


# создать пользователя rmq
payload = { "password": rabbitmq_processor_password, "tags": "" }
try:
    response = requests.put('http://rabbitmq:15672/api/users/processor', json=payload, auth=(rabbitmq_user, rabbitmq_password))
    if response.status_code not in [201,204]:
        raise ValueError('Unexpected status code: ', response.status_code)
except Exception as e:
    logger.error(e)
    sys.exit(1)

# permissions
payload = {
            "configure": "^(amq\.(gen.*|default)|scripts)$",
            "write":     "^(amq\.(gen.*|default)|scripts)$",
            "read":      "^(amq\.(gen.*|default)|scripts|scripts\.results)$"
          }
try:
    response = requests.put('http://rabbitmq:15672/api/permissions/elma365/processor', json=payload, auth=(rabbitmq_user, rabbitmq_password))
    if response.status_code not in [201,204]:
        raise ValueError('Unexpected status code: ', response.status_code)
except Exception as e:
    logger.error(e)
    sys.exit(1)
