package category

//go:generate easyjson category.go

import (
	"github.com/satori/go.uuid"
	"fmt"
)

// Category Id, Name
// easyjson:json
type Category struct {
	ID uuid.UUID `json:"id"`
	Name string `json:"name"`
}

// Validate email field type
func (c Category) Validate() error {
	if uuid.Equal(c.ID, uuid.Nil) {
		return fmt.Errorf("uuid could not be nil value")
	}

	if len(c.Name) == 0 {
		return fmt.Errorf("category name required")
	}

	return nil
}