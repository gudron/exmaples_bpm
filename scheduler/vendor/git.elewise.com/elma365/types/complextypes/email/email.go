package email

import (
	"fmt"

	"github.com/asaskevich/govalidator"
)

//go:generate easyjson email.go

// Email is a tuple of email information
//
// easyjson:json
type Email struct {
	Type  string `json:"type"`
	Email string `json:"email"`
}

// Validate email field type
func (e Email) Validate() error {
	if !govalidator.IsEmail(e.Email) {
		return fmt.Errorf("invalid EMAIL <%s>", e.Email)
	}

	return nil
}
