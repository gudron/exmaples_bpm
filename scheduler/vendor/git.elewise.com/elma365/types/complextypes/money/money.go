package money

//go:generate easyjson money.go

import "git.elewise.com/elma365/types/complextypes/money/currency"

// Money is currency cents pair
//
// easyjson:json
type Money struct {
	C currency.Currency `json:"currency"`
	V int64             `json:"cents"`
}
