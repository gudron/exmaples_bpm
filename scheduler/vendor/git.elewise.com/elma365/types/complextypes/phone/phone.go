package phone

//go:generate easyjson phone.go

// Phone is a tuple of phone information
//
// See https://www.ietf.org/rfc/rfc3966.txt for details of format
// easyjson:json
type Phone struct {
	Tel     string `json:"tel"`     // local part
	Context string `json:"context"` // phone-context in RFC // global part of address. Internation code or domain name
	Ext     string `json:"ext"`
	Type    string `json:"type"`
}

//func (p Phone) Validate(resource helpers.ValidationHelper) error {
//	if len(p.Tel) == 0 {
//		return fmt.Errorf("local part of PHONE must not be empty")
//	}
//	if len(p.Context) == 0 {
//		return fmt.Errorf("context(global) part of PHONE must not be empty")
//	}
//
//	if !(strings.HasPrefix(p.Context, "+") || strings.ContainsRune(p.Context, '.')) {
//		return fmt.Errorf("PHONE context must start with '+' or be a domain-like string")
//	}
//
//	return nil
//}
