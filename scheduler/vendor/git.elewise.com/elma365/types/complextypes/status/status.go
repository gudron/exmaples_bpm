package status

//go:generate easyjson status.go

// Status is a tuple of satus information
//
// easyjson:json
type Status struct {
	Status uint32 `json:"status"`
	Order  uint32 `json:"order"`
}
