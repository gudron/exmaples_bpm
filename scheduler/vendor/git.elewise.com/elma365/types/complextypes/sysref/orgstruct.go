package sysref

import (
	"github.com/satori/go.uuid"
)

// OSNode is a reference to orgstruct node
type OSNode = uuid.UUID
