package sysref

import (
	"github.com/satori/go.uuid"
)

// User reference
type User = uuid.UUID
