package types

//go:generate easyjson field.go

import (
	"encoding/json"
	"regexp"
	"time"

	"git.elewise.com/elma365/types/complextypes/category"
	"git.elewise.com/elma365/types/complextypes/email"
	"git.elewise.com/elma365/types/complextypes/file"
	"git.elewise.com/elma365/types/complextypes/money"
	"git.elewise.com/elma365/types/complextypes/phone"
	"git.elewise.com/elma365/types/complextypes/status"
	"git.elewise.com/elma365/types/complextypes/sysref"

	"strings"

	"github.com/asaskevich/govalidator"
	"github.com/pkg/errors"
)

// Field is a definition of application field
//
// easyjson:json
type Field struct {
	Code       string          `json:"code"         valid:"code,required"`
	Type       Type            `json:"type"         valid:"required"`
	Searchable bool            `json:"searchable"` // Возможность поиска по этому полю
	Indexed    bool            `json:"indexed"`    // Полнотекстовый поиск
	Deleted    bool            `json:"deleted"`
	Array      bool            `json:"array"` // Для валидации данных
	Required   bool            `json:"required"`
	Single     bool            `json:"single"` // Для отображения данных
	Default    json.RawMessage `json:"defaultValue"`
	Data       json.RawMessage `json:"data"` // Данные, специфичные для конкретного типа поля
}

// Validate value for this field
func (f Field) Validate(value json.RawMessage) (interface{}, error) {
	if value == nil || string(value) == "null" {
		if f.Required {

			return nil, errors.Errorf("%s: field is required", f.Code)
		}
		return nil, nil
	}
	if f.Type == String && string(value) == `""` {
		if f.Required {

			return nil, errors.Errorf("%s: field is required", f.Code)
		}
		return "", nil
	}

	var res interface{}
	var err error
	if f.Array {
		res, err = f.validateArray(value)
	} else {
		res, err = f.validateSingle(value)
	}

	return res, errors.WithStack(err)
}

// ValidateDefault value of the field
func (f Field) ValidateDefault() error {
	if len(f.Default) == 0 || string(f.Default) == "null" || string(f.Default) == `""` {

		return nil
	}

	var err error
	if f.Array {
		_, err = f.validateArray(f.Default)
	} else {
		_, err = f.validateSingle(f.Default)
	}

	return errors.WithStack(err)
}

func (f Field) validateArray(value json.RawMessage) (interface{}, error) {
	valarr := make([]json.RawMessage, 0)
	if err := json.Unmarshal(value, &valarr); err != nil {

		return nil, err
	}

	if f.Required && len(valarr) == 0 {

		return nil, errors.Errorf("%s: field is required", f.Code)
	}

	var res interface{}
	var ares []interface{}
	for _, val := range valarr {
		valitdatedFieldValue, err := f.validateSingle(val)
		if err != nil {
			return nil, err
		}
		ares = append(ares, valitdatedFieldValue)
	}
	res = ares
	return res, nil
}

// nolint: gocyclo
// Yeah, there is a huge and ugly switch.
// FIXME: разнести валидацию кастомных типов за интерфейс Validator
func (f Field) validateSingle(value json.RawMessage) (interface{}, error) {
	switch f.Type {
	case String:
		var res string
		err := f.errorParse(json.Unmarshal(value, &res))

		return res, err

	case Float:
		var res float64
		err := f.errorParse(json.Unmarshal(value, &res))

		return res, err

	case Integer:
		var res int32
		err := f.errorParse(json.Unmarshal(value, &res))

		return res, err

	case Boolean:
		var res bool
		err := f.errorParse(json.Unmarshal(value, &res))

		return res, err

	case DateTime:
		var res time.Time
		err := f.errorParse(json.Unmarshal(value, &res))

		return res, err

	case Duration:
		var res int32
		err := f.errorParse(json.Unmarshal(value, &res))

		return res, err

	case Category:
		var res category.Category
		err := f.errorParse(json.Unmarshal(value, &res))
		if err != nil {
			return nil, err
		}

		err = res.Validate()
		if err != nil {
			return nil, errors.Errorf("%s: %s", f.Code, err.Error())
		}

		return res, err

	case Tag:
		var res string
		err := f.errorParse(json.Unmarshal(value, &res))

		return res, err

	case Money:
		var res money.Money
		err := f.errorParse(json.Unmarshal(value, &res))

		return res, err

	case File:
		var res file.File
		err := f.errorParse(json.Unmarshal(value, &res))

		return res, err

	case Phone:
		var res phone.Phone
		err := f.errorParse(json.Unmarshal(value, &res))

		return res, err

	case Email:
		var res email.Email
		err := f.errorParse(json.Unmarshal(value, &res))
		if err != nil {
			return nil, err
		}

		err = res.Validate()
		if err != nil {
			return nil, errors.Errorf("%s: %s", f.Code, err.Error())
		}

		return res, nil

	case Version:
		logger.Print("WARN: VERSION does not implemented yet")
		var res string
		err := f.errorParse(json.Unmarshal(value, &res))
		if err != nil {

			return nil, err
		}
		if !govalidator.IsSemver(res) {

			return nil, errors.Errorf("%s: invalid %s", f.Code, f.Type)
		}

		return res, nil

	case JSON:

		return value, nil

	case SysUser:
		var res sysref.User
		err := f.errorParse(json.Unmarshal(value, &res))

		return res, err

	case SysOSNode:
		var res sysref.OSNode
		err := f.errorParse(json.Unmarshal(value, &res))

		return res, err

	case SysCollection:
		var res string
		err := f.errorParse(json.Unmarshal(value, &res))

		return res, err
	case Status:
		var res status.Status
		err := f.errorParse(json.Unmarshal(value, &res))

		return res, err
	}

	return nil, errors.Errorf("%s: type %s does not implemented yet", f.Code, f.Type)
}

var (
	jsonErrorRE = regexp.MustCompile(`^json: cannot unmarshal (.*) into Go value of type (.*)$`)
	uuidErrorRe = regexp.MustCompile(`^uuid: (.*): invalid$`)
	timeErrorRE = regexp.MustCompile(`^parsing time "(.*)" as ""2006-01-02T15:04:05Z07:00"": cannot parse "(.*)" as "(.*)"$`)
	timeParts   = map[string]string{
		"2006":   "year",
		"01":     "month",
		"02":     "day",
		"15":     "hour",
		"04":     "minutes",
		"05":     "seconds",
		"Z07:00": "time zone",
	}
)

func (f Field) errorParse(err error) error {
	if err == nil {

		return nil
	}

	if parts := jsonErrorRE.FindStringSubmatch(err.Error()); len(parts) == 3 {

		err = errors.Errorf("cannot unmarshal %s into field of type %s", parts[1], f.Type)

	} else if parts := uuidErrorRe.FindStringSubmatch(err.Error()); len(parts) == 2 {

		err = errors.New("invalid uuid")

	} else if parts := timeErrorRE.FindStringSubmatch(err.Error()); len(parts) == 4 {
		timePart, ok := timeParts[parts[3]]
		if !ok {
			timePart = parts[3]
		}
		err = errors.Errorf("cannot parse %q as %s", strings.Trim(parts[2], `"`), timePart)
	}

	return errors.Wrap(err, f.Code)
}
