package types

//go:generate easyjson fields.go

import (
	"database/sql/driver"
	"encoding/json"

	"github.com/pkg/errors"
)

// Fields is slice of fields
//
// easyjson:json
type Fields []Field

// Scan implements sql.Scanner interface
func (o *Fields) Scan(pSrc interface{}) error {
	var arrfs []Field

	switch src := pSrc.(type) {
	case []byte:
		if err := json.Unmarshal(src, &arrfs); err != nil {
			return err
		}

	default:

		return errors.Errorf("Fields.Scan: cannot scan type %T into Fields", pSrc)
	}
	*o = Fields(arrfs)

	return nil
}

// Value implements sql.Valuer interface
func (o Fields) Value() (value driver.Value, err error) {

	return json.Marshal(o)
}
