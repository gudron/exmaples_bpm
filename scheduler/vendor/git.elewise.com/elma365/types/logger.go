package types

import (
	"log"
	"os"
)

var logger *log.Logger

func init() {
	SetLogger(log.New(os.Stderr, "", 0))
}

// SetLogger for debug output of library (default Stderr using)
func SetLogger(l *log.Logger) {
	logger = l
}
